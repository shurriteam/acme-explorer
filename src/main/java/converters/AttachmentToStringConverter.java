package converters;

import domain.Attachment;
import domain.Auditor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Component
@Transactional

public class AttachmentToStringConverter implements Converter<Attachment, String> {


    @Override

    public String convert(Attachment actor) {
        Assert.notNull(actor);
        String result;
        result = String.valueOf(actor.getId());
        return result;
    }


}
