package converters;

import domain.Explorer;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.ExplorerRepository;


@Component
@Transactional

public class StringToExplorerConverter implements Converter<String, Explorer> {

   @Autowired
   ExplorerRepository explorerRepository;

   @Override
   public Explorer convert(String text) {
      Explorer result;
      int id;

      try {
         if (StringUtils.isEmpty(text))
            result = null;
         else {
            id = Integer.valueOf(text);
            result = explorerRepository.findOne(id);
         }
      } catch (Throwable oops) {
         throw new IllegalArgumentException(oops);
      }

      return result;

   }
}