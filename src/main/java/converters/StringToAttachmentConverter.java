package converters;


import domain.Attachment;
import domain.Audit;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.AttachmentRepository;
import repositories.AuditRepository;

@Component
@Transactional


public class StringToAttachmentConverter implements Converter<String, Attachment> {


    @Autowired
    AttachmentRepository attachmentRepository;

    @Override

    public Attachment convert(String text) {
        Attachment res;
        int id;

        try {
            if (StringUtils.isEmpty(text))
                res = null;
            else {
                id = Integer.valueOf(text);
                res = attachmentRepository.findOne(id);

            }
        } catch (Throwable oops) {
            throw new IllegalArgumentException(oops);
        }

        return res;


    }
}
