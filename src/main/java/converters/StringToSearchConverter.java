package converters;


import domain.Banner;
import domain.Search;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.BannerRepository;
import repositories.SearchRepository;

@Component
@Transactional


public class StringToSearchConverter implements Converter<String, Search>{


    @Autowired
    SearchRepository searchRepository;

    @Override

    public Search convert(String text){
        Search res;
        int id;

        try {
            if (StringUtils.isEmpty(text))
                res=null;
            else{
                id=Integer.valueOf(text);
                res=searchRepository.findOne(id);

            }
        }catch (Throwable  oops){
            throw new IllegalArgumentException(oops);
        }

        return res;








    }
}
