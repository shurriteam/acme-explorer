
package converters;

import domain.Note;
import domain.Sponsorship;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Component
@Transactional
public class SponsorshipToStringConverter implements Converter<Sponsorship, String> {

    @Override
    public String convert(Sponsorship actor) {
        Assert.notNull(actor);
        String result;
        result = String.valueOf(actor.getId());
        return result;
    }

}
