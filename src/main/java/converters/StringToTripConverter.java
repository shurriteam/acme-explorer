package converters;

import domain.Mezzage;
import domain.Trip;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.MezzageRepository;
import repositories.TripRepository;


@Component
@Transactional

public class StringToTripConverter implements Converter<String, Trip>{


    @Autowired
    TripRepository mezzageRepository;
    @Override

    public Trip convert(String text) {
        Trip result;
        int id;

        try {
            if (StringUtils.isEmpty(text))
                result = null;
            else {
                id = Integer.valueOf(text);
                result = mezzageRepository.findOne(id);
            }
        } catch (Throwable oops) {
            throw new IllegalArgumentException(oops);
        }

        return result;

    }

}