package converters;


import domain.Sponsor;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.SponsorRepository;

@Component
@Transactional
public class StringToSponsorConverter implements Converter<String, Sponsor> {

   @Autowired
   SponsorRepository rangerRepository;

   @Override
   public Sponsor convert(String perri) {
      Sponsor result;
      int id;
      try {
         if (StringUtils.isEmpty(perri))
            result = null;
         else {
            id = Integer.valueOf(perri);
            result = rangerRepository.findOne(id);
         }
      } catch (Throwable oops) {
         throw new IllegalArgumentException(oops);
      }

      return result;

   }

}
