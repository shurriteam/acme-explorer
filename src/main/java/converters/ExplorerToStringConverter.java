package converters;

import domain.Explorer;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Component
@Transactional

public class ExplorerToStringConverter implements Converter<Explorer, String> {


   @Override

   public String convert(Explorer actor) {
      Assert.notNull(actor);
      String result;
      result = String.valueOf(actor.getId());
      return result;
   }


}
