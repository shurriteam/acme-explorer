package converters;

import domain.Audit;
import domain.Banner;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;


@Component
@Transactional
public class AuditToStringConverter implements Converter<Audit, String> {


    @Override
    public String convert(Audit actor) {
        Assert.notNull(actor);
        String result;
        result = String.valueOf(actor.getId());
        return result;
    }
}
