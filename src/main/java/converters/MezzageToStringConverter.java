package converters;

import domain.Mezzage;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;


@Component
@Transactional
public class MezzageToStringConverter implements Converter<Mezzage,String> {

    @Override

    public String convert(Mezzage actor) {
        Assert.notNull(actor);
        String result;
        result = String.valueOf(actor.getId());
        return result;
    }
}
