
package converters;

import domain.Actor;
import domain.Trip;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Component
@Transactional
public class TripToStringConverter implements Converter<Trip, String> {

    @Override
    public String convert(Trip actor) {
        Assert.notNull(actor);
        String result;
        result = String.valueOf(actor.getId());
        return result;
    }

}
