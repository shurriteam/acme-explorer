
package converters;

import domain.Actor;
import domain.Note;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Component
@Transactional
public class NoteToStringConverter implements Converter<Note, String> {

    @Override
    public String convert(Note actor) {
        Assert.notNull(actor);
        String result;
        result = String.valueOf(actor.getId());
        return result;
    }

}
