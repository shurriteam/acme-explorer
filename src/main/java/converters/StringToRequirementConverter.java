package converters;


import domain.Requirement;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.RequirementRepository;

@Component
@Transactional
public class StringToRequirementConverter implements Converter<String, Requirement> {

   @Autowired
   RequirementRepository requirementRepository;

   @Override
   public Requirement convert(String perri) {
      Requirement result;
      int id;
      try {
         if (StringUtils.isEmpty(perri))
            result = null;
         else {
            id = Integer.valueOf(perri);
            result = requirementRepository.findOne(id);
         }
      } catch (Throwable oops) {
         throw new IllegalArgumentException(oops);
      }

      return result;

   }

}
