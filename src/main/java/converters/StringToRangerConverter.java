package converters;


import domain.Ranger;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.RangerRepository;

@Component
@Transactional
public class StringToRangerConverter implements Converter<String, Ranger> {

    @Autowired
    RangerRepository rangerRepository;

    @Override
    public Ranger convert(String perri) {
        Ranger result;
        int id;
        try {
            if (StringUtils.isEmpty(perri))
                result = null;
            else {
                id = Integer.valueOf(perri);
                result = rangerRepository.findOne(id);
            }
        } catch (Throwable oops) {
            throw new IllegalArgumentException(oops);
        }

        return result;

    }

}
