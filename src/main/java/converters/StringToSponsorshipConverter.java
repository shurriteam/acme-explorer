package converters;


import domain.Attachment;
import domain.Sponsorship;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.AttachmentRepository;
import repositories.SponsorRepository;
import repositories.SponsorshipRepository;

@Component
@Transactional


public class StringToSponsorshipConverter implements Converter<String, Sponsorship> {


    @Autowired
    SponsorshipRepository attachmentRepository;

    @Override

    public Sponsorship convert(String text) {
        Sponsorship res;
        int id;

        try {
            if (StringUtils.isEmpty(text))
                res = null;
            else {
                id = Integer.valueOf(text);
                res = attachmentRepository.findOne(id);

            }
        } catch (Throwable oops) {
            throw new IllegalArgumentException(oops);
        }

        return res;


    }
}
