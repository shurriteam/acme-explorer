package converters;

import domain.LegalText;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;


@Component
@Transactional
public class LegalTextToStringConverter implements Converter<LegalText, String> {


   @Override
   public String convert(LegalText actor) {
      Assert.notNull(actor);
      String result;
      result = String.valueOf(actor.getId());
      return result;
   }
}