package converters;

import domain.Tag;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.TagRepository;


@Component
@Transactional

public class StringToTagConverter implements Converter<String, Tag> {


   @Autowired
   TagRepository tagRepository;

   @Override

   public Tag convert(String text) {
      Tag result;
      int id;

      try {
         if (StringUtils.isEmpty(text))
            result = null;
         else {
            id = Integer.valueOf(text);
            result = tagRepository.findOne(id);
         }
      } catch (Throwable oops) {
         throw new IllegalArgumentException(oops);
      }

      return result;

   }

}