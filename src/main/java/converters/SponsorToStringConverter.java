package converters;

import domain.Sponsor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Component
@Transactional

public class SponsorToStringConverter implements Converter<Sponsor, String> {


   @Override

   public String convert(Sponsor actor) {
      Assert.notNull(actor);
      String result;
      result = String.valueOf(actor.getId());
      return result;
   }


}
