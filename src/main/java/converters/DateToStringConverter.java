package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Component
@Transactional

public class DateToStringConverter implements Converter<Date, String> {

   @Override
   public String convert(Date date) {
      String result;

      if (date == null)
         result = null;
      else
         result = date.toString().replace("-", "/");

      return result;
   }

}