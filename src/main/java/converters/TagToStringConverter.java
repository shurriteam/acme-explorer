package converters;

import domain.Tag;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;


@Component
@Transactional

public class TagToStringConverter implements Converter<Tag, String> {

   @Override
   public String convert(Tag actor) {
      Assert.notNull(actor);
      String result;
      result = String.valueOf(actor.getId());
      return result;
   }
}
