package converters;

import domain.SurvivalClass;
import domain.Tag;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.SurvivalclassRepository;
import org.springframework.core.convert.converter.Converter;



@Component
@Transactional

public class StringToSurvivalClassConverter implements Converter<String, SurvivalClass>{

    @Autowired
    private SurvivalclassRepository survivalclassRepository;





    @Override

    public SurvivalClass convert(String text) {
        SurvivalClass result;
        int id;

        try {
            if (StringUtils.isEmpty(text))
                result = null;
            else {
                id = Integer.valueOf(text);
                result=survivalclassRepository.findOne(id);
            }
        } catch (Throwable oops) {
            throw new IllegalArgumentException(oops);
        }

        return result;

    }



}
