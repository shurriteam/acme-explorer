
package converters;

import domain.Location;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.LocationRepository;


@Component
@Transactional
public class StringToLocationConverter implements Converter<String, Location> {

   @Autowired
   LocationRepository locationRepository;

   @Override
   public Location convert(String text) {
      Location result;
      int id;

      try {
         if (StringUtils.isEmpty(text))
            result = null;
         else {
            id = Integer.valueOf(text);
            result = locationRepository.findOne(id);
         }
      } catch (Throwable oops) {
         throw new IllegalArgumentException(oops);
      }

      return result;

   }

}
