package converters;

import domain.Banner;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;


@Component
@Transactional
public class BannerToStringConverter implements Converter<Banner, String> {


    @Override
    public String convert(Banner actor) {
        Assert.notNull(actor);
        String result;
        result = String.valueOf(actor.getId());
        return result;
    }
}
