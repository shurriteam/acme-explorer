package converters;

import domain.Auditor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Component
@Transactional

public class AuditorToStringConverter implements Converter<Auditor, String> {


   @Override

   public String convert(Auditor actor) {
      Assert.notNull(actor);
      String result;
      result = String.valueOf(actor.getId());
      return result;
   }


}
