package converters;


import domain.LegalText;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.LegalTextRepository;

@Component
@Transactional
public class StringToLegalTextConverter implements Converter<String, LegalText> {

   @Autowired
   LegalTextRepository legalTextRepository;

   @Override
   public LegalText convert(String perri) {
      LegalText result;
      int id;
      try {
         if (StringUtils.isEmpty(perri))
            result = null;
         else {
            id = Integer.valueOf(perri);
            result = legalTextRepository.findOne(id);
         }
      } catch (Throwable oops) {
         throw new IllegalArgumentException(oops);
      }

      return result;

   }

}
