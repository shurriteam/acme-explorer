package converters;


import domain.Audit;
import domain.Banner;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.AuditRepository;
import repositories.BannerRepository;

@Component
@Transactional


public class StringToAuditConverter implements Converter<String, Audit>{


    @Autowired
    AuditRepository auditRepository;

    @Override

    public Audit convert(String text){
        Audit res;
        int id;

        try {
            if (StringUtils.isEmpty(text))
                res=null;
            else{
                id=Integer.valueOf(text);
                res=auditRepository.findOne(id);

            }
        }catch (Throwable  oops){
            throw new IllegalArgumentException(oops);
        }

        return res;








    }
}
