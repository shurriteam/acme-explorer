package converters;

import domain.Stage;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;


@Component
@Transactional
public class StageToStringConverter implements Converter<Stage, String> {


   @Override
   public String convert(Stage actor) {
      Assert.notNull(actor);
      String result;
      result = String.valueOf(actor.getId());
      return result;
   }
}
