/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package repositories;

import domain.Explorer;
import domain.Status;
import domain.Trip;
import domain.TripApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import utilities.Finder;

import java.util.Collection;

@Repository
public interface ExplorerRepository extends JpaRepository<Explorer, Integer> {


   @Query("select c from Explorer c where c.userAccount.id = ?1")
   Explorer findByUserAccountId(int userAccountId);

   @Query("select c from Explorer c join c.userAccount u join u.authorities a where a.authority = 'EXPLORER'")
   Collection<Explorer> explorers();

   @Query("select c from Explorer c where c.id = ?1")
   Collection<Finder> getFinderByExplorer(int explorerId);

   @Query("select c from Explorer c join c.tripApplications tp join tp.trip t where t.id=?1 and t.endDate <= current_date ")
   Collection<Trip> tripsEndedInWithIveEnrolled(int explorerId);

   @Query("select tp from Explorer c join c.tripApplications tp where tp.status = 2")
   Collection<TripApplication> dueApplications();

   @Query("select tp from Explorer c join c.tripApplications tp where c.id =?1 and tp.status = ?2")
   Collection<TripApplication> explorerApplicationsFromStatus(int explorerId, Status status);

   @Query("select ta from Explorer c join c.tripApplications ta where c.id =?1")
   Collection<TripApplication> allExplorersApplications(int explorerId);
}
