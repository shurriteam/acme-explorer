/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package repositories;

import domain.Auditor;
import domain.SurvivalClass;
import domain.TripApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface SurvivalclassRepository extends JpaRepository<SurvivalClass, Integer> {

   @Query("select t.survivalClasses from Explorer c join c.tripApplications ta join ta.trip t where c.id =?1 and t.endDate > current_date ")
   Collection<SurvivalClass> survivalClassesFromExplorer(int explorerID);

   @Query("select a from TripApplication a where a.trip.id=?1 and a.owner.id =?2")
   TripApplication findApplication(int tripID, int explorerID);

}
