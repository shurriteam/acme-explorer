/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package repositories;

import domain.Curricula;
import domain.Ranger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface RangerRepository extends JpaRepository<Ranger, Integer> {


    @Query("select c.curricula from Ranger c where c.id = ?1")
    Curricula getCurriculaByRangerId(int id);


   @Query("select c from Ranger c where c.userAccount.id = ?1")
    Ranger findByUserAccountId(int userAccountId);

}
