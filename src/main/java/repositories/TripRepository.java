/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package repositories;

import domain.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface TripRepository extends JpaRepository<Trip, Integer>, PagingAndSortingRepository<Trip, Integer> {

    @Query("select c.audits from Trip c where c.id = ?1")
    Collection<Audit> getAuditByTrip(int tripId);

    @Query("select c.notes from Trip c where c.id = ?1")
    Collection<Note> getNotesByTrip(int tripId);

    @Query("select c.relatedTrips from Category c where c = ?1")
    Collection<Trip> tripsGivenACategory(Category category);

    @Query("select c from Trip c where c.cancelled=false and c.endDate > current_date")
    Collection<Trip> showableTrips();

    @Query("select c.owner from TripApplication c where c.trip.id=?1")
    Collection<Explorer> explorerByTrip(int tripId);

    @Query("select sv.explorers from Trip c join c.survivalClasses sv where c=?1")
    Collection<Explorer> explorerEnrolledInSC(Trip t);
}
