/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package repositories;

import domain.Explorer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;

/**
 * Created by daviddelatorre on 15/5/17.
 */
public interface UserRepository extends JpaRepository<Explorer, Integer> {

    @Query("select c from Explorer c where c.userAccount.id = ?1")
    Explorer findByUserAccountId(int userAccountId);

    @Query("select c from Explorer c join c.userAccount u join u.authorities a where a.authority = 'MODERATOR'")
    Collection<Explorer> usersNotModerator();

}
