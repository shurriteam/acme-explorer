/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package repositories;

import domain.Actor;
import domain.Folder;
import domain.Mezzage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;

/**
 * Created by daviddelatorre on 15/5/17.
 */
public interface MezzageRepository extends JpaRepository<Mezzage, Integer> {

   @Query("select u.folders from Actor u where u.id = ?1")
   Collection<Folder> getFolder(int aci);

   @Query("select u from Actor u where u.email = ?1")
   Actor findUserByEmail(String email);

   @Query("select f.mezzages from Actor u join u.folders f where u=?1")
   Collection<Mezzage> allMessages(Actor u);

   @Query("select f from Actor c join c.folders f where f.name like %?2 and f.owner = ?1")
   Folder folderByName(Actor actor, String nameFolder);

}
