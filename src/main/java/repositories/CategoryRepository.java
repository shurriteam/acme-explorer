/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package repositories;

import domain.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 * Created by daviddelatorre on 15/5/17.
 */
public interface CategoryRepository extends JpaRepository<Category, Integer> {


   @Query("select c.id from Category c where c.name='category'")
   int categoryMasterId();

   @Modifying
   @Query("UPDATE Trip t SET t.category = ?2  where t.category = ?1")
   void safeDelete(Category category, Category father);

}
