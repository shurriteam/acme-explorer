/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package repositories;

import domain.Explorer;
import domain.Manager;
import domain.SurvivalClass;
import domain.Trip;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface ManagerRepository extends JpaRepository<Manager, Integer> {
   @Query("select c from Manager c where c.userAccount.id = ?1")
    Manager findByUserAccountId(int userAccountId);

    @Query("select c from Manager c join c.userAccount u join u.authorities a where a.authority = 'MANAGER'")
    Collection<Manager> explorers();

    @Query("select c, count(tp.owner) from Manager c join c.organizedTrips t join t.tripApplications tp group by c")
    Collection<Explorer> pruebaA();

    @Query("select m.organizedTrips from Manager m join  m.organizedTrips where m.id=?1")
    Collection<Trip> organizedTripsFromAGivenManager(int managerID);

    @Query("select tp.survivalClasses from Manager t join t.organizedTrips tp where t =?1 ")
    Collection<SurvivalClass> survivalClassFromManager(Manager m);

}
