/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package repositories;

import domain.Explorer;
import domain.Search;
import domain.Trip;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@Repository
public interface SearchRepository extends JpaRepository<Search, Integer> {

    @Query("select c from Search c where c.searcher = ?1")
    Collection<Search> searchByActor(Explorer a);


   @Query("select c from Trip c where c.price between ?1 and ?2")
   List<Trip> findByPrice(double underprice, double overprice);

   @Query("select c from Trip c where c.startDate=?1")
   List<Trip> findByStartDate(Date startDate);

   @Query("select c from Trip c where c.endDate=?1")
   List<Trip> findByEndDate(Date endDate);

   @Query("select c from Trip c where c.title like ?1 or c.description like ?1 or c.ticker like ?1")
   List<Trip> findByKeyWord(String keyword);

   @Query("select c from Trip c where c.price < ?1")
   List<Trip> findByHighestPrice(Double price);

   @Query("select c from Trip c where c.price > ?1")
   List<Trip> findByLowestPrice(Double price);

   @Query("select c from Search c where c.searcher = ?1 and c.searchDate > ?2")
   Collection<Search> trunkatedSearchByExplorer(Explorer explorer, Date cachedDate);

}
