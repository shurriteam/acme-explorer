/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package repositories;

import domain.Sponsor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface SponsorRepository extends JpaRepository<Sponsor, Integer> {
   @Query("select c from Sponsor c where c.userAccount.id = ?1")
   Sponsor findByUserAccountId(int userAccountId);

   @Query("select c from Sponsor c join c.userAccount u join u.authorities a where a.authority = 'EXPLORER'")
   Collection<Sponsor> explorers();

}
