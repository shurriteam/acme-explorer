/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package repositories;

import domain.Audit;
import domain.Auditor;
import domain.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface AuditorRepository extends JpaRepository<Auditor, Integer> {
   @Query("select c from Auditor c where c.userAccount.id = ?1")
   Auditor findByUserAccountId(int userAccountId);

   @Query("select c from Auditor c join c.userAccount u join u.authorities a where a.authority = 'AUDITOR'")
   Collection<Auditor> auditors();

   @Query("select c.notes from Auditor c where c.id = ?1")
   Collection<Note> getNotesByAuditor(int auditorId);

   @Query("select c.audits from Auditor c where c.id = ?1")
   Collection<Audit> getAuditionsByAuditor(int auditorId);
}
