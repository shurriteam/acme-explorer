/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package repositories;

import domain.EndorseRecord;
import domain.Explorer;
import domain.Trip;
import domain.TripApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface TripapplicationRepository extends JpaRepository<TripApplication, Integer> {

    @Query("select t from TripApplication t where t.status = 2 and t.trip.endDate > current_date and t.owner.id =?1")
    Collection<TripApplication> tripApplicationsWithAcceptedStatus(int explorerId);

    @Query("select c from TripApplication c where c.owner=?1 and c.trip=?2")
    TripApplication tripAGivenTripExplorer(Explorer explorer, Trip trip);





}
