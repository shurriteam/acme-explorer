/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package repositories;

import domain.Auditor;
import domain.LegalText;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface LegalTextRepository extends JpaRepository<LegalText, Integer> {


    @Query("select c from LegalText c where c.status='PUBLISHED'")
    Collection<LegalText> publishedLegalText();
}
