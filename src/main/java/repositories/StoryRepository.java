/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package repositories;

import domain.EducationRecord;
import domain.Explorer;
import domain.Story;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface StoryRepository extends JpaRepository<Story, Integer> {

    @Query("select c from Story c where c.explorer = ?1")
    Collection<Story> storyByExplorer(Explorer explorer);
}
