/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package repositories;

import domain.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by daviddelatorre on 28/3/17.
 */
@Repository
public interface AdministratorRepository extends JpaRepository<Administrator, Integer> {

   @Query("select c from Administrator c where c.userAccount.id = ?1")
   Administrator findByUserAccountId(int userAccountId);


   @Query("select c from Administrator c join c.userAccount u join u.authorities a where a.authority = 'ADMINISTRATOR'")
   Collection<Administrator> administrators();

////   //TODO esta query no funciona correctamente, probar m�s detenidamente
//   @Query("select m from Manager m where (select f from m.folders f where f.name like 'spam%' and f.mezzages is not empty ) is not empty")
//   Collection<Manager> suspiciousManagers();


 @Query("select m from Actor m join m.folders f where f.name like 'spam%' and f.mezzages is not empty")
 Collection<Actor> suspiciousActors();

   @Query("select t from Tag t where t.trip.size=0")
   Collection<Tag> findNotUsedTag();


    //*************************** LEVEL C ***************************


    //The average, the minimum, the maximum, and the standard deviation of the number of applications per trip.
    @Query("select avg(t.tripApplications.size) from Trip t")
    Double averageNumberOfApplicationsPerTrip();

    @Query("select max(t.tripApplications.size) from Trip t")
    int maximumNumberOfAppicationsPerTrip();

    @Query("select min(t.tripApplications.size) from Trip t")
    int minimumNumberOfApplicationsPerTrip();

    @Query("select stddev(t.tripApplications.size) from Trip t")
    Double standardDeviationOfTheNumberOfApplicationsPerTrip();


    @Query("select avg(m.organizedTrips.size) from Manager m")
    Double averageNumberOfTripsManagerPerManager();

    @Query("select max(m.organizedTrips.size) from Manager m")
    int maximumNumberOfTripsManagerPerManager();

    @Query("select min (m.organizedTrips.size)from Manager m")
    int minimumNumberOfTripsManagerPerManager();


    @Query(" select  stddev(m.organizedTrips.size) from Manager m")
    Double standartDeviationOfNumberOfTripsManagerPerManager();


    @Query("select avg(p.price) from Trip p")
    Double averageNumberOfTripsPrice();

    @Query("select max(p.price) from Trip p")
    int maximumNumberOfTripsPrice();

    @Query("select min(p.price) from Trip p")
    int minimumNumberOfTripsPrice();


    @Query("select stddev(p.price) from Trip p")
    Double standartDeviationOfNumberOfTripsPrice();


    @Query("select avg(r.tripsToGuide.size) from Ranger r")
    Double averageNumberOfTripsGuidedPerRanger();

    @Query(" select max(r.tripsToGuide.size) from Ranger r")
    int maximumNumberOfTripsGuidedPerRanger();

    @Query("select min(r.tripsToGuide.size) from Ranger r")
    int minimumNumberOfTripsGuidedPerRanger();


    @Query("select stddev(r.tripsToGuide.size) from Ranger r")
    Double standartDeviationOfNumberOfTripsGuidedPerRanger();


    // The ratio of applications with status ?PENDING?.
    // El ratio se calcula posteriormente en java con los datos devueltos por las siguientes queries
    @Query("select t from TripApplication t")
    Collection<TripApplication> numberOfApplications();

    @Query("select t from TripApplication t WHERE t.status = 0")
    Collection<TripApplication> numberOfAppsWithStatusPending();

    //The ratio of applications with status ?DUE?.
    @Query("select t from TripApplication t WHERE t.status = 2")
    Collection<TripApplication> numberOfAppsWithStatusDue();

    // The ratio of applications with status ?ACCEPTED?.
    @Query("select t from TripApplication t WHERE t.status = 3")
    Collection<TripApplication> numberOfAppsWithStatusAccepted();

    // The ratio of applications with status ?CANCELLED?.
    @Query("select t from TripApplication t WHERE t.status = 4")
    Collection<TripApplication> numberOfAppsWithStatusCancelled();

// The ratio of trips that have been cancelled versus the total number of trips that have been organised.
    // El ratio se calcula posteriormente en java con los datos devueltos por las siguientes queries

    @Query("select t from Trip t join t.tripApplications tp where tp.status = 4")
    Collection<Trip> numberOfCancelledTrips();

    @Query("select t from Trip t")
    Collection<Trip> numberOfTrips();


    //The listing of trips that have got at least 10% more applications than the av-erage, ordered by number of applications.

    @Query("select t from Trip t where t.tripApplications.size > (select avg(tripApplications.size) from Trip) * 0.1 order by t.tripApplications.size")
    Collection<Trip> tripsThatHaveGotAtLeast10PercMoreApplicationsThanTheAverageOrdereByNumberOfApplications();

    // A table with the number of times that each legal text?s been referenced.
    @Query("select t, count (t.trips.size) from LegalText t group by t")
    List<Object[]> numberOfTimesEachLegalTextHasBeenReferenced();


    //*************************** LEVEL B ***************************

    //The minimum, the maximum, the average, and the standard deviation of the number of notes per trip.
    @Query("select avg(t.notes.size) from Trip t")
    Double averageNumberOfNotesPerTrip();

    @Query("select max(t.notes.size) from Trip t")
    int maximumNumberOfNotesPerTrip();

    @Query("select min(t.notes.size) from Trip t")
    int minimumNumberOfNotesPerTrip();


    @Query("select stddev(t.notes.size) from Trip t")
    Double standartDeviationOfNotesPerTrip();

    // The minimum, the maximum, the average, and the standard deviation of audit records per trip.
    @Query("select avg(t.audits.size) from Trip t")
    Double averageNumberOfAuditsPerTrip();

    @Query("select max(t.audits.size) from Trip t")
    int maxNumberOfAuditsPerTrip();

    @Query("select min(t.audits.size) from Trip t")
    int minNumberOfAuditsPerTrip();


    @Query("select stddev(t.audits.size) from Trip t")
    Double standartDeviationOfAuditsPerTrip();


    // The ratio of trips with an audit record.
    // Se usa la query anterior que devuelve todos los Trip
    @Query("select t from Trip t where t.audits IS NOT EMPTY ")
    Collection<Trip> numberOfTripsWithoutAudits();


    // The ratio of rangers who have registered their curricula.
// El ratio se calcula posteriormente en java con los datos devueltos por las siguientes queries
    @Query("select t from Ranger t")
    Collection<Ranger> numberOfRangers();

    @Query("select t from Ranger t where t.curricula is not null")
    Collection<Ranger> numberOfRangersWithoutCurricula();

    // The ratio of rangers whose curriculum?s been endorsed.
    @Query("select ranger from Ranger ranger JOIN ranger.curricula c where c.endorseRecords IS NOT empty ")
    Collection<Ranger> rangerWithoutEndorsedCurricula();

    // The ratio of suspicious managers.
    @Query("select m from Manager m")
    Collection<Manager> numberOfManagers();


    @Query("select m from Manager m where (select f from m.folders f where f.name like 'spam%' and f.mezzages is not empty ) is not null")
    Collection<Manager> numerOfSuspiciousManagers();

    // The ratio of suspicious rangers.

    @Query("select m from Ranger m where (select f from m.folders f where f.name like 'spam%' and f.mezzages is not empty ) is not null")
    Collection<Ranger> numberOfSuspiciousRangers();




}
