/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;


import domain.AdministratorUtils;
import domain.Attachment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.AttachmentRepository;
import repositories.UtilsRepository;

import java.util.Collection;

@Service
@Transactional
public class Utilservice {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private UtilsRepository utilsRepository;

   // Managed repository--------------------------------------------------------------------------------

   public Utilservice() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public AdministratorUtils create() {
      AdministratorUtils res;
      res = new AdministratorUtils();
      return res;
   }

   public Collection<AdministratorUtils> findAll() {
      Collection<AdministratorUtils> res;
      res = utilsRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public AdministratorUtils findOne(int Cate) {
      AdministratorUtils res;
      res = utilsRepository.findOne(Cate);
      Assert.notNull(res);
      return res;
   }

   public AdministratorUtils save(AdministratorUtils a) {
      Assert.notNull(a);
      AdministratorUtils res;
      res = utilsRepository.save(a);
      return res;

   }

   public void delete(AdministratorUtils a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      utilsRepository.delete(a);
   }

   // Other business methods -------------------------------------------------------------------------------

}



