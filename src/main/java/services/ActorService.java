/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.Actor;
import domain.AdministratorUtils;
import domain.Folder;
import domain.Mezzage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import repositories.ActorRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import security.UserAccountService;

import javax.transaction.Transactional;
import java.util.*;

import static org.hibernate.jpa.internal.QueryImpl.LOG;

@Service
@Transactional
public class ActorService {

   // Managed Repository ------------------------
   @Autowired
   private ActorRepository actorRepository;
   @Autowired
   private UserAccountService userAccountService;
   @Autowired
   private FolderService folderService;
   @Autowired
   private MezzageService mezzageService;
   @Qualifier("utilservice")
   @Autowired
   private Utilservice utilservice;


   // Supporting services -----------------------

   // Constructor -------------------------------
   public ActorService() {
      super();
   }

   // Simple CRUD methods -----------------------

   public Actor findOne(int actorId) {
      Actor result;

      result = actorRepository.findOne(actorId);

      return result;
   }

   public Collection<Actor> findAll() {
      Collection<Actor> result;

      result = actorRepository.findAll();

      return result;
   }

   public Actor save(Actor actor) {
      Assert.notNull(actor);
      return actorRepository.save(actor);
   }

   public void delete(Actor actor) {
      Assert.notNull(actor);
      Assert.isTrue(actorRepository.exists(actor.getId()));
      actorRepository.delete(actor);
   }

   // Other business methods -----------------------


   public Actor findByPrincipal() {
      Actor result;
      UserAccount userAccount;

      userAccount = LoginService.getPrincipal();
      Assert.notNull(userAccount);
      result = findByUserAccount(userAccount);
      Assert.notNull(result);

      return result;
   }

   private Actor findByUserAccount(UserAccount userAccount) {
      Assert.notNull(userAccount);

      Actor result;

      result = actorRepository.findByUserAccountId(userAccount.getId());

      return result;
   }

   public void flush() {
      actorRepository.flush();
   }

   //Manage Folder

    boolean findByPrincipalHasRol(String rol) {
      boolean res = false;
      Assert.notNull(rol, "rol is null");

      Actor a = this.findByPrincipal();
      Assert.notNull(a, "actor is null");
      for(Authority at : a.getUserAccount().getAuthorities()){
         if(at.getAuthority().equals(rol)){
            res = true;
            break;
         }
      }
      return res;
   }


   public boolean updateBroacastMezzages(){
      boolean res = false;
      try {
         Actor actor = findByPrincipal();
         Folder f = actorRepository.getNotificationFolder(actor);
         List<AdministratorUtils> administratorUtils = new ArrayList<>(utilservice.findAll());
         Set<Mezzage> actorMezzages = new HashSet<>(f.getMezzages());
         Assert.isTrue(checkNewNotifications(f), "There is no new notification availables");
         LOG.info("New notifications availables, checking it");
         actorMezzages.addAll(administratorUtils.get(0).getBroadcastedMezzanges());
         f.setMezzages(actorMezzages);
         folderService.save(f);
         res=true;
      } catch (Exception e) {
         e.printStackTrace();
      }
      return res;
   }

   private boolean checkNewNotifications(Folder f){
      boolean res = false;
      List<AdministratorUtils> administratorUtils = new ArrayList<>(utilservice.findAll());
      LOG.info("Checking if there is new notifications");
      if(res = administratorUtils.get(0).getBroadcastedMezzanges().size()>f.getMezzages().size()){
         res = true;
      }

      return res;
   }
   public Actor actorByEmail(String email){
      Actor res = actorRepository.findUserByEmail(email);
      Assert.notNull(res);
      return res;
   }
}