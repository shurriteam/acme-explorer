package services;

import domain.Search;
import domain.Trip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import repositories.SearchRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by daviddelatorre on 28/3/17.
 */
@Service
@Transactional
public class Search2Service {

   // Managed Repository ------------------------
   @Autowired
   private SearchRepository searchRepository;
   @Autowired
   private ActorService chorbiService;

   @Autowired
   private TripService tripService;

   // Supporting services -----------------------

   // Constructor -------------------------------
   public Search2Service() {
      super();
   }

   // Simple CRUD methods -----------------------
   public Search create() {
      Search res;
      res = new Search();
      return res;
   }

   public Search findOne(int actorId) {
      Search result;

      result = searchRepository.findOne(actorId);

      return result;
   }

   public Collection<Search> findAll() {
      Collection<Search> result;

      result = searchRepository.findAll();

      return result;
   }

   public Search save(Search actor) {
      Assert.notNull(actor);
      return searchRepository.save(actor);
   }

   public void delete(Search actor) {
      Assert.notNull(actor);
      Assert.isTrue(searchRepository.exists(actor.getId()));
      searchRepository.delete(actor);
   }

   // Other business methods -----------------------

   public void flush() {
      searchRepository.flush();
   }

//    public List<Chorbi> finder(Integer age, Relationship relationship, Genre genre, Coordinate coordinate, String keyword){
//        List<Chorbi> chorbis = new ArrayList<>(chorbiService.findAll());
//        List<Chorbi> aux = new ArrayList<>();
//        if (age==null||relationship==null||genre==null||coordinate.getCity()==null||coordinate.getCountry()==null||coordinate.getState()==null||coordinate.getProvince()==null||keyword==null){
//            return chorbis;
//        }else {
//            for (Chorbi p : chorbis){
//                if ((p.getAge()==age||p.getGenre().equals(genre)||p.getRelationship().equals(relationship)||p.getCoordinate().getCity().equals(coordinate.getCity())||p.getCoordinate().getCountry().equals(coordinate.getCountry())||p.getCoordinate().getState().equals(coordinate.getState()) || p.getCoordinate().getProvince().equals(coordinate.getProvince())&& containsKey(chorbis,keyword))){
//                    aux.add(p);
//                }
//            }
//            return aux;
//        }
//    }
//    private Boolean containsKey(List<Chorbi> chorbis, String keyword){
//        Boolean res = false;
//        for (Chorbi p : chorbis){
//            if (p.getDescription().contains(keyword) || p.getCoordinate().getProvince().contains(keyword) || p.getCoordinate().getCity().contains(keyword)|| p.getCoordinate().getState().contains(keyword)||p.getCoordinate().getCountry().contains(keyword)){
//                res = true;
//            }
//        }
//        return res;
//    }


   public List<Trip> deprecatedFinder(Search search) {

      List<Trip> todos = new ArrayList<>(tripService.findAll());

      if (!(search.getStartDate() == null)) {
         todos.retainAll(searchRepository.findByStartDate(search.getStartDate()));
      }
      if (!(search.getEndDate() == null)) {
         todos.retainAll(searchRepository.findByEndDate(search.getEndDate()));
      }
      if (!(search.getLowestPrice() == null)) {
         todos.retainAll(searchRepository.findByLowestPrice(search.getLowestPrice()));
      }
      if (!(search.getHighestPrice() == null)) {
         todos.retainAll(searchRepository.findByHighestPrice(search.getHighestPrice()));
      }
      if (!search.getKeyWord().isEmpty()) {
         todos.retainAll(searchRepository.findByKeyWord(search.getKeyWord()));
      }

      return todos;

   }




}
