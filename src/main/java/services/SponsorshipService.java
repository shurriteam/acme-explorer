/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.Sponsor;
import domain.Sponsorship;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.SponsorshipRepository;

import java.util.Collection;

@Service
@Transactional
public class SponsorshipService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private SponsorshipRepository sponsorshipRepository;

   // Managed repository--------------------------------------------------------------------------------

   public SponsorshipService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public Sponsorship create() {
      Sponsorship res;
      res = new Sponsorship();
      return res;
   }

   public Collection<Sponsorship> findAll() {
      Collection<Sponsorship> res;
      res = sponsorshipRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public Sponsorship findOne(int Cate) {
      Sponsorship res;
      res = sponsorshipRepository.findOne(Cate);
      Assert.notNull(res);
      return res;
   }

   public Sponsorship save(Sponsorship a) {
      Assert.notNull(a);
      Sponsorship res;
      res = sponsorshipRepository.save(a);
      return res;

   }

   public void delete(Sponsorship a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      sponsorshipRepository.delete(a);
   }


   // Other business methods -------------------------------------------------------------------------------
   public void flush() {
      sponsorshipRepository.flush();
   }
}



