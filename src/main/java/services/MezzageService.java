/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.Actor;
import domain.Folder;
import domain.Mezzage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import static org.hibernate.jpa.internal.QueryImpl.LOG;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@Transactional
public class MezzageService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private repositories.MezzageRepository mezzageRepository;


   @Autowired
   private ActorService actorService;

   @Autowired
   private FolderService folderService;


   // Managed repository--------------------------------------------------------------------------------

   public MezzageService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public Mezzage create() {
      Mezzage res = new Mezzage();
      return res;
   }

   public Collection<Mezzage> findAll() {

      Collection<Mezzage> res = mezzageRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public Mezzage findOne(int mezzageId) {
      Mezzage res = mezzageRepository.findOne(mezzageId);
      Assert.notNull(res);
      return res;
   }

   public Mezzage save(Mezzage a) {
      Assert.notNull(a);
      Mezzage res = mezzageRepository.save(a);
      return res;
   }

   public void delete(Mezzage a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      mezzageRepository.delete(a);
   }

   // Other business methods -------------------------------------------------------------------------------
   public Collection<Folder> getFolders() {

      Actor u;
      u = actorService.findByPrincipal();
      Collection<Folder> res = null;
      try {
         Assert.notNull(u, "El actor no existe");
         res = mezzageRepository.getFolder(u.getId());
      } catch (Exception e) {
         e.printStackTrace();
      }
      return res;
   }

   public boolean modifyFolder(Folder f) {
      Actor u;
      boolean res = false;
      u = actorService.findByPrincipal();
      try {
         LOG.info("Modifiying folder " + f);
         Assert.notNull(u, "El actor no existe");
         Assert.notNull(f);
         Assert.isTrue(u.getFolders().contains(f), "Esta carpeta no pertence al actor autentificado");
         Assert.isTrue(!this.isSystemFolder(f),"This folder is a system folder ant can't be modified");
         folderService.save(f);
         res = true;
      } catch (Exception e) {
         e.printStackTrace();
      }
      return res;
   }

   public boolean deleteFolder(Folder f) {
      Actor u;
      boolean res = false;
      u = actorService.findByPrincipal();
      try {
         LOG.info("Modifiying folder " + f);
         Assert.notNull(u, "El actor no existe");
         Assert.notNull(f, "Folder is null");
         Assert.isTrue(u.getFolders().contains(f), "Esta carpeta no pertence al actor autentificado");
         Assert.isTrue(!this.isSystemFolder(f),"This folder is a system folder ant can't be modified");
         folderService.delete(f);
         res = true;
      } catch (Exception e) {
         e.printStackTrace();
      }
      return res;
   }


   public Mezzage recieveMessage(Mezzage mezzage, Actor a) {


      List<Folder> folders = new ArrayList<>(a.getFolders());
      Assert.notEmpty(folders, "carpetas vacias");
      folders.get(2).getMezzages().add(mezzage);
      a.setFolders(folders);
      return mezzage;
   }

   public void textMessage(Mezzage mezzage) {

      Assert.notNull(mezzage.getSubject(), "El subject no existe");
      Assert.notNull(mezzage.getBody(), "El body no existe");
      Assert.notNull(mezzage.getReceiverEmail(), "El recipient no existe");

      Actor receiver = mezzageRepository.findUserByEmail(mezzage.getReceiverEmail());

      Actor sender = mezzageRepository.findUserByEmail(mezzage.getSenderEmail());


      mezzage.setRecipient(receiver);
      Mezzage mezzage1 = this.create();
      mezzage1.setMoment(mezzage.getMoment());
      mezzage1.setSenderEmail(mezzage.getSenderEmail());
      mezzage1.setBody(mezzage.getBody());
      mezzage1.setSubject(mezzage.getSubject());
      mezzage1.setPriority(mezzage.getPriority());
      mezzage1.setRecipient(mezzage.getRecipient());
      mezzage1.setReceiverEmail(mezzage.getReceiverEmail());

      if(this.checkBodySpam(mezzage.getBody())){
         Folder f = this.folderByName(receiver, "spambox");
         mezzage1.setFolder(f);
      }else {
         Folder f = this.folderByName(receiver, "inbox");
         mezzage1.setFolder(f);
      }




      Folder fs = this.folderByName(sender, "outbox");
      mezzage.setFolder(fs);

      this.save(mezzage);
      this.save(mezzage1);


   }


   public Collection<Mezzage> allMessages(Actor a) {

      return mezzageRepository.allMessages(a);
   }

   public Folder folderByName(Actor a, String name) {

      return mezzageRepository.folderByName(a, name);
   }


   private boolean isSystemFolder(Folder f){
      Assert.notNull(f);
      boolean res = false;

      if(f.getName().equals("inbox")
              || f.getName().equals("outbox")
              || f.getName().equals("trashbox")
              || f.getName().equals("inbox")
              || f.getName().equals("spambox")){
         res = true;
      }

      return res;

   }

   private boolean checkBodySpam(String body){
      boolean res = false;

      String regexpMail = "^\\bviagra|\\bsex|\\bcialis|\\bjes extender";
      Pattern mail = Pattern.compile(regexpMail);
      Matcher matcher1 = mail.matcher(body);
      if(matcher1.find()){
         res = true;
      }

      return res;

   }
}



