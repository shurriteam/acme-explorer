/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import repositories.AdministratorRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import security.UserAccountService;

import javax.naming.OperationNotSupportedException;
import javax.transaction.Transactional;
import java.util.*;

import static org.hibernate.jpa.internal.QueryImpl.LOG;

/**
 * Created by daviddelatorre on 28/3/17.
 */
@Service
@Transactional
public class AdministratorService {

   // Managed Repository ------------------------
   @Autowired
   public AdministratorRepository administratorRepository;

   // Supporting services -----------------------

   @Autowired
   private UserAccountService userAccountService;
   @Autowired
   private FolderService folderService;
   @Autowired
   private ActorService actorService;
   @Autowired
   private LegalTextService legalTextService;
   @Autowired
   private TagService tagService;
   @Autowired
   private TripService trip;
   @Autowired
   private CategoryService categoryService;
   @Autowired
   private Utilservice utilservice;

   public AdministratorService() {
      super();
   }

   // Simple CRUD methods -----------------------
   public Administrator create() {
      Administrator res;
      res = new Administrator();
      return res;
   }

   public Administrator findOne(int actorId) {
      Administrator result;

      result = administratorRepository.findOne(actorId);

      return result;
   }

   public Collection<Administrator> findAll() {
      Collection<Administrator> result;

      result = administratorRepository.findAll();

      return result;
   }

   public Administrator save(Administrator actor) {
      Assert.notNull(actor);
      return administratorRepository.save(actor);
   }

   public void delete(Administrator actor) {
      Assert.notNull(actor);
      Assert.isTrue(administratorRepository.exists(actor.getId()));
      administratorRepository.delete(actor);
   }


   // Other business methods -----------------------


   public Administrator findByPrincipal() {
      Administrator result;
      UserAccount userAccount;

      userAccount = LoginService.getPrincipal();
      Assert.notNull(userAccount);
      result = findByUserAccount(userAccount);
      Assert.notNull(result);

      return result;
   }

   private Administrator findByUserAccount(UserAccount userAccount) {
      Assert.notNull(userAccount);

      Administrator result;

      result = administratorRepository.findByUserAccountId(userAccount.getId());

      return result;
   }
   public Actor registerAsAdministrator(Administrator u) {
      Assert.notNull(u);
      Authority autoh = new Authority();
      autoh.setAuthority("ADMINISTRATOR");
      UserAccount res = new UserAccount();
      res.addAuthority(autoh);
      res.setUsername(u.getUserAccount().getUsername());
      Md5PasswordEncoder encoder;
      encoder = new Md5PasswordEncoder();
      String hash = encoder.encodePassword(u.getUserAccount().getPassword(), null);
      res.setPassword(hash);
      UserAccount userAccount = userAccountService.save(res);
      u.setUserAccount(userAccount);
      Assert.notNull(u.getUserAccount().getAuthorities(), "authorities null al registrar");
      Administrator resu = administratorRepository.save(u);
      Collection<Mezzage> received = new HashSet<>();
      Collection<Mezzage> sended = new HashSet<>();
      Collection<Folder> folders = new HashSet<>();
      Folder inbox = folderService.create();
      inbox.setName("inbox");
      inbox.setOwner(resu);
      Collection<Mezzage> innnn = new HashSet<>();
      inbox.setMezzages(innnn);
      Folder outbox = folderService.create();
      outbox.setName("outbox");
      outbox.setOwner(resu);
      Collection<Mezzage> ouuuu = new HashSet<>();
      outbox.setMezzages(ouuuu);
      Folder spambox = folderService.create();
      spambox.setName("spambox");
      spambox.setOwner(resu);
      Collection<Mezzage> spaaaam = new HashSet<>();
      spambox.setMezzages(spaaaam);
      Folder trashBox = folderService.create();
      trashBox.setName("trashbox");
      trashBox.setOwner(resu);
      Collection<Mezzage> trashhh = new HashSet<>();
      trashBox.setMezzages(trashhh);
       Folder notificationBox = folderService.create();
       notificationBox.setName("notificationbox");
       notificationBox.setOwner(resu);
       Collection<Mezzage> notificBox = new HashSet<>();
       notificationBox.setMezzages(notificBox);
       folders.add(notificationBox);
      folders.add(inbox);
      folders.add(outbox);
      folders.add(spambox);
      folders.add(trashBox);
      resu.setFolders(folders);
      resu.setReceivedMezzages(received);
      resu.setSendedMezzages(sended);

      return resu;
   }


   public Collection<Actor> suspiciousActors() {
      Collection<Actor> res = null;
      LOG.info("Collecting suspicious Managers");
      try {
         Assert.isTrue(actorService.findByPrincipalHasRol("ADMINISTRATOR"), "The authenticated actor doesn't has ADMINISTRATOR rol");
         res = administratorRepository.suspiciousActors();
      } catch (Exception e) {
         e.printStackTrace();
      }

      return res;
   }



   public boolean banActor(Actor actor){
      boolean res = false;

      try {
         Assert.notNull(actor, "actor is null" + actor);
         Assert.isTrue(actorService.findByPrincipalHasRol("ADMINISTRATOR"), "The authenticated actor doesn't has ADMINISTRATOR rol");
         for(Authority at: actor.getUserAccount().getAuthorities()){
            if(at.getAuthority().equals("BAN")){
               throw new OperationNotSupportedException("The actor already banned");
            }
            }
         LOG.info("Banning actor " + actor);
         Authority authority = new Authority();
         authority.setAuthority("BAN");
         actor.getUserAccount().getAuthorities().add(authority);
         LOG.info("Actor banning sucessfully ");
         res = true;
      } catch (OperationNotSupportedException e) {
         e.printStackTrace();
      }

      return res;
      }

   public boolean unbanActor(Actor actor){
      boolean res = false;

      try {
         Assert.notNull(actor, "actor is null" + actor);
         Assert.isTrue(actorService.findByPrincipalHasRol("ADMINISTRATOR"), "The authenticated actor doesn't has ADMINISTRATOR rol");
         for(Authority at: actor.getUserAccount().getAuthorities()){
            if(at.getAuthority().equals("BAN")){
               LOG.info("Unbanning actor " + actor);
               actor.getUserAccount().getAuthorities().remove(at);
               LOG.info("Actor unbanning sucessfully");
               res = true;
               break;
            }else {
               throw new OperationNotSupportedException("The actor is not banned");

            }
         }

      } catch (OperationNotSupportedException e) {
         e.printStackTrace();
      }

      return res;
   }

   public LegalText createLegalText(){
      LegalText res = null;

      try {
         LOG.info("Creating LegalText...");
         Assert.isTrue(actorService.findByPrincipalHasRol("ADMINISTRATOR"),"The authenticated actor doesn't has ADMINISTRATOR rol");
         res = legalTextService.create();
      } catch (Exception e) {
         e.printStackTrace();
      }

      return res;
   }

    public boolean editLegalText(LegalText legalText){
      Assert.notNull(legalText);
      boolean res=false;

         Assert.isTrue(actorService.findByPrincipalHasRol("ADMINISTRATOR"), "The authenticated actor doesn't has ADMINISTRATOR rol");
         try{
            if (legalText.getStatus().equals(LegalStatus.DRAFT)) {
            legalTextService.save(legalText);
            LOG.info("LegalText succesfully modified");
            res = true;
         }else{
            throw new NoSuchElementException("The Legal Text is already published and can't be edit");
         }
      } catch (NoSuchElementException e) {
         e.printStackTrace();
      }

       return res;
    }



    public boolean deletLegalText(LegalText legalText){
       Assert.notNull(legalText);

       boolean res=false;

       Assert.isTrue(actorService.findByPrincipalHasRol("ADMINISTRATOR"),"The authenticated actor doesn't has ADMINISTRATOR rol");
       try {
          if (legalText.getStatus().equals(LegalStatus.DRAFT)) {
             legalTextService.delete(legalText);
             LOG.info("LegalText succesfully delete");
             res = true;
          }else{
             throw new NoSuchElementException("The Legal Text is already published and can't be delete");
          }
       } catch (NoSuchElementException e) {
          e.printStackTrace();
       }

       return res;
    }

    public Tag createTag(){
       Tag res = null;

       try {
          LOG.info("Creating tag...");
          Assert.isTrue(actorService.findByPrincipalHasRol("ADMINISTRATOR"),"The authenticated actor doesn't has ADMINISTRATOR rol");
          res = tagService.create();
       } catch (Exception e) {
          e.printStackTrace();
       }

       return res;
    }

   public boolean editTags(Tag tag){

      boolean res=false;
      Assert.notNull(tag, "trip is null" + tag);

      Assert.isTrue(actorService.findByPrincipalHasRol("ADMINISTRATOR"),"The authenticated actor doesn't has ADMINISTRATOR rol");
       try{
          if(!administratorRepository.findNotUsedTag().contains(tag)){
             tagService.save(tag);
             LOG.info("tag succesfully modified");
             res = true;
          }else{
             throw new NoSuchElementException("El  tag no esta guardado");
          }
       } catch (NoSuchElementException e) {
          e.printStackTrace();
       }

      return res;
   }



   public Category createCategory(){
      Category res = null;

      try {
         LOG.info("Creating Category...");
         Assert.isTrue(actorService.findByPrincipalHasRol("ADMINISTRATOR"),"The authenticated actor doesn't has ADMINISTRATOR rol");
         res = categoryService.create();
      } catch (Exception e) {
         e.printStackTrace();
      }

      return res;
   }

   public boolean editCategory(Category category){
      Assert.notNull(category);
      boolean res=false;

      Assert.isTrue(actorService.findByPrincipalHasRol("ADMINISTRATOR"), "The authenticated actor doesn't has ADMINISTRATOR rol");
      try{
            categoryService.save(category);
            LOG.info("category succesfully modified");
            res = true;

      } catch (NoSuchElementException e) {
         e.printStackTrace();
      }

      return res;
   }



   public boolean deleteCategory(Category category){
      Assert.notNull(category);
      boolean res=false;
      Assert.isTrue(actorService.findByPrincipalHasRol("ADMINISTRATOR"),"The authenticated actor doesn't has ADMINISTRATOR rol");
      try {
            categoryService.delete(category);
            LOG.info("category succesfully delete");
            res = true;

      } catch (NoSuchElementException e) {
         e.printStackTrace();
      }

      return res;
   }

   public boolean storeBroadCastMessage(Mezzage m){
      boolean res = false;

      try {
         LOG.info("Saving broadcast Message " + m);
         Assert.notNull(m);
         Assert.isTrue(actorService.findByPrincipalHasRol("ADMINISTRATOR"), "The authenticated actor doesn't has ADMINISTRATOR rol");
         List<AdministratorUtils> administratorUtils =  new ArrayList<>(utilservice.findAll());
         AdministratorUtils administratorUtils1 = administratorUtils.get(0);
         administratorUtils1.getBroadcastedMezzanges().add(m);
         utilservice.save(administratorUtils1);

         LOG.info("Broadcast message succesfully created");
         res = true;
      } catch (Exception e) {
         e.printStackTrace();
      }

      return res;
   }

    public Boolean banUser(Actor user) {
        Boolean res = false;
        if (user.getUserAccount().isEnabled()) {
            Authority authority2 = new Authority();
            authority2.setAuthority("BAN");
            user.getUserAccount().addAuthority(authority2);
            user.setBanned(true);
            res = true;
        }
        return res;
    }

    public Boolean unbanUser(Actor user) {
        Boolean res = false;
        if (user.getUserAccount().isEnabled()) {
            Authority authority2 = new Authority();
            authority2.setAuthority("BAN");
            user.getUserAccount().removeAuthority(authority2);
            user.setBanned(false);
            res = true;

        }
        return res;
    }


   public void flush() {
      administratorRepository.flush();
   }


   //DASHBOARD

    //*************************** LEVEL C ***************************


    //The average, the minimum, the maximum, and the standard deviation of the number of applications per trip.@Query("select avg(t.tripApplications.size) from Trip t"))

    public Double averageNumberOfApplicationsPerTrip() {
        return administratorRepository.averageNumberOfApplicationsPerTrip();
    }

    public int maximumNumberOfAppicationsPerTrip() {
        return administratorRepository.maximumNumberOfAppicationsPerTrip();
    }

    public int minimumNumberOfApplicationsPerTrip() {
        return administratorRepository.minimumNumberOfApplicationsPerTrip();
    }

    public Double standardDeviationOfTheNumberOfApplicationsPerTrip() {
        return administratorRepository.standardDeviationOfTheNumberOfApplicationsPerTrip();
    }


    public Double averageNumberOfTripsManagerPerManager() {
        return administratorRepository.averageNumberOfTripsManagerPerManager();
    }


    public int maximumNumberOfTripsManagerPerManager() {
        return administratorRepository.maximumNumberOfTripsManagerPerManager();
    }


    public int minimumNumberOfTripsManagerPerManager() {
        return administratorRepository.minimumNumberOfTripsManagerPerManager();
    }


    public Double standartDeviationOfNumberOfTripsManagerPerManager() {
        return administratorRepository.standartDeviationOfNumberOfTripsManagerPerManager();
    }



    public Double averageNumberOfTripsPrice() {
        return administratorRepository.averageNumberOfTripsPrice();
    }


    public int maximumNumberOfTripsPrice() {
        return administratorRepository.maximumNumberOfTripsPrice();
    }


    public int minimumNumberOfTripsPrice() {
        return administratorRepository.minimumNumberOfTripsPrice();
    }


    public Double standartDeviationOfNumberOfTripsPrice() {
        return administratorRepository.standartDeviationOfNumberOfTripsPrice();
    }


    // The average, the minimum, the maximum, and the standard deviation of the number trips guided per ranger.

    public Double averageNumberOfTripsGuidedPerRanger() {
        return administratorRepository.averageNumberOfTripsGuidedPerRanger();
    }


    public int maximumNumberOfTripsGuidedPerRanger() {
        return administratorRepository.maximumNumberOfTripsGuidedPerRanger();
    }


    public int minimumNumberOfTripsGuidedPerRanger() {
        return administratorRepository.minimumNumberOfTripsGuidedPerRanger();
    }

    public Double standartDeviationOfNumberOfTripsGuidedPerRanger() {
        return administratorRepository.standartDeviationOfNumberOfTripsGuidedPerRanger();
    }


    // The ratio of applications with status ?PENDING?.
    // El ratio se calcula posteriormente en java con los datos devueltos por las siguientes queries

    public Collection<TripApplication> numberOfApplications() {
        return administratorRepository.numberOfApplications();
    }


    public Double numberOfAppsWithStatusPending() {
        return new Double(administratorRepository.numberOfAppsWithStatusPending().size());
    }

    public Double ratioPendingVsAll() {
        Double res = numberOfAppsWithStatusPending() / numberOfApplications().size();
        return res;
    }

    //The ratio of applications with status ?DUE?.

    public Double numberOfAppsWithStatusDue() {

       return new Double(administratorRepository.numberOfAppsWithStatusDue().size());
    }

    public Double ratioPendingVsDue() {
        Double res = numberOfAppsWithStatusDue() / numberOfApplications().size();
        return res;
    }
    // The ratio of applications with status ?ACCEPTED?.

    public Double numberOfAppsWithStatusAccepted() {

       return new Double(administratorRepository.numberOfAppsWithStatusAccepted().size());
    }

    public Double ratioPendingVsAccepted() {
        Double res = numberOfAppsWithStatusAccepted() / numberOfApplications().size();
        return res;
    }
    // The ratio of applications with status ?CANCELLED?.

    public Double numberOfAppsWithStatusCancelled() {
        return new Double(administratorRepository.numberOfAppsWithStatusCancelled().size());
    }

    public Double ratioPendingVsCancelled() {
        Double res = numberOfAppsWithStatusCancelled() / numberOfApplications().size();
        return res;
    }

// The ratio of trips that have been cancelled versus the total number of trips that have been organised.
    // El ratio se calcula posteriormente en java con los datos devueltos por las siguientes queries


    public Double numberOfCancelledTrips() {
        return
                new Double(administratorRepository.numberOfCancelledTrips().size());
    }


    public Double numberOfTrips() {
        return new Double(administratorRepository.numberOfTrips().size());
    }

    public Double ratioCancelledTrips() {
        Double res = numberOfCancelledTrips() / numberOfTrips();
        return res;
    }


    //The listing of trips that have got at least 10% more applications than the av-erage, ordered by number of applications.

    public Collection<Trip> tripsThatHaveGotAtLeast10PercMoreApplicationsThanTheAverageOrdereByNumberOfApplications() {
        return administratorRepository.tripsThatHaveGotAtLeast10PercMoreApplicationsThanTheAverageOrdereByNumberOfApplications();

    }

    // A table with the number of times that each legal text?s been referenced.

    public Map<LegalText, Long> numberOfTimesEachLegalTextHasBeenReferenced() {

        List<Object[]> resultList = administratorRepository.numberOfTimesEachLegalTextHasBeenReferenced();
       Map<LegalText, Long> resultMap = new HashMap<>(resultList.size());

        for (Object[] result : resultList){
            resultMap.put((LegalText) result[0], (Long) result[1]);
        }
       return resultMap;

    }


//*************************** LEVEL B ***************************

    //The minimum, the maximum, the average, and the standard deviation of the number of notes per trip.

    public Double averageNumberOfNotesPerTrip() {
        return administratorRepository.averageNumberOfNotesPerTrip();
    }


    public int maximumNumberOfNotesPerTrip() {
        return administratorRepository.maximumNumberOfNotesPerTrip();
    }


    public int minimumNumberOfNotesPerTrip() {
        return administratorRepository.minimumNumberOfNotesPerTrip();
    }


    public Double standartDeviationOfNotesPerTrip() {
        return administratorRepository.standartDeviationOfNotesPerTrip();
    }

    // The minimum, the maximum, the average, and the standard deviation of audit records per trip.

    public Double averageNumberOfAuditsPerTrip() {
        return administratorRepository.averageNumberOfAuditsPerTrip();
    }


    public int maxNumberOfAuditsPerTrip() {
        return administratorRepository.maxNumberOfAuditsPerTrip();
    }


    public int minNumberOfAuditsPerTrip() {
        return administratorRepository.minNumberOfAuditsPerTrip();
    }


    public Double standartDeviationOfAuditsPerTrip() {
        return administratorRepository.standartDeviationOfAuditsPerTrip();
    }


    // The ratio of trips with an audit record.
    // Se usa la query anterior que devuelve todos los Trip

    public Integer numberOfTripsWithoutAudits() {
        return administratorRepository.numberOfTripsWithoutAudits().size();
    }

    public Double ratioOfTripsWithAnAuditRecord() {
        return numberOfTripsWithoutAudits() / numberOfTrips();
    }

    // The ratio of rangers who have registered their curricula.
// El ratio se calcula posteriormente en java con los datos devueltos por las siguientes queries

    public Double numberOfRangers() {
        return new Double(administratorRepository.numberOfCancelledTrips().size());
    }


    public int numberOfRangersWithoutCurricula() {
        return administratorRepository.numberOfRangersWithoutCurricula().size();
    }

    public Double ratioOfRangersWhoHasntRegisteredTheirCurricula() {
        return numberOfRangersWithoutCurricula() / numberOfRangers();
    }

    // The ratio of rangers whose curriculum?s been endorsed.

    public Double rangerWithoutEndorsedCurricula() {
        return new Double(administratorRepository.rangerWithoutEndorsedCurricula().size());
    }

    public Double ratioOfRangersWithoutEndorsedCurricula() {
        return rangerWithoutEndorsedCurricula() / numberOfRangers();
    }

    // The ratio of suspicious managers.

    public int numberOfManagers() {
        return administratorRepository.numberOfManagers().size();
    }


    public int numerOfSuspiciousManagers() {
        return administratorRepository.numerOfSuspiciousManagers().size();
    }

    public int ratioOfSuspiciousManagers() {
        return numerOfSuspiciousManagers() / numberOfManagers();
    }
//
//     The ratio of suspicious rangers.
//
public Double numberOfSuspiciousRangers() {
    return new Double(administratorRepository.numberOfSuspiciousRangers().size());
}

    public Double ratioOfSuspiciousRangers() {
        return numberOfSuspiciousRangers() / numberOfRangers();
    }
}
