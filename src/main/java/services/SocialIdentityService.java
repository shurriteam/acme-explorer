/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.Search;
import domain.SocialIdentity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.SocialIdentityRepository;

import java.util.Collection;

@Service
@Transactional
public class SocialIdentityService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private SocialIdentityRepository socialIdentityRepository;

   // Managed repository--------------------------------------------------------------------------------

   public SocialIdentityService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public SocialIdentity create() {
      SocialIdentity res;
      res = new SocialIdentity();
      return res;
   }

   public Collection<SocialIdentity> findAll() {
      Collection<SocialIdentity> res;
      res = socialIdentityRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public SocialIdentity findOne(int Cate) {
      SocialIdentity res;
      res = socialIdentityRepository.findOne(Cate);
      Assert.notNull(res);
      return res;
   }

   public SocialIdentity save(SocialIdentity a) {
      Assert.notNull(a);
      SocialIdentity res;
      res = socialIdentityRepository.save(a);
      return res;

   }

   public void delete(SocialIdentity a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      socialIdentityRepository.delete(a);
   }


   // Other business methods -------------------------------------------------------------------------------
   public void flush() {
      socialIdentityRepository.flush();
   }
}



