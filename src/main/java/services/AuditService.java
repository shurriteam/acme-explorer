/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;


import domain.Attachment;
import domain.Audit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.AttachmentRepository;
import repositories.AuditRepository;

import java.util.Collection;

@Service
@Transactional
public class AuditService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private AuditRepository auditRepository;

   // Managed repository--------------------------------------------------------------------------------

   public AuditService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public Audit create() {
      Audit res;
      res = new Audit();
      return res;
   }

   public Collection<Audit> findAll() {
      Collection<Audit> res;
      res = auditRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public Audit findOne(int Cate) {
      Audit res;
      res = auditRepository.findOne(Cate);
      Assert.notNull(res);
      return res;
   }

   public Audit save(Audit a) {
      Assert.notNull(a);
      Audit res;
      res = auditRepository.save(a);
      return res;

   }

   public void delete(Audit a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      auditRepository.delete(a);
   }

   // Other business methods -------------------------------------------------------------------------------
   public void flush() {
      auditRepository.flush();
   }

}



