/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.Requirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.RequirementRepository;

import java.util.Collection;

@Service
@Transactional
public class RequirementService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private RequirementRepository requirementRepository;

   // Managed repository--------------------------------------------------------------------------------

   public RequirementService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public Requirement create() {
      Requirement res;
      res = new Requirement();
      return res;
   }

   public Collection<Requirement> findAll() {
      Collection<Requirement> res;
      res = requirementRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public Requirement findOne(int Cate) {
      Requirement res;
      res = requirementRepository.findOne(Cate);
      Assert.notNull(res);
      return res;
   }

   public Requirement save(Requirement a) {
      Assert.notNull(a);
      Requirement res;
      res = requirementRepository.save(a);
      return res;

   }

   public void delete(Requirement a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      requirementRepository.delete(a);
   }


   // Other business methods -------------------------------------------------------------------------------
   public void flush() {
      requirementRepository.flush();
   }
}



