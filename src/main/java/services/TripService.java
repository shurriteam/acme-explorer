/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.TripRepository;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Random;

import static org.hibernate.jpa.internal.QueryImpl.LOG;

@Service
@Transactional
public class TripService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private TripRepository tripRepository;

   // Managed repository--------------------------------------------------------------------------------

   public TripService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public Trip create() {
      Trip res;
      res = new Trip();
      return res;
   }

   public Collection<Trip> findAll() {
      Collection<Trip> res;
      res = tripRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public Trip findOne(int Cate) {
      Trip res;
      res = tripRepository.findOne(Cate);
      Assert.notNull(res);
      return res;
   }

   public Trip save(Trip a) {
      Assert.notNull(a);
      Trip res;
      res = tripRepository.save(a);
      return res;

   }

   public void delete(Trip a) {
      Assert.notNull(a);
      Assert.isTrue(!a.getPublicationDate().before(new Date(System.currentTimeMillis() - 100)) || a.isCancelled(), "Trip is already published or not cancelled / El viaje est� publicado o no se ha cancelado");
      //TODO Trycacht para la vista, para que cuando est� publicado o no cancelado te avise de eso
      Assert.isTrue(a.getId() != 0);
      tripRepository.delete(a);
   }

   // Other business methods -------------------------------------------------------------------------------
   public void flush() {
      tripRepository.flush();
   }




   public Curricula displayRangersCurriculumFromTrip(int tripId){
      Trip trip = tripRepository.findOne(tripId);
      Ranger ranger = trip.getGuide();
      Curricula res = ranger.getCurricula();
      Assert.notNull(res);
      return  res;
   }

   public Collection<Audit> displayAuditsFromTrip(int tripId){
      LOG.info("Retrieving Audits from Trip with id " + tripId);
      Collection<Audit> res = null;
      try {
         Assert.notNull(tripId, "Trip id null or empty " + tripId);
         res = tripRepository.getAuditByTrip(tripId);
      } catch (Exception e) {
         e.printStackTrace();
      }
   return res;

   }

   public Collection<Note> getNotesByTrip(int tripId){
      LOG.info("Retrieving Notes from Trip with id " + tripId);
      Collection<Note> res = null;
      try {
         Assert.notNull(tripId, "Trip id null or empty " + tripId);
         res = tripRepository.getNotesByTrip(tripId);
      } catch (Exception e) {
         e.printStackTrace();
      }
      return res;

   }

   public Collection<Trip> tripsByCategory(Category category){
      Assert.notNull(category);
      LOG.info("obteniendo viajes asociados a la categor�a " + category + ".");
      Collection<Trip> res = tripRepository.tripsGivenACategory(category);
      Assert.notNull(category,"Categor�a sin viajes");
      return res;
   }

   public Iterable<Trip> tripByPage(int pageNumber, int pageSize){

      PageRequest pageRequest = new PageRequest(pageNumber,pageSize);

      Iterable<Trip> res = tripRepository.findAll(pageRequest);

      return  res;
   }

   public Collection<Trip> showableTrip() {

      LOG.info("Retrieving showable trips");
      Collection<Trip> res = null;
      try {
         res = tripRepository.showableTrips();
      } catch (Exception e) {
         e.printStackTrace();
      }
      return res;

   }
   public String randomFourLettersGenerator(){

      char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
      StringBuilder sb = new StringBuilder();
      Random random = new Random();
      for (int i = 0; i < 4; i++) {
         char c = chars[random.nextInt(chars.length)];
         sb.append(c);
      }
      String output = sb.toString();


      return  output;
   }
   public String tickerGenerator(Trip trip){
      Date pub = new Date(System.currentTimeMillis()-100);
      String letters = "-"+randomFourLettersGenerator();
      DateFormat dateFormat = new SimpleDateFormat("yyMMdd");
      String date = dateFormat.format(pub);
      String ticker = date+letters;
      return ticker;
   }

   public Collection<Explorer> explorersbyTrip(int tripId){

      Collection<Explorer> res = null;
      try {
         Assert.notNull(tripId);
         res = tripRepository.explorerByTrip(tripId);
      } catch (Exception e) {
         e.printStackTrace();
      }
      return res;
   }

    public Collection<Explorer> explorersInSv(Trip t) {

        Collection<Explorer> explorers = null;

        try {
            Assert.notNull(t);
            explorers = tripRepository.explorerEnrolledInSC(t);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return explorers;

    }
}



