/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;


import domain.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.LocationRepository;

import java.util.Collection;

@Service
@Transactional
public class LocationService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private LocationRepository locationRepository;

   // Managed repository--------------------------------------------------------------------------------

   public LocationService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public Location create() {
      Location res;
      res = new Location();
      return res;
   }

   public Collection<Location> findAll() {
      Collection<Location> res;
      res = locationRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public Location findOne(int Cate) {
      Location res;
      res = locationRepository.findOne(Cate);
      Assert.notNull(res);
      return res;
   }

   public Location save(Location a) {
      Assert.notNull(a);
      Location res;
      res = locationRepository.save(a);
      return res;

   }

   public void delete(Location a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      locationRepository.delete(a);
   }

   // Other business methods -------------------------------------------------------------------------------

}



