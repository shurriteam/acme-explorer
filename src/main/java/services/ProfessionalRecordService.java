/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;


import domain.PersonalRecord;
import domain.ProfessionalRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.ProfessionalRecordRepository;

import java.util.Collection;

@Service
@Transactional
public class ProfessionalRecordService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private ProfessionalRecordRepository professionalRecordRepo;

   // Managed repository--------------------------------------------------------------------------------

   public ProfessionalRecordService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public ProfessionalRecord create() {
      ProfessionalRecord res;
      res = new ProfessionalRecord();
      return res;
   }

   public Collection<ProfessionalRecord> findAll() {
      Collection<ProfessionalRecord> res;
      res = professionalRecordRepo.findAll();
      Assert.notNull(res);
      return res;
   }

   public ProfessionalRecord findOne(int Cate) {
      ProfessionalRecord res;
      res = professionalRecordRepo.findOne(Cate);
      Assert.notNull(res);
      return res;
   }

   public ProfessionalRecord save(ProfessionalRecord a) {
      Assert.notNull(a);
      ProfessionalRecord res;
      res = professionalRecordRepo.save(a);
      return res;

   }

   public void delete(ProfessionalRecord a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      professionalRecordRepo.delete(a);
   }


   // Other business methods -------------------------------------------------------------------------------
   public void flush() {
      professionalRecordRepo.flush();
   }
}



