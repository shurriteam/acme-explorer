/*
 * Copyright © 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.AdministratorUtils;
import domain.Explorer;
import domain.Search;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.SearchRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class SearchService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private SearchRepository searchRepository;
   @Autowired
   private Utilservice utilservice;

   // Managed repository--------------------------------------------------------------------------------

   public SearchService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public Search create() {
      Search res;
      res = new Search();
      return res;
   }

   public Collection<Search> findAll() {
      Collection<Search> res;
      res = searchRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public Search findOne(int Cate) {
      Search res;
      res = searchRepository.findOne(Cate);
      Assert.notNull(res);
      return res;
   }

   public Search save(Search a) {
      Assert.notNull(a);
      Search res;
      res = searchRepository.save(a);
      return res;

   }

   public void delete(Search a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      searchRepository.delete(a);
   }


   // Other business methods -------------------------------------------------------------------------------
   public void flush() {
      searchRepository.flush();
   }


//   public Collection<Trip> searcher(String keyword){
//
//      DatabaseUtil databaseUtil;
//      Collection<Trip> res = new ArrayList<>();
//      try {
//
//         databaseUtil = new DatabaseUtil();
//         databaseUtil.initialise();
//         LOG.info("buscando la información relativa a la weyword: "+keyword +", en nuestro sistema");
//         databaseUtil.setReadUncommittedIsolationLevel();
//         res = interpretLine(keyword, databaseUtil.getEntityManager());
//
//      } catch (final Throwable oops) {
//         ThrowablePrinter.print(oops);
//
//      }
//      return res;
//   }

//   private static Collection<Trip> interpretLine(String read, EntityManager entityManager) {
//      FullTextEntityManager fullTextEntityManager =
//              org.hibernate.search.jpa.Search.getFullTextEntityManager(entityManager);
//      QueryBuilder qb = fullTextEntityManager.getSearchFactory()
//              .buildQueryBuilder().forEntity(Trip.class).get();
//      org.apache.lucene.search.Query luceneQuery = qb
//              .keyword()
//              .onFields("title", "ticker", "description")
//              .matching(read)
//              .createQuery();
//
//      javax.persistence.Query jpaQuery =
//              fullTextEntityManager.createFullTextQuery(luceneQuery, Trip.class);
//      List<Trip> rst = jpaQuery.getResultList();
//      Set rst2 = new HashSet<Trip>();
//      rst2.addAll(rst);
//      return rst2;
//
//   }

   public Collection<Search> searcherByExplorer(Explorer explorer) {

      Collection<Search> searches = null;
      try {
         Assert.notNull(explorer);
         searches = searchRepository.searchByActor(explorer);
      } catch (Exception e) {
         e.printStackTrace();
      }
      return searches;
   }


   public Collection<Search> trunkateSearchByExplorer(Explorer explorer){
      Collection<Search> res = null;
      try {
         List<AdministratorUtils> administratorUtils = new ArrayList<>(utilservice.findAll());
         Date max =  new Date(System.currentTimeMillis()- administratorUtils.get(0).getSearchCache() * 24 * 3600 * 1000l );
         res = searchRepository.trunkatedSearchByExplorer(explorer,max);
      } catch (Exception e) {
         e.printStackTrace();
      }
      return res;


   }
}



