/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.SurvivalclassRepository;

import javax.naming.OperationNotSupportedException;

import static org.hibernate.jpa.internal.QueryImpl.LOG;
import java.util.Collection;
import java.util.Date;

@Service
@Transactional
public class SurvivalClassService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private SurvivalclassRepository survivalclassRepository;

   // Managed repository--------------------------------------------------------------------------------

   public SurvivalClassService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------
  @Autowired
   private ExplorerService explorerService;
   @Autowired
   private TripService tripService;
   @Autowired
   private ActorService actorService;
   @Autowired
   private StoryService storyService;
   // Simple CRUD method --------------------------------------------------------------------------------

   public SurvivalClass create() {
      SurvivalClass res;
      res = new SurvivalClass();
      return res;
   }

   public Collection<SurvivalClass> findAll() {
      Collection<SurvivalClass> res;
      res = survivalclassRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public SurvivalClass findOne(int Cate) {
      SurvivalClass res;
      res = survivalclassRepository.findOne(Cate);
      Assert.notNull(res);
      return res;
   }

   public SurvivalClass save(SurvivalClass a) {
      Assert.notNull(a);
      SurvivalClass res;
      res = survivalclassRepository.save(a);
      return res;

   }

   public void delete(SurvivalClass a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      survivalclassRepository.delete(a);
   }


   // Other business methods -------------------------------------------------------------------------------


   public boolean enrollSurvivalClass(int survivalClassId, int explorerId){

         LOG.info("Enrolling on the survival with id: "+survivalClassId+", the explorer with the following id:  " + explorerId);
         boolean res = false;

         try {
            Assert.notNull(survivalClassId, "id de la survival class nulo");
            Assert.notNull(explorerId, "id del explorer nulo");
            Assert.isTrue(actorService.findByPrincipalHasRol("EXPLORER"), "You're not an explorer");

             Explorer explorer  = explorerService.findOne(explorerId);
             Collection<SurvivalClass> survivalClasses = survivalclassRepository.survivalClassesFromExplorer(explorerId);
             SurvivalClass survivalClass = survivalclassRepository.findOne(survivalClassId);
             Assert.isTrue(survivalClass.getStartDate().after(new Date(System.currentTimeMillis()-1000)));
             TripApplication app=this.survivalclassRepository.findApplication(survivalClass.getTrip().getId(),explorerId);

             Assert.notNull(app);
             Assert.isTrue(app.getStatus().equals("ACCEPTED"));



            if (!(survivalClasses.contains(survivalClass))){
               explorer.getSurvivalClasses().add(survivalClass);
               explorerService.save(explorer);
               survivalClass.getExplorers().add(explorer);
               survivalclassRepository.save(survivalClass);
               LOG.info("Successfully enrolled");
               res = true;

            }else{
               throw new OperationNotSupportedException("La survival class no pertenece al viaje");
            }


         } catch (Exception e) {
            e.printStackTrace();
         }

         return res;

   }
//kaw

    public boolean desenrollSurvivalClass(int survivalClassId, int explorerId) {

        LOG.info("Enrolling on the survival with id: " + survivalClassId + ", the explorer with the following id:  " + explorerId);
        boolean res = false;

        try {
            Assert.notNull(survivalClassId, "id de la survival class nulo");
            Assert.notNull(explorerId, "id del explorer nulo");
            Assert.isTrue(actorService.findByPrincipalHasRol("EXPLORER"), "You're not an explorer");

            SurvivalClass survivalClass = survivalclassRepository.findOne(survivalClassId);
            Assert.isTrue(survivalClass.getStartDate().before(new Date(System.currentTimeMillis()-1000)));

            Explorer explorer=  explorerService.findOne(explorerId);
            survivalClass.getExplorers().remove(explorer);

            survivalclassRepository.save(survivalClass);
            explorer.getSurvivalClasses().remove(survivalClass);
            this.explorerService.save(explorer);
            LOG.info("Successfully desenrolled");

        } catch (Exception e) {
            e.printStackTrace();
        }

        return res;
    }



   public boolean writeAnStoryInAnyTrip(Story story, int tripId, int explorerId){

      LOG.info("Writing the story with id: "+ story.getId() +", the explorer with the following id:  " + explorerId);
      boolean res = false;

      try {
         Assert.notNull(story, "story nulo");
         Assert.notNull(tripId, "tripId nulo");
         Assert.notNull(explorerId, "id del explorer nulo");
         Assert.isTrue(actorService.findByPrincipalHasRol("EXPLORER"), "You're not an explorer");
         Explorer explorer  = explorerService.findOne(explorerId);
         Collection<Trip> trips = explorerService.tripsEndedInWithIveEnrolled(explorerId);
         Trip trip = tripService.findOne(tripId);

         if (trips.contains(trip)){
            story.setAssociatedTrip(trip);
            storyService.save(story);
            trip.getStories().add(story);
            tripService.save(trip);
         }else {
            throw new OperationNotSupportedException("O el viaje no ha acabado o no participa en �l (no pertenece a tus viajes)");
         }
            LOG.info("Successfully enrolled");
            res = true;

      } catch (Exception e) {
         e.printStackTrace();
      }

      return res;

   }




   public void flush() {
      survivalclassRepository.flush();
   }
}



