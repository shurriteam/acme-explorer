/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;


import domain.Note;
import domain.PersonalRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.PersonalRecordRepository;

import java.util.Collection;

@Service
@Transactional
public class PersonalRecordService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private PersonalRecordRepository personalRecordRepository;

   // Managed repository--------------------------------------------------------------------------------

   public PersonalRecordService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public PersonalRecord create() {
      PersonalRecord res;
      res = new PersonalRecord();
      return res;
   }

   public Collection<PersonalRecord> findAll() {
      Collection<PersonalRecord> res;
      res = personalRecordRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public PersonalRecord findOne(int Cate) {
      PersonalRecord res;
      res = personalRecordRepository.findOne(Cate);
      Assert.notNull(res);
      return res;
   }

   public PersonalRecord save(PersonalRecord a) {
      Assert.notNull(a);
      PersonalRecord res;
      res = personalRecordRepository.save(a);
      return res;

   }

   public void delete(PersonalRecord a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      personalRecordRepository.delete(a);
   }


   // Other business methods -------------------------------------------------------------------------------
   public void flush() {
      personalRecordRepository.flush();
   }
}



