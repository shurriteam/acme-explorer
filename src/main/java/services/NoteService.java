/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;


import domain.Note;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.NoteRepository;

import java.util.Collection;

@Service
@Transactional
public class NoteService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private NoteRepository noteRepository;

   // Managed repository--------------------------------------------------------------------------------

   public NoteService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public Note create() {
      Note res;
      res = new Note();
      return res;
   }

   public Collection<Note> findAll() {
      Collection<Note> res;
      res = noteRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public Note findOne(int Cate) {
      Note res;
      res = noteRepository.findOne(Cate);
      Assert.notNull(res);
      return res;
   }

   public Note save(Note a) {
         Note res;
         Assert.notNull(a);
         res = noteRepository.save(a);
         return res;

   }

   public void delete(Note a) {

      if (a.getRemark().isEmpty()) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      noteRepository.delete(a);
   }else{
      throw new IllegalArgumentException("If the note is edited it's impossible to erase it");
   }

}

   // Other business methods -------------------------------------------------------------------------------
   public void flush() {
      noteRepository.flush();
   }
}



