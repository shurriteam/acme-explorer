/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.Sponsor;
import domain.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.StageRepository;

import java.util.Collection;

@Service
@Transactional
public class StageService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private StageRepository stageRepository;

   // Managed repository--------------------------------------------------------------------------------

   public StageService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public Stage create() {
      Stage res;
      res = new Stage();
      return res;
   }

   public Collection<Stage> findAll() {
      Collection<Stage> res;
      res = stageRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public Stage findOne(int Cate) {
      Stage res;
      res = stageRepository.findOne(Cate);
      Assert.notNull(res);
      return res;
   }

   public Stage save(Stage a) {
      Assert.notNull(a);
      Stage res;
      res = stageRepository.save(a);
      return res;

   }

   public void delete(Stage a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      stageRepository.delete(a);
   }


   // Other business methods -------------------------------------------------------------------------------
   public void flush() {
      stageRepository.flush();
   }
}



