/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.Actor;
import domain.Folder;
import domain.Mezzage;
import domain.Sponsor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.SponsorRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import security.UserAccountService;

import java.util.Collection;
import java.util.HashSet;

@Service
@Transactional
public class SponsorService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private SponsorRepository sponsorRepository;

   // Managed repository--------------------------------------------------------------------------------

   public SponsorService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------
@Autowired
private UserAccountService userAccountService;
   @Autowired
   private FolderService folderService;
   // Simple CRUD method --------------------------------------------------------------------------------

   public Sponsor create() {
      Sponsor res;
      res = new Sponsor();
      return res;
   }

   public Collection<Sponsor> findAll() {
      Collection<Sponsor> res;
      res = sponsorRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public Sponsor findOne(int Cate) {
      Sponsor res;
      res = sponsorRepository.findOne(Cate);
      Assert.notNull(res);
      return res;
   }

   public Sponsor save(Sponsor a) {
      Assert.notNull(a);
      Sponsor res;
      res = sponsorRepository.save(a);
      return res;

   }

   public void delete(Sponsor a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      sponsorRepository.delete(a);
   }

   public Actor registerAsSponsor(Sponsor u) {
      Assert.notNull(u);
      Authority autoh = new Authority();
      autoh.setAuthority("SPONSOR");
      UserAccount res = new UserAccount();
      res.addAuthority(autoh);
      res.setUsername(u.getUserAccount().getUsername());
      Md5PasswordEncoder encoder;
      encoder = new Md5PasswordEncoder();
      String hash = encoder.encodePassword(u.getUserAccount().getPassword(), null);
      res.setPassword(hash);
      UserAccount userAccount = userAccountService.save(res);
      u.setUserAccount(userAccount);
      Assert.notNull(u.getUserAccount().getAuthorities(), "authorities null al registrar");
      Sponsor resu = sponsorRepository.save(u);
      Collection<Mezzage> received = new HashSet<>();
      Collection<Mezzage> sended = new HashSet<>();
      Collection<Folder> folders = new HashSet<>();
      Folder inbox = folderService.create();
      inbox.setName("inbox");
      inbox.setOwner(resu);
      Collection<Mezzage> innnn = new HashSet<>();
      inbox.setMezzages(innnn);
      Folder outbox = folderService.create();
      outbox.setName("outbox");
      outbox.setOwner(resu);
      Collection<Mezzage> ouuuu = new HashSet<>();
      outbox.setMezzages(ouuuu);
      Folder spambox = folderService.create();
      spambox.setName("spambox");
      spambox.setOwner(resu);
      Collection<Mezzage> spaaaam = new HashSet<>();
      spambox.setMezzages(spaaaam);
      Folder trashBox = folderService.create();
      trashBox.setName("trashbox");
      trashBox.setOwner(resu);
      Collection<Mezzage> trashhh = new HashSet<>();
      trashBox.setMezzages(trashhh);
       Folder notificationBox = folderService.create();
       notificationBox.setName("notificationbox");
       notificationBox.setOwner(resu);
       Collection<Mezzage> notificBox = new HashSet<>();
       notificationBox.setMezzages(notificBox);
       folders.add(notificationBox);
      folders.add(inbox);
      folders.add(outbox);
      folders.add(spambox);
      folders.add(trashBox);
      resu.setFolders(folders);
      resu.setReceivedMezzages(received);
      resu.setSendedMezzages(sended);

      return resu;
   }
   // Other business methods -------------------------------------------------------------------------------
   public void flush() {
      sponsorRepository.flush();
   }


   public Sponsor findByPrincipal() {
      Sponsor result;
      UserAccount userAccount;
      userAccount = LoginService.getPrincipal();
      Assert.notNull(userAccount);
      result = findByUserAccount(userAccount);
      Assert.notNull(result);
      return result;
   }

   private Sponsor findByUserAccount(UserAccount userAccount) {
      Assert.notNull(userAccount);

      Sponsor result;

      result = sponsorRepository.findByUserAccountId(userAccount.getId());

      return result;
   }
}



