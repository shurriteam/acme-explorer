/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.Explorer;
import domain.Sponsor;
import domain.Story;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.StoryRepository;

import java.util.Collection;

@Service
@Transactional
public class StoryService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private StoryRepository storyRepository;

   // Managed repository--------------------------------------------------------------------------------

   public StoryService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public Story create() {
      Story res;
      res = new Story();
      return res;
   }

   public Collection<Story> findAll() {
      Collection<Story> res;
      res = storyRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public Story findOne(int Cate) {
      Story res;
      res = storyRepository.findOne(Cate);
      Assert.notNull(res);
      return res;
   }

   public Story save(Story a) {
      Assert.notNull(a);
      Story res;
      res = storyRepository.save(a);
      return res;

   }

   public void delete(Story a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      storyRepository.delete(a);
   }


   // Other business methods -------------------------------------------------------------------------------
   public void flush() {
      storyRepository.flush();
   }

   public Collection<Story> storiesbyExplorer(Explorer explorer){
      Collection<Story> stories = null;
      try {
         Assert.notNull(explorer);
         stories =  storyRepository.storyByExplorer(explorer);
      } catch (Exception e) {
         e.printStackTrace();
      }

      return stories;

   }
}



