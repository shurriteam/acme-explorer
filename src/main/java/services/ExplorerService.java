/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.ExplorerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import security.UserAccountService;

import java.util.*;

import static org.hibernate.jpa.internal.QueryImpl.LOG;

@Service
@Transactional
public class ExplorerService {

    // Constructors--------------------------------------------------------------------------------------

    @Autowired
    private ExplorerRepository explorerRepository;
    @Autowired
    private UserAccountService userAccountService;
    @Autowired
    private FolderService folderService;

    @Autowired
    private ActorService actorService;
    @Autowired
    private TripApplicationService tripApplicationService;
    @Autowired
    private TripService tripService;
    @Autowired
    private CreditCardService creditCardService;

    @Autowired
    private AdministratorService administratorService;
   @Autowired
   private ManagerService managerService;
    @Qualifier("searchService")
    @Autowired
    private SearchService searchService;
    @Autowired
    private MezzageService mezzageService;


    // Managed repository--------------------------------------------------------------------------------


    // Suporting services --------------------------------------------------------------------------------


    public ExplorerService() {
        super();
    }


    // Simple CRUD method --------------------------------------------------------------------------------

    public Explorer create() {
        Explorer res = new Explorer();
        return res;
    }

    public Collection<Explorer> findAll() {
        Collection<Explorer> res = explorerRepository.findAll();
        Assert.notNull(res);
        return res;
    }

    public Explorer findOne(int User) {
        domain.Explorer res = explorerRepository.findOne(User);
        Assert.notNull(res);
        return res;
    }

    public Explorer save(Explorer a) {
        Assert.notNull(a);
        Explorer res = explorerRepository.save(a);
        return res;
    }

    public void delete(Explorer a) {
        Assert.notNull(a);
        Assert.isTrue(a.getId() != 0);
        explorerRepository.delete(a);

    }

    public void flush() {
        explorerRepository.flush();
    }

    // Other business methods -------------------------------------------------------------------------------


   public Explorer findByPrincipal() {
      Explorer result;
      UserAccount userAccount;
      userAccount = LoginService.getPrincipal();
      Assert.notNull(userAccount);
      result = findByUserAccount(userAccount);
      Assert.notNull(result);
      return result;
   }

   private Explorer findByUserAccount(UserAccount userAccount) {
      Assert.notNull(userAccount);
      Explorer result;
      result = explorerRepository.findByUserAccountId(userAccount.getId());
      return result;
   }

    public Actor registerAsExplorer(Explorer u) {
       Explorer resu = null;
       try {
          Assert.notNull(u);
          Authority autoh = new Authority();
          autoh.setAuthority("EXPLORER");
          UserAccount res = new UserAccount();
          res.addAuthority(autoh);
          res.setUsername(u.getUserAccount().getUsername());
          Md5PasswordEncoder encoder;
          encoder = new Md5PasswordEncoder();
          String hash = encoder.encodePassword(u.getUserAccount().getPassword(), null);
          res.setPassword(hash);
          UserAccount userAccount = userAccountService.save(res);
          u.setUserAccount(userAccount);
          Assert.notNull(u.getUserAccount().getAuthorities(), "authorities null al registrar");
          CreditCard c = creditCardService.create();
          resu = explorerRepository.save(u);
          c.setHolderName(resu.getName());
          resu.setCreditCard(c);

          //creditCardService.save(c);
          Collection<Mezzage> received = new HashSet<>();
          Collection<Mezzage> sended = new HashSet<>();
          Collection<Folder> folders = new ArrayList<>();
          Folder inbox = folderService.create();
          inbox.setName("inbox");
          inbox.setOwner(resu);
          Collection<Mezzage> innnn = new HashSet<>();
          inbox.setMezzages(innnn);
          Folder outbox = folderService.create();
          outbox.setName("outbox");
          outbox.setOwner(resu);
          Collection<Mezzage> ouuuu = new HashSet<>();
          outbox.setMezzages(ouuuu);
          Folder spambox = folderService.create();
          spambox.setName("spambox");
          spambox.setOwner(resu);
          Collection<Mezzage> spaaaam = new HashSet<>();
          spambox.setMezzages(spaaaam);
          Folder trashBox = folderService.create();
          trashBox.setName("trashbox");
          trashBox.setOwner(resu);
          Collection<Mezzage> trashhh = new HashSet<>();
          trashBox.setMezzages(trashhh);
          Folder notificationBox = folderService.create();
          notificationBox.setName("notificationbox");
          notificationBox.setOwner(resu);
          Collection<Mezzage> notificBox = new HashSet<>();
          notificationBox.setMezzages(notificBox);
          folders.add(notificationBox);
          folders.add(inbox);
          folders.add(outbox);
          folders.add(spambox);
          folders.add(trashBox);
          resu.setFolders(folders);
          resu.setReceivedMezzages(received);
          resu.setSendedMezzages(sended);
       } catch (Exception e) {
          e.printStackTrace();
       }

       return resu;
    }


   public Collection<Trip> tripsEndedInWithIveEnrolled(int explorerId) {
        Collection<Trip> trips = explorerRepository.tripsEndedInWithIveEnrolled(explorerId);
        return trips;
    }


   public Collection<TripApplication> applicationOfExplorer(int explorerId) {
        Collection<TripApplication> res = null;

        try {
            Assert.notNull(explorerId, "explorerId is null");
            Assert.isTrue(actorService.findByPrincipalHasRol("EXPLORER"));
            res = explorerRepository.dueApplications();
            Assert.notEmpty(res);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

   public Collection<TripApplication> allExplorersApplications(int explorerId) {
      Collection<TripApplication> res = new ArrayList<>();
      try {
         Assert.notNull(explorerId, "explorerId is null");
         res = explorerRepository.allExplorersApplications(explorerId);
         Assert.notEmpty(res);
      } catch (Exception e) {
         e.printStackTrace();
      }
      return res;
   }
public boolean AplyTrip(Trip trip, TripApplication tripApplication){

    Assert.notNull(trip);

    boolean res=false;
    Assert.notNull(trip, "trip is null" + trip);

    Assert.isTrue(actorService.findByPrincipalHasRol("EXPLORER"),"The authenticated actor doesn't has ADMINISTRATOR rol");
    Assert.isTrue(tripApplication.getOwner().equals(actorService.findByPrincipal()));
    try{
        if(!(trip.getTripApplications().contains(tripApplication))){

            trip.getTripApplications().add(tripApplication);
            tripApplication.setTrip(trip);
            tripApplicationService.save(tripApplication);
            tripService.save(trip);
            res = true;
            LOG.info("successfully applied for the trip");
        }
    } catch (Exception e) {
        e.printStackTrace();
    }

        return res;

    }

    public  Collection<TripApplication> myTripApplicationsFromStatus(Status status) {
      Explorer e = findByPrincipal();

        Collection<TripApplication> res = new ArrayList<>();
        try {
           Assert.isTrue(actorService.findByPrincipalHasRol("EXPLORER"),"The authenticated actor doesn't has EXPLORERS rol");
            Assert.notNull(e, "exlorer nulo");
            Assert.notNull(status, "status nuulo");
            res = explorerRepository.explorerApplicationsFromStatus(e.getId(), status);
            Assert.notEmpty(res, "el explorer no tiene applications con ese estatus");
            return res;
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    return res;
}



    public boolean cancelTripApplication(TripApplication tripApplication) {


        boolean res = false;
        Assert.notNull(tripApplication, "trip is null" + tripApplication);
        Date ahora = new Date(System.currentTimeMillis() - 1000);
        Assert.isTrue(actorService.findByPrincipalHasRol("EXPLORER"), "The authenticated actor doesn't has EXPLORERS rol");
        try {
            if (tripApplication.getTrip().getPublicationDate().before(ahora) && tripApplication.getStatus().equals(Status.ACCEPTED)) {
                tripApplication.setStatus(Status.CANCELLED);
                tripApplicationService.save(tripApplication);
                LOG.info("tripApplication successfully cancelled");
               sendNotifMessageFromTripAppChanges(tripApplication,Status.CANCELLED);
                res = true;
            } else {
                throw new NoSuchElementException("ApplicationTrip no cancelada");
            }
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }

        return res;

    }
    public boolean acceptTripApplication(TripApplication tripApplication){
      boolean res = false;
       try {
          Assert.notNull(tripApplication);
          Assert.notNull(findByPrincipal());
          Assert.isTrue(actorService.findByPrincipalHasRol("EXPLORER"));
          Assert.isTrue(findByPrincipal().getTripApplications().contains(tripApplication));
          Assert.isTrue(tripApplication.getStatus().equals(Status.DUE));
          Assert.isTrue(creditCardService.checkCreditCard(findByPrincipal().getCreditCard()));
          tripApplication.setStatus(Status.ACCEPTED);
          tripApplicationService.save(tripApplication);
          sendNotifMessageFromTripAppChanges(tripApplication,Status.ACCEPTED);


          LOG.info("TrippApplication succesfully accepted");
       } catch (Exception e) {
          e.printStackTrace();
       }
      return res;

    }

    private boolean sendNotifMessageFromTripAppChanges(TripApplication tripApplication, Status status){
       boolean res = false;
       try {
          Mezzage mezzage = mezzageService.create();
          mezzage.setRecipient(tripApplication.getOwner());
          Mezzage mezzage1 = mezzageService.create();
          mezzage1.setRecipient(tripApplication.getManager());
           mezzage.setSender(actorService.findByPrincipal());
           mezzage1.setSender(actorService.findByPrincipal());
          Date now = new Date(System.currentTimeMillis()-100);
          mezzage.setMoment(now);
          mezzage1.setMoment(now);
          mezzage.setSubject("Changes in " + tripApplication);
          mezzage1.setSubject("Changes in " + tripApplication);
          mezzage.setPriority(Priority.NEUTRAL);
          mezzage1.setPriority(Priority.NEUTRAL);
           mezzage.setSenderEmail(actorService.findByPrincipal().getEmail());
           mezzage1.setSenderEmail(actorService.findByPrincipal().getEmail());
          mezzage.setReceiverEmail(tripApplication.getOwner().getEmail());
          mezzage1.setReceiverEmail(tripApplication.getManager().getEmail());
          String body = "Status of TripApplication " + tripApplication+ " was changed to " + status + ".";
          mezzage.setBody(body);
          mezzage1.setBody(body);
            LOG.info("sending message to the manager and the explorer");
          mezzageService.textMessage(mezzage);
          mezzageService.textMessage(mezzage1);
          res = true;
       } catch (Exception e) {
          e.printStackTrace();
       }

      return res;
    }




    public boolean editSearch(Search search){
        boolean res = false;

        try {
            LOG.info("Editing search criteria...");
            Assert.isTrue(actorService.findByPrincipalHasRol("EXPLORER"));
            Assert.isTrue(findByPrincipal().getSearches().contains(search));
            searchService.save(search);
            LOG.info("Search criteria update sucesfully");
            res = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;

    }


    public boolean deleteSearch(Search search){
        boolean res = false;

        try {
            LOG.info("Editing search criteria...");
            Assert.isTrue(actorService.findByPrincipalHasRol("EXPLORER"));
            Assert.isTrue(findByPrincipal().getSearches().contains(search));
            searchService.delete(search);
            LOG.info("Search criteria deleted sucesfully");
            res = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;

    }

//    public Collection<Trip> makeSearchbyKeyword(Search search) {
//
//        Collection<Trip> res = null;
//
//        try {
//            LOG.info("Searching with keyowrd... " + search.getKeyWord());
//            Assert.isTrue(actorService.findByPrincipalHasRol("EXPLORER"));
//            Assert.isTrue(findByPrincipal().getSearches().contains(search));
//            res = searchService.searcher(search.getKeyWord());
//            return res;
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        LOG.info("Search is empty");
//        return res;
//    }



}




