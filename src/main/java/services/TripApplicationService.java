/*
 * Copyright © 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.TripapplicationRepository;

import javax.naming.OperationNotSupportedException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class TripApplicationService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private TripapplicationRepository tripapplicationRepository;
   @Autowired
   private TripService tripService;
   @Autowired
   private ActorService actorService;
   @Autowired
   private MezzageService mezzageService;

   // Managed repository--------------------------------------------------------------------------------

   public TripApplicationService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public TripApplication create() {
      TripApplication res;
      res = new TripApplication();
      return res;
   }

   public Collection<TripApplication> findAll() {
      Collection<TripApplication> res;
      res = tripapplicationRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public TripApplication findOne(int Cate) {
      TripApplication res;
      res = tripapplicationRepository.findOne(Cate);
      Assert.notNull(res);
      return res;
   }

   public TripApplication save(TripApplication a) {
      Assert.notNull(a);
      TripApplication res;
      res = tripapplicationRepository.save(a);
      return res;

   }

   public void delete(TripApplication a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      tripapplicationRepository.delete(a);
   }


   // Other business methods -------------------------------------------------------------------------------
   public void flush() {
      tripapplicationRepository.flush();
   }


   public boolean cancelTripApplication(TripApplication trip, String reason) {
      boolean res = false;
      Assert.notNull(trip, "trip nulo");
      Assert.notNull(reason, "reason nulo");
      Actor actorAutentificiado = actorService.findByPrincipal();

      try {
         Assert.isTrue(actorService.findByPrincipalHasRol("EXPLORER"));
         if(tripapplicationRepository.tripApplicationsWithAcceptedStatus(actorAutentificiado.getId()).contains(trip)){
            trip.setStatus(Status.CANCELLED);
            trip.setCancellReason(reason);
            tripapplicationRepository.save(trip);

         }else {
            throw new OperationNotSupportedException("El status de trip no es compatible con su cancelación, revise su estado");
         }
      } catch (OperationNotSupportedException e) {
         e.printStackTrace();
      }


      return res;


   }


   public TripApplication tripAExplorerTrip(Explorer explorer, Trip trip){
      TripApplication res = null;

      try {
         Assert.notNull(explorer);
         Assert.notNull(trip);
         res=tripapplicationRepository.tripAGivenTripExplorer(explorer,trip);
      } catch (Exception e) {
         e.printStackTrace();
      }

      return res;

   }
   
   public void sendChangeMezzage(Explorer explorer, Manager manager, Status status, TripApplication tripApplication){

      try {
         Assert.notNull(explorer);
         Assert.notNull(manager);
         Assert.notNull(status);

         List<Folder> explorerFolders = new ArrayList<>(explorer.getFolders());
         List<Folder> managerFolders = new ArrayList<>(manager.getFolders());

         Mezzage Emezzage = mezzageService.create();
         Emezzage.setPriority(Priority.NEUTRAL);
         Emezzage.setReceiverEmail(explorer.getEmail());
         Emezzage.setSenderEmail(manager.getEmail());
         Emezzage.setMoment(new Date(System.currentTimeMillis()-100));
         Emezzage.setSubject("Changes in your application " + tripApplication.toString());
         Emezzage.setBody("The status of your application " + tripApplication.toString() + " has change to " + String.valueOf(status));

         Emezzage.setSender(manager);
         Emezzage.setRecipient(explorer);
         Emezzage.setFolder(explorerFolders.get(4));
         explorerFolders.get(4).getMezzages().add(Emezzage);

         Mezzage Mmezzage = mezzageService.create();
         Mmezzage.setPriority(Priority.NEUTRAL);
         Mmezzage.setReceiverEmail(manager.getEmail());
         Mmezzage.setSenderEmail(manager.getEmail());
         Mmezzage.setMoment(new Date(System.currentTimeMillis()-100));
         Mmezzage.setSubject("Changes in application " + tripApplication.toString());
         Mmezzage.setBody("The status of  application " + tripApplication.toString() + " has change to " + String.valueOf(status));

         Mmezzage.setSender(manager);
         Mmezzage.setRecipient(manager);
         Mmezzage.setFolder(managerFolders.get(4));
         managerFolders.get(4).getMezzages().add(Mmezzage);
      } catch (Exception e) {
         e.printStackTrace();
      }


   }

}