/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.EducationRecord;
import domain.EmergencyContact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.EducationRecordRepository;
import repositories.EmergencyContactRepository;

import java.util.Collection;

/**
 * Created by daviddelatorre on 12/3/17.
 */
@Service
@Transactional
public class EmergencyContactService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private EmergencyContactRepository emergencyContactRepository;

   // Managed repository--------------------------------------------------------------------------------

   public EmergencyContactService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public EmergencyContact create() {
      EmergencyContact res;
      res = new EmergencyContact();
      return res;
   }

   public Collection<EmergencyContact> findAll() {
      Collection<EmergencyContact> res;
      res = emergencyContactRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public EmergencyContact findOne(int Banner) {
      EmergencyContact res;
      res = emergencyContactRepository.findOne(Banner);
      Assert.notNull(res);
      return res;
   }

   public EmergencyContact save(EmergencyContact a) {
      Assert.notNull(a);
      EmergencyContact res;
      res = emergencyContactRepository.save(a);
      return res;
   }

   public void delete(EmergencyContact a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      emergencyContactRepository.delete(a);
   }

   // Other business methods -------------------------------------------------------------------------------
   public void flush() {
      emergencyContactRepository.flush();
   }
}
