/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;


import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.AuditorRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import security.UserAccountService;

import javax.naming.OperationNotSupportedException;
import java.util.Collection;
import java.util.HashSet;
import java.util.NoSuchElementException;

import static org.hibernate.jpa.internal.QueryImpl.LOG;

@Service
@Transactional
public class AuditorService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private AuditorRepository auditorRepository;

   // Managed repository--------------------------------------------------------------------------------

   public AuditorService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------
   @Autowired
   private FolderService folderService;
   @Autowired
   private UserAccountService userAccountService;
   @Autowired
   private TripService tripService;
   @Autowired
   private AuditService auditService;
   // Simple CRUD method --------------------------------------------------------------------------------

   public Auditor create() {
      Auditor res;
      res = new Auditor();
      return res;
   }

   public Collection<Auditor> findAll() {
      Collection<Auditor> res;
      res = auditorRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public Auditor findOne(int Cate) {
      Auditor res;
      res = auditorRepository.findOne(Cate);
      Assert.notNull(res);
      return res;
   }

   public Auditor save(Auditor a) {
      Assert.notNull(a);
      Auditor res;
      res = auditorRepository.save(a);
      return res;

   }

   public void delete(Auditor a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      auditorRepository.delete(a);
   }



   public Auditor findByPrincipal() {
      Auditor result;
      UserAccount userAccount;
      userAccount = LoginService.getPrincipal();
      Assert.notNull(userAccount);
      result = findByUserAccount(userAccount);
      Assert.notNull(result);
      return result;
   }

   private Auditor findByUserAccount(UserAccount userAccount) {
      Assert.notNull(userAccount);

      Auditor result;

      result = auditorRepository.findByUserAccountId(userAccount.getId());

      return result;
   }


   // Other business methods -------------------------------------------------------------------------------
   public Actor registerAsAuditor(Auditor u) {

      LOG.info("Registering auditor " + u);
      Assert.notNull(u);
      Authority autoh = new Authority();
      autoh.setAuthority("EXPLORER");
      UserAccount res = new UserAccount();
      res.addAuthority(autoh);
      res.setUsername(u.getUserAccount().getUsername());
      Md5PasswordEncoder encoder;
      encoder = new Md5PasswordEncoder();
      LOG.info("Encoding password " + u.getUserAccount().getPassword());
      String hash = encoder.encodePassword(u.getUserAccount().getPassword(), null);
      res.setPassword(hash);
      UserAccount userAccount = userAccountService.save(res);
      u.setUserAccount(userAccount);
      Assert.notNull(u.getUserAccount().getAuthorities(), "authorities null al registrar");
      Auditor resu = auditorRepository.save(u);
      Collection<Mezzage> received = new HashSet<>();
      Collection<Mezzage> sended = new HashSet<>();
      Collection<Folder> folders = new HashSet<>();
      Folder inbox = folderService.create();
      inbox.setName("inbox");
      inbox.setOwner(resu);
      Collection<Mezzage> innnn = new HashSet<>();
      inbox.setMezzages(innnn);
      Folder outbox = folderService.create();
      outbox.setName("outbox");
      outbox.setOwner(resu);
      Collection<Mezzage> ouuuu = new HashSet<>();
      outbox.setMezzages(ouuuu);
      Folder spambox = folderService.create();
      spambox.setName("spambox");
      spambox.setOwner(resu);
      Collection<Mezzage> spaaaam = new HashSet<>();
      spambox.setMezzages(spaaaam);
      Folder trashBox = folderService.create();
      trashBox.setName("trashbox");
      trashBox.setOwner(resu);
      Collection<Mezzage> trashhh = new HashSet<>();
      trashBox.setMezzages(trashhh);
       Folder notificationBox = folderService.create();
       notificationBox.setName("notificationbox");
       notificationBox.setOwner(resu);
       Collection<Mezzage> notificBox = new HashSet<>();
       notificationBox.setMezzages(notificBox);
       folders.add(notificationBox);
      folders.add(inbox);
      folders.add(outbox);
      folders.add(spambox);
      folders.add(trashBox);
      resu.setFolders(folders);
      resu.setReceivedMezzages(received);
      resu.setSendedMezzages(sended);

      return resu;
   }

   public Collection<Note> getNotesByAuditor(int auditorId){
      LOG.info("Retrieving Notes from Auditor with id " + auditorId);
      Collection<Note> res = null;
      try {
         Assert.notNull(auditorId, "auditorId  null or empty " + auditorId);
         res = auditorRepository.getNotesByAuditor(auditorId);
      } catch (Exception e) {
         e.printStackTrace();
      }
      return res;

   }

   public boolean writeNoteForTrip(Note note, Trip trip){
      boolean res = false;
      Assert.notNull(note, "note is null" + note);
      Assert.notNull(trip, "reply is null" + trip);

      try {
         LOG.info("Adding note " + note + "to Trip " + trip);
         trip.getNotes().add(note);
         tripService.save(trip);
         LOG.info("Note succesfully added");
         res = true;
      } catch (Exception e) {
         e.printStackTrace();
      }
      return res;
   }

   public Collection<Audit> getAuditionsByAuditor(int auditorId){
      LOG.info("Retrieving Audits from Auditor with id " + auditorId);
      Collection<Audit> res = null;
      try {
         Assert.notNull(auditorId, "auditorId null or empty " + auditorId);
         res = auditorRepository.getAuditionsByAuditor(auditorId);
      } catch (Exception e) {
         e.printStackTrace();
      }
      return res;

   }

   public boolean writeAuditForTrip(Audit audit, Trip trip){
      boolean res = false;


      try {
         LOG.info("Adding audit " + audit + "to Trip " + trip);
         Assert.notNull(audit, "note is null" + audit);
         Assert.notNull(trip, "reply is null" + trip);
         audit.setAuditStatus(AuditStatus.PUBLISHED);
         audit.setTrip(trip);
         auditService.save(audit);
         trip.getAudits().add(audit);
         tripService.save(trip);
         LOG.info("Audit succesfully added");
         res = true;
      } catch (Exception e) {
         e.printStackTrace();
      }
      return res;
   }

   /**
    * Modifica una auditoria, lo �nico que hace es guardar la auditor�a
    *
    * @param audit Auditor�a a modificar
    * @param auditorId Id del auditor que esta intentando modificar la auditoria.
    * @return .....
    */
   public boolean modifyAudit(Audit audit, int auditorId){
      boolean res = false;
      Assert.notNull(audit, "audit is null" + audit);

      try {
         if(getAuditionsByAuditor(auditorId).contains(audit) && audit.getAuditStatus().equals(AuditStatus.DRAFT)){
            LOG.info("Modifying audit " + audit);
            auditService.save(audit);
            LOG.info("Audit succesfully modified");
            res = true;
         }else{
            throw new NoSuchElementException("The "+audit + " does not belong to this auditor or is already published");
         }
      } catch (NoSuchElementException e) {
         e.printStackTrace();
      }

      return res;
   }


   public boolean deleteAudit(Audit audit, Trip trip){
      boolean res= false;
      Assert.notNull(audit, "audit is null" + audit);
      Assert.notNull(trip, "trip is null" + trip);

      try {
         if (audit.getAuditStatus().equals(AuditStatus.DRAFT)){
            LOG.info("Audit status is DRAFT, removing audit " + audit + "from Trip " + trip);
            audit.setTrip(null);
            auditService.save(audit);
            trip.getAudits().remove(audit);
            tripService.save(trip);
            LOG.info("Audit succesfully removed");
            res = true;
         }else {
            throw new OperationNotSupportedException("Audit is already published, it can't be delete");
         }
      } catch (OperationNotSupportedException e) {
         e.printStackTrace();
      }


      return res;
   }



}



