/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;


import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.RangerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import security.UserAccountService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import static org.hibernate.jpa.internal.QueryImpl.LOG;

@Service
@Transactional
public class RangerService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private RangerRepository rangerRepository;

   // Managed repository--------------------------------------------------------------------------------

   public RangerService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------
@Autowired
private UserAccountService userAccountService;
   @Autowired
   private FolderService folderService;
   @Autowired
   private CurriculaService curriculaService;
   // Simple CRUD method --------------------------------------------------------------------------------

   public Ranger create() {
      Ranger res;
      res = new Ranger();
      return res;
   }

   public Collection<Ranger> findAll() {
      Collection<Ranger> res;
      res = rangerRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public Ranger findOne(int Cate) {
      Ranger res;
      res = rangerRepository.findOne(Cate);
      Assert.notNull(res);
      return res;
   }

   public Ranger save(Ranger a) {
      Assert.notNull(a);
      Ranger res;
      res = rangerRepository.save(a);
      return res;

   }

   public void delete(Ranger a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      rangerRepository.delete(a);
   }




   public Ranger findByPrincipal() {
      Ranger result;
      UserAccount userAccount;
      userAccount = LoginService.getPrincipal();
      Assert.notNull(userAccount);
      result = findByUserAccount(userAccount);
      Assert.notNull(result);
      return result;
   }

   private Ranger findByUserAccount(UserAccount userAccount) {
      Assert.notNull(userAccount);

      Ranger result;

      result = rangerRepository.findByUserAccountId(userAccount.getId());

      return result;
   }



   public Actor registerAsRanger(Ranger u) {
      Assert.notNull(u);
      Authority autoh = new Authority();
      autoh.setAuthority("RANGER");
      UserAccount res = new UserAccount();
      res.addAuthority(autoh);
      res.setUsername(u.getUserAccount().getUsername());
      Md5PasswordEncoder encoder;
      encoder = new Md5PasswordEncoder();
      String hash = encoder.encodePassword(u.getUserAccount().getPassword(), null);
      res.setPassword(hash);
      UserAccount userAccount = userAccountService.save(res);
      u.setUserAccount(userAccount);
      Assert.notNull(u.getUserAccount().getAuthorities(), "authorities null al registrar");
      Ranger resu = rangerRepository.save(u);
      Collection<Mezzage> received = new ArrayList<>();
      Collection<Mezzage> sended = new ArrayList<>();
      Collection<Folder> folders = new ArrayList<>();
      Folder inbox = folderService.create();
      inbox.setName("inbox");
      inbox.setOwner(resu);
      Collection<Mezzage> innnn = new ArrayList<>();
      inbox.setMezzages(innnn);
      Folder outbox = folderService.create();
      outbox.setName("outbox");
      outbox.setOwner(resu);
      Collection<Mezzage> ouuuu = new ArrayList<>();
      outbox.setMezzages(ouuuu);
      Folder spambox = folderService.create();
      spambox.setName("spambox");
      spambox.setOwner(resu);
      Collection<Mezzage> spaaaam = new ArrayList<>();
      spambox.setMezzages(spaaaam);
      Folder trashBox = folderService.create();
      trashBox.setName("trashbox");
      trashBox.setOwner(resu);
      Collection<Mezzage> trashhh = new ArrayList<>();
      trashBox.setMezzages(trashhh);
      Folder notificationBox = folderService.create();
      notificationBox.setName("notificationBox");
      notificationBox.setOwner(resu);
      Collection<Mezzage> notificBox = new ArrayList<>();
      notificationBox.setMezzages(notificBox);
      folders.add(notificationBox);
      folders.add(inbox);
      folders.add(outbox);
      folders.add(spambox);
      folders.add(trashBox);
      resu.setFolders(folders);
      resu.setReceivedMezzages(received);
      resu.setSendedMezzages(sended);

      return resu;
   }
   // Other business methods -------------------------------------------------------------------------------
   public void flush() {
      rangerRepository.flush();
   }

   public Curricula getCurriculaByRangerId(int id){

      LOG.info("Extracting curricula of Ranger with id " + id);
      try {
         Assert.notNull(id, "Null Ranger id " + id);
         Curricula res = rangerRepository.getCurriculaByRangerId(id);
         if (res == null){
            LOG.info("Curricula is empty");
         }
         return res;
      }catch (Exception e){
         throw e;
      }


   }

   public boolean modifyCurriculaofRanger(Curricula curricula, Ranger ranger){
      LOG.info("Modifying curricula of Ranger " +ranger+ "with " + curricula);

      boolean res = false;

      try {
         Assert.notNull(curricula, curricula + " curricula is null");
         Assert.notNull(curricula, ranger + " ranger is null");
         Assert.isTrue(curricula.equals(ranger.getCurricula()),"El curruculum no pertenece al ranger");
         curriculaService.save(curricula);
         LOG.info("Curricula succesfully modified");
         res = true;
      } catch (Exception e) {
         e.printStackTrace();
      }

      return res;

   }
   public boolean deleteCurriculaofRanger(Ranger ranger){
      LOG.info("Deleting curricula of Ranger " +ranger+ "with " + ranger.getCurricula());

      boolean res = false;

      try {
         Assert.notNull(ranger.getCurricula(), ranger.getCurricula() + " curricula is null");
         Assert.notNull(ranger.getCurricula(), ranger + " ranger is null");
         ranger.setCurricula(null);
         curriculaService.delete(ranger.getCurricula());
         LOG.info("Curricula succesfully deleted");
         res = true;
      } catch (Exception e) {
         e.printStackTrace();
      }

      return res;

   }


}



