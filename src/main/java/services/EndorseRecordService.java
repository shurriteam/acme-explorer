/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.EmergencyContact;
import domain.EndorseRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.EmergencyContactRepository;
import repositories.EndorseRecordRepository;

import java.util.Collection;

/**
 * Created by daviddelatorre on 12/3/17.
 */
@Service
@Transactional
public class EndorseRecordService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private EndorseRecordRepository endorseRecordRepository;

   // Managed repository--------------------------------------------------------------------------------

   public EndorseRecordService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public EndorseRecord create() {
      EndorseRecord res;
      res = new EndorseRecord();
      return res;
   }

   public Collection<EndorseRecord> findAll() {
      Collection<EndorseRecord> res;
      res = endorseRecordRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public EndorseRecord findOne(int Banner) {
      EndorseRecord res;
      res = endorseRecordRepository.findOne(Banner);
      Assert.notNull(res);
      return res;
   }

   public EndorseRecord save(EndorseRecord a) {
      Assert.notNull(a);
      EndorseRecord res;
      res = endorseRecordRepository.save(a);
      return res;
   }

   public void delete(EndorseRecord a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      endorseRecordRepository.delete(a);
   }

   // Other business methods -------------------------------------------------------------------------------
   public void flush() {
      endorseRecordRepository.flush();
   }
}
