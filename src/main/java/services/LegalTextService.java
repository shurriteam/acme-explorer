/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.EndorseRecord;
import domain.LegalText;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.EndorseRecordRepository;
import repositories.LegalTextRepository;

import java.util.Collection;

/**
 * Created by daviddelatorre on 12/3/17.
 */
@Service
@Transactional
public class LegalTextService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private LegalTextRepository legalTextRepository;

   // Managed repository--------------------------------------------------------------------------------

   public LegalTextService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public LegalText create() {
      LegalText res;

      res = new LegalText();
      return res;
   }

   public Collection<LegalText> findAll() {
      Collection<LegalText> res;
      res = legalTextRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public LegalText findOne(int Banner) {
      LegalText res;
      res = legalTextRepository.findOne(Banner);
      Assert.notNull(res);
      return res;
   }

   public LegalText save(LegalText a) {
      Assert.notNull(a);
      LegalText res;
      res = legalTextRepository.save(a);
      return res;
   }

   public void delete(LegalText a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      legalTextRepository.delete(a);
   }

   // Other business methods -------------------------------------------------------------------------------
   public void flush() {
      legalTextRepository.flush();
   }

    public Collection<LegalText> publishedLT() {
        return legalTextRepository.publishedLegalText();
    }
}
