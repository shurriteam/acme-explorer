/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.Sponsor;
import domain.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.TagRepository;

import java.util.Collection;

@Service
@Transactional
public class TagService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private TagRepository tagRepository;

   // Managed repository--------------------------------------------------------------------------------

   public TagService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public Tag create() {
      Tag res;
      res = new Tag();
      return res;
   }

   public Collection<Tag> findAll() {
      Collection<Tag> res;
      res = tagRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public Tag findOne(int Cate) {
      Tag res;
      res = tagRepository.findOne(Cate);
      Assert.notNull(res);
      return res;
   }

   public Tag save(Tag a) {
      Assert.notNull(a);
      Tag res;
      res = tagRepository.save(a);
      return res;

   }

   public void delete(Tag a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      tagRepository.delete(a);
   }


   // Other business methods -------------------------------------------------------------------------------
   public void flush() {
      tagRepository.flush();
   }
}



