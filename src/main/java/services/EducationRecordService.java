/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.Banner;
import domain.EducationRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.BannerRepository;
import repositories.EducationRecordRepository;

import java.util.Collection;

/**
 * Created by daviddelatorre on 12/3/17.
 */
@Service
@Transactional
public class EducationRecordService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private EducationRecordRepository educationRecordRepository;

   // Managed repository--------------------------------------------------------------------------------

   public EducationRecordService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public EducationRecord create() {
      EducationRecord res;
      res = new EducationRecord();
      return res;
   }

   public Collection<EducationRecord> findAll() {
      Collection<EducationRecord> res;
      res = educationRecordRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public EducationRecord findOne(int Banner) {
      domain.EducationRecord res;
      res = educationRecordRepository.findOne(Banner);
      Assert.notNull(res);
      return res;
   }

   public EducationRecord save(EducationRecord a) {
      Assert.notNull(a);
      EducationRecord res;
      res = educationRecordRepository.save(a);
      return res;
   }

   public void delete(EducationRecord a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      educationRecordRepository.delete(a);
   }

   // Other business methods -------------------------------------------------------------------------------
   public void flush() {
      educationRecordRepository.flush();
   }
}
