/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;


import domain.Attachment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.AttachmentRepository;

import java.util.Collection;

@Service
@Transactional
public class AttachmentService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private AttachmentRepository attachmentRepository;

   // Managed repository--------------------------------------------------------------------------------

   public AttachmentService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public Attachment create() {
      Attachment res;
      res = new Attachment();
      return res;
   }

   public Collection<Attachment> findAll() {
      Collection<Attachment> res;
      res = attachmentRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public Attachment findOne(int Cate) {
      Attachment res;
      res = attachmentRepository.findOne(Cate);
      Assert.notNull(res);
      return res;
   }

   public Attachment save(Attachment a) {
      Assert.notNull(a);
      Attachment res;
      res = attachmentRepository.save(a);
      return res;

   }

   public void delete(Attachment a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      attachmentRepository.delete(a);
   }

   // Other business methods -------------------------------------------------------------------------------
   public void flush() {
      attachmentRepository.flush();
   }

}



