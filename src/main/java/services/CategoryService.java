/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;


import domain.Category;
import domain.Trip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.CategoryRepository;

import java.util.Collection;

@Service
@Transactional
public class CategoryService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private CategoryRepository categoryRepository;
   @Autowired
   private TripService tripService;

   // Managed repository--------------------------------------------------------------------------------

   public CategoryService() {
      super();
   }

//perri
   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public Category create() {
      Category res;
      res = new Category();
      return res;
   }

   public Collection<Category> findAll() {
      Collection<Category> res;
      res = categoryRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public Category findOne(int Cate) {
      Category res;
      res = categoryRepository.findOne(Cate);
      Assert.notNull(res);
      return res;
   }

   public Category save(Category a) {
      Assert.notNull(a);
      Category res;
      res = categoryRepository.save(a);
      return res;

   }

   public void delete(Category a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      categoryRepository.delete(a);
   }
   // Other business methods -------------------------------------------------------------------------------

   public void flush() {
      categoryRepository.flush();
   }


   public int categoryMaster() {

      return categoryRepository.categoryMasterId();

   }

//public void deletesons(Category category){
//
//      for(Category cat : category.getSons()){
//
//         category.getSons().remove(cat);
//         cat.setFather(category.getFather());
//         category.getFather().getSons().add(cat);
//
//
////         flush();
////         cat.setRelatedTrips(null);
////         flush();
////         deletesons(cat);
////         delete(cat);
////         flush();
//
//      }
//}


   public void deleteCategory(Category category) {

      try {
         Assert.notNull(category);


         if (!category.getSons().isEmpty() && category.getFather() != null) {
            Category topFather = category.getFather();
            Collection<Category> sons = category.getSons();
            for (Category cat : sons) {
               cat.setFather(topFather);
               save(cat);
               flush();
            }
            category.getFather().getSons().remove(category);
            save(topFather);
            category.getSons().clear();
         }

         if (!category.getRelatedTrips().isEmpty() && category.getFather() != null) {
            Category topFather = category.getFather();
            Collection<Trip> relatedTrips = category.getRelatedTrips();
            for (Trip trip : relatedTrips) {
               trip.setCategory(topFather);
               tripService.save(trip);
            }
            category.getRelatedTrips().clear();
         }

         if (category.getFather() != null) {

            category.setFather(null);

         }


      } catch (Exception e) {
         e.printStackTrace();
      }

   }



}



