/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;


import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.ManagerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import security.UserAccountService;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.NoSuchElementException;

import static org.hibernate.jpa.internal.QueryImpl.LOG;

@Service
@Transactional
public class ManagerService {

    // Constructors--------------------------------------------------------------------------------------

    @Autowired
    private ManagerRepository managerRepository;

    // Managed repository--------------------------------------------------------------------------------

    public ManagerService() {
        super();
    }


    // Suporting services --------------------------------------------------------------------------------
    @Autowired
    private CurriculaService curriculaService;
    @Autowired
    private NoteService noteService;
    @Autowired
    private TripService tripService;
    @Autowired
    private TripApplicationService tripApplicationService;
    @Autowired
    private ActorService actorService;
    @Autowired
    private UserAccountService userAccountService;
    @Autowired
    private FolderService folderService;
    @Autowired
    private SurvivalClassService survivalClassService;

    // Simple CRUD method --------------------------------------------------------------------------------

    public Manager create() {
        Manager res;
        res = new Manager();
        return res;
    }

    public Collection<Manager> findAll() {
        Collection<Manager> res;
        res = managerRepository.findAll();
        Assert.notNull(res);
        return res;
    }

    public Manager findOne(int Cate) {
        Manager res;
        res = managerRepository.findOne(Cate);
        Assert.notNull(res);
        return res;
    }

    public Manager save(Manager a) {
        Assert.notNull(a);
        Manager res;
        res = managerRepository.save(a);
        return res;

    }

    public void delete(Manager a) {
        Assert.notNull(a);
        Assert.isTrue(a.getId() != 0);
        managerRepository.delete(a);
    }

    // Other business methods -------------------------------------------------------------------------------

    public Manager findByPrincipal() {
        Manager result;
        UserAccount userAccount;
        userAccount = LoginService.getPrincipal();
        Assert.notNull(userAccount);
        result = findByUserAccount(userAccount);
        Assert.notNull(result);
        return result;
    }

    public Manager findByUserAccount(UserAccount userAccount) {
        Assert.notNull(userAccount);

        Manager result;

        result = managerRepository.findByUserAccountId(userAccount.getId());

        return result;
    }

    public boolean noteReply(int noteId, String reply) {
        LOG.info("Replying the note with id " + noteId + "with the reply " + reply);
        boolean res = false;

        try {
            Assert.notNull(noteId, "noteId is null" + noteId);
            Assert.notNull(reply, "reply is null" + reply);
            Note note = noteService.findOne(noteId);
            note.setReply(reply);
            noteService.save(note);
            LOG.info("Succesfully replied");
            res = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return res;
    }


    public void flush() {
        managerRepository.flush();
    }


//metodos relacionados con Trip


    public Actor registerAsMAnager(Manager u) {
        Assert.notNull(u);
        Authority autoh = new Authority();
        autoh.setAuthority("MANAGER");
        UserAccount res = new UserAccount();
        res.addAuthority(autoh);
        res.setUsername(u.getUserAccount().getUsername());
        Md5PasswordEncoder encoder;
        encoder = new Md5PasswordEncoder();
        String hash = encoder.encodePassword(u.getUserAccount().getPassword(), null);
        res.setPassword(hash);
        UserAccount userAccount = userAccountService.save(res);
        u.setUserAccount(userAccount);
       u.getUserAccount().getAuthorities().add(autoh);
        Assert.notNull(u.getUserAccount().getAuthorities(), "authorities null al registrar");


       Manager resu = managerRepository.save(u);
        Collection<Mezzage> received = new HashSet<>();
        Collection<Mezzage> sended = new HashSet<>();
        Collection<Folder> folders = new HashSet<>();
        Folder inbox = folderService.create();
        inbox.setName("inbox");
        inbox.setOwner(resu);
        Collection<Mezzage> innnn = new HashSet<>();
        inbox.setMezzages(innnn);
        Folder outbox = folderService.create();
        outbox.setName("outbox");
        outbox.setOwner(resu);
        Collection<Mezzage> ouuuu = new HashSet<>();
        outbox.setMezzages(ouuuu);
        Folder spambox = folderService.create();
        spambox.setName("spambox");
        spambox.setOwner(resu);
        Collection<Mezzage> spaaaam = new HashSet<>();
        spambox.setMezzages(spaaaam);
        Folder trashBox = folderService.create();
        trashBox.setName("trashbox");
        trashBox.setOwner(resu);
        Collection<Mezzage> trashhh = new HashSet<>();
        trashBox.setMezzages(trashhh);
        Folder notificationBox = folderService.create();
        notificationBox.setName("notificationbox");
        notificationBox.setOwner(resu);
        Collection<Mezzage> notificBox = new HashSet<>();
        notificationBox.setMezzages(notificBox);
        folders.add(notificationBox);
        folders.add(inbox);
        folders.add(outbox);
        folders.add(spambox);
        folders.add(trashBox);
        resu.setFolders(folders);
        resu.setReceivedMezzages(received);
        resu.setSendedMezzages(sended);

        return resu;
    }


    public boolean editTrip(Trip trip) {

        Assert.notNull(trip);
        boolean res = false;
        Assert.notNull(trip, "trip is null" + trip);
        Date ahora = new Date(System.currentTimeMillis() - 1000);
        Assert.isTrue(actorService.findByPrincipalHasRol("MANAGER"), "The authenticated actor doesn't has ADMINISTRATOR rol");
        try {
            if (trip.getPublicationDate().before(ahora)) {
                tripService.save(trip);
                LOG.info("trip succesfully modified");
                res = true;
            } else {
                throw new NoSuchElementException("El  trip no esta guardado");
            }
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }

        return res;
    }


    public boolean cancelTripOrDeleteTrip(Trip trip) {

        Assert.notNull(trip);
        boolean res = false;
        Assert.notNull(trip, "trip is null" + trip);
        Date ahora = new Date(System.currentTimeMillis() - 1000);
        Assert.isTrue(actorService.findByPrincipalHasRol("MANAGER"), "The authenticated actor doesn't has ADMINISTRATOR rol");
        try {
            if (trip.getPublicationDate().before(ahora) && trip.getPublicationDate().after(ahora) ) {
                tripService.delete(trip);
                LOG.info("trip succesfully modified");
                res = true;
            } else {
                throw new NoSuchElementException("El  trip no puede editarse por que est� actualmente en vigor, consulte sus fechas");
            }
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }

        return res;

    }

    public boolean editMyTrip(TripApplication tripApplication){
        Assert.notNull(tripApplication);
        boolean res=false;
        Assert.notNull(tripApplication,"trip is null"+tripApplication);
        Manager manager = findByPrincipal();
        Assert.isTrue(actorService.findByPrincipalHasRol("MANAGER"),"The authenticated actor doesn't has ADMINISTRATOR rol");
            try{
                if((managerRepository.organizedTripsFromAGivenManager(manager.getId()).contains(tripApplication.getTrip()))){
                    tripApplicationService.save(tripApplication);
                    LOG.info("tripApplication  succesfully modified");
                    res = true;
                }else{
                    throw new NoSuchElementException("El  trip no esta guardado");
                }
            } catch (NoSuchElementException e) {
                e.printStackTrace();
            }

        return res;
    }

    public SurvivalClass createSurvivalClass(){
        SurvivalClass res = null;

        try {
            LOG.info("Creating SurvivalClass...");
            Assert.isTrue(actorService.findByPrincipalHasRol("MANAGER"));
            res = survivalClassService.create();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return res;
    }

    public boolean editSurvivalClass(SurvivalClass survivalClass){
        Assert.notNull(survivalClass);
        boolean res=false;

        try{
           Assert.isTrue(actorService.findByPrincipalHasRol("MANAGER"), "The authenticated actor doesn't has MANAGER rol");
           Assert.isTrue(managerRepository.survivalClassFromManager(findByPrincipal()).contains(survivalClass), "Estas editando una clase que no te pertenece");

           survivalClassService.save(survivalClass);
                LOG.info("SurvivalClass succesfully modified");
                res = true;

        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }

        return res;
    }



    public boolean deletSurvivalClass(SurvivalClass survivalClass){
        Assert.notNull(survivalClass);

        boolean res=false;

        try {
           Assert.isTrue(actorService.findByPrincipalHasRol("MANAGER"), "The authenticated actor doesn't has MANAGER rol");
           Assert.isTrue(managerRepository.survivalClassFromManager(findByPrincipal()).contains(survivalClass), "Estas borrando una clase que no te pertenece");

           survivalClassService.delete(survivalClass);
                LOG.info("SurvivalClass succesfully delete");
                res = true;

        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }

        return res;
    }

   public Collection<SurvivalClass> survivalClassesFromManager(Manager manager) {
      Collection<SurvivalClass> res = managerRepository.survivalClassFromManager(manager);
      return res;
   }
}
