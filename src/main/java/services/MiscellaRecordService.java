/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;


import domain.Manager;
import domain.MiscellaRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.MiscellaRecordRepository;

import java.util.Collection;

@Service
@Transactional
public class MiscellaRecordService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private MiscellaRecordRepository miscellaRecordRepository;

   // Managed repository--------------------------------------------------------------------------------

   public MiscellaRecordService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public MiscellaRecord create() {
      MiscellaRecord res;
      res = new MiscellaRecord();
      return res;
   }

   public Collection<MiscellaRecord> findAll() {
      Collection<MiscellaRecord> res;
      res = miscellaRecordRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public MiscellaRecord findOne(int Cate) {
      MiscellaRecord res;
      res = miscellaRecordRepository.findOne(Cate);
      Assert.notNull(res);
      return res;
   }

   public MiscellaRecord save(MiscellaRecord a) {
      Assert.notNull(a);
      MiscellaRecord res;
      res = miscellaRecordRepository.save(a);
      return res;

   }

   public void delete(MiscellaRecord a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      miscellaRecordRepository.delete(a);
   }

   // Other business methods -------------------------------------------------------------------------------
   // Other business methods -------------------------------------------------------------------------------
   public void flush() {
      miscellaRecordRepository.flush();
   }
}



