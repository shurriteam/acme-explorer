package controllers;


import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.AttachmentService;
import services.AuditService;
import services.AuditorService;
import services.TripService;

import javax.naming.OperationNotSupportedException;
import javax.validation.Valid;
import java.util.Collection;
import java.util.Date;

@Controller
@RequestMapping("/audit")

public class AuditController extends AbstractController {

    //----------Services-----------
    @Autowired
    private AuditService auditService;

    @Autowired
    private AuditorService auditorService;

    @Autowired
    private TripService tripService;
    @Autowired
    private AttachmentService attachmentService;


    //Constructors----------------------------------------------

    public AuditController() {
        super();
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView auditList() {

        ModelAndView result;
        Collection<Audit> audits;
        Auditor auditor = auditorService.findByPrincipal();
        audits = auditor.getAudits();
        result = new ModelAndView("audit/list");
        result.addObject("audits", audits);
        result.addObject("requestURI", "audit/list.do");

        return result;
    }


    //Create Method -----------------------------------------------------------

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView create(@RequestParam int tripId) {

        ModelAndView result;

        Audit audit = auditService.create();
        Trip trip = tripService.findOne(tripId);

        audit.setTitle("_");
        audit.setDescription("_");
        audit.setAuditStatus(AuditStatus.DRAFT);


        audit.setTrip(trip);

        result = createEditModelAndView(audit);

        return result;

    }

    // Edition ---------------------------------------------------------

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public ModelAndView edit(@RequestParam int auditId) {
        ModelAndView result;
        Audit audit;

        try {

            audit = auditService.findOne(auditId);
            Assert.notNull(audit);
            if (audit.getAuditStatus().equals(AuditStatus.DRAFT)) {
                result = createEditModelAndView(audit);
                result.addObject("audit", audit);
            } else {
                throw new OperationNotSupportedException("The audit  is in FINAL mode ant it can't be edited.");

            }


        } catch (Exception e) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", e.getMessage());
        }


        return result;
    }


    @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
    public ModelAndView save(@Valid Audit audit, BindingResult binding) {
        ModelAndView result;

        if (binding.hasErrors()) {
            result = createEditModelAndView(audit);
        } else {
            try {
                audit.setMomment(new Date(System.currentTimeMillis() - 1000));
                Auditor auditor = auditorService.findByPrincipal();

                if (!auditor.getAudits().contains(audit)) {
                    audit.setAuditor(auditor);
                    auditor.getAudits().add(audit);
                    auditService.flush();
                    audit.getTrip().getAudits().add(audit);
                }
                auditService.save(audit);
                result = new ModelAndView("redirect:list.do");
            } catch (Throwable oops) {
                result = createEditModelAndView(audit, " general.commit.error");
            }
        }
        return result;
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public ModelAndView delete(@RequestParam int auditId) {
        ModelAndView result;
        Audit audit = auditService.findOne(auditId);

        try {
            if (audit.getAuditStatus().equals(AuditStatus.DRAFT)) {
                auditService.delete(audit);
                result = new ModelAndView("redirect:list.do");
            } else {
                throw new OperationNotSupportedException("The audit  is in FINAL mode ant it can't be delete.");
            }


        } catch (Throwable oops) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", oops.getMessage());
        }


        return result;
    }


    @RequestMapping(value = "/publish", method = RequestMethod.GET)
    public ModelAndView publish(@RequestParam int auditId) {
        ModelAndView result;
        Audit audit = auditService.findOne(auditId);

        try {


            if (audit.getAuditStatus().equals(AuditStatus.DRAFT)) {
                audit.setAuditStatus(AuditStatus.PUBLISHED);
                auditService.save(audit);
                result = new ModelAndView("redirect:list.do");
            } else {
                throw new OperationNotSupportedException("The audit is already published");
            }


        } catch (Throwable oops) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", oops.getMessage());
        }


        return result;
    }
    // Ancillary methods ------------------------------------------------

    protected ModelAndView createEditModelAndView(Audit audit) {
        ModelAndView result;

        result = createEditModelAndView(audit, null);

        return result;
    }

    protected ModelAndView createEditModelAndView(Audit audit, String message) {
        ModelAndView result;

        result = new ModelAndView("audit/edit");
        result.addObject("audit", audit);
        result.addObject("message", message);

        return result;

    }


    //MANAGE ATTACHMENTS

    @RequestMapping(value = "/createA", method = RequestMethod.GET)
    public ModelAndView createA(@RequestParam int auditId) {

        ModelAndView result;

        Attachment attachment = attachmentService.create();
       attachment.setFriendId(auditId);
       attachmentService.save(attachment);


        result = createEditModelAndView(attachment);

        return result;

    }

    @RequestMapping(value = "/editA", method = RequestMethod.POST, params = "save")
    public ModelAndView save(@Valid Attachment audit, BindingResult binding) {
        ModelAndView result;

        if (binding.hasErrors()) {
            result = createEditModelAndView(audit);
        } else {
            try {
               Audit audit1 = auditService.findOne(audit.getFriendId());
               audit1.getAttachment().add(audit);
               attachmentService.flush();
//                attachmentService.save(audit);
               result = new ModelAndView("redirect: list.do");
            } catch (Throwable oops) {
                result = createEditModelAndView(audit, " general.commit.error");
            }
        }
        return result;
    }


    protected ModelAndView createEditModelAndView(Attachment audit) {
        ModelAndView result;

        result = createEditModelAndView(audit, null);

        return result;
    }

    protected ModelAndView createEditModelAndView(Attachment audit, String message) {
        ModelAndView result;

        result = new ModelAndView("audit/editA");
        result.addObject("attachment", audit);
        result.addObject("message", message);

        return result;

    }
}
