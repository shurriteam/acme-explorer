/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;

import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Date;

@Controller
@RequestMapping("/tripApplication")

public class TripApplicationController extends AbstractController {


   //Services ----------------------------------------------------------------

   @Autowired
   private TripApplicationService tripApplicationService;
   @Autowired
   private ExplorerService explorerService;
   @Autowired
   private ActorService actorService;
   @Autowired
   private TripService tripService;
   @Autowired
   private ManagerService managerService;


   //Constructors----------------------------------------------

   public TripApplicationController() {
      super();
   }


   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView tripApplicationList() {

      ModelAndView result;
      Collection<TripApplication> tripApplication;
      tripApplication = tripApplicationService.findAll();
      result = new ModelAndView("application/list");
      result.addObject("tripApplications", tripApplication);
      result.addObject("requestURI", "tripApplication/list.do");

      return result;
   }

   @RequestMapping(value = "/listMy", method = RequestMethod.GET)
   public ModelAndView managersTripApplicationList() {

      ModelAndView result;
      Collection<TripApplication> tripApplication;
      Manager e = managerService.findByPrincipal();
      Assert.notNull(e, "explorer null ?!?!?!?!");

      tripApplication = e.getApplicationsToAccept();

      result = new ModelAndView("application/list");
      result.addObject("tripApplications", tripApplication);
      result.addObject("requestURI", "tripApplication/listMy.do");

      return result;
   }


   @RequestMapping(value = "/listMyTripApplications", method = RequestMethod.GET)
   public ModelAndView explorersTripApplicationList() {

      ModelAndView result;
      Collection<TripApplication> tripApplication;
      Explorer e = explorerService.findByPrincipal();
      Assert.notNull(e, "explorer null ?!?!?!?!");
      tripApplication = explorerService.allExplorersApplications(e.getId());

      result = new ModelAndView("application/list");
      result.addObject("tripApplications", tripApplication);
      result.addObject("requestURI", "tripApplication/listMyTripApplications.do");

      return result;
   }


   //Create Method -----------------------------------------------------------

   @RequestMapping(value = "/apply", method = RequestMethod.GET)
   public ModelAndView apply(@RequestParam int tripId) {

      ModelAndView result;

      try {
         Trip trip = tripService.findOne(tripId);
         Explorer explorer = explorerService.findByPrincipal();
          if (!tripService.explorersbyTrip(tripId).contains(explorer) &&
                  !trip.getStartDate().after(new Date(System.currentTimeMillis() - 100))) {

            TripApplication tripApplication = tripApplicationService.create();
            tripApplication.setStatus(Status.PENDING);
            tripApplication.setCreationDate(new Date(System.currentTimeMillis()-100));
            tripApplication.setOwner(explorer);
            tripApplication.setCancelled(false);
            tripApplication.setTrip(trip);
            explorer.getTripApplications().add(tripApplication);
            tripApplicationService.flush();
            trip.getTripApplications().add(tripApplication);
            tripApplicationService.flush();

            result = new ModelAndView("administrator/success");
            result.addObject("ok", "You are now registered ");
         }else
         {
            throw new UnsupportedOperationException("The trip has ended / El viaje finaliz�");
         }
      } catch (UnsupportedOperationException e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }


      return result;

   }

   @RequestMapping(value = "/unapply", method = RequestMethod.GET)
   public ModelAndView unapply(@RequestParam int tripId) {

      ModelAndView result;

      try {
         Trip trip = tripService.findOne(tripId);
         Explorer explorer = explorerService.findByPrincipal();
          if (tripService.explorersbyTrip(tripId).contains(explorer) && !trip.getStartDate().after(new Date(System.currentTimeMillis() - 100))) {

            TripApplication tripApplication = tripApplicationService.tripAExplorerTrip(explorer,trip);

            trip.getTripApplications().remove(tripApplication);
            explorer.getTripApplications().remove(tripApplication);
            tripApplicationService.delete(tripApplication);


            result = new ModelAndView("administrator/success");
            result.addObject("ok", "You are not registered now ");
         }else
         {
            throw new UnsupportedOperationException("You are not registered or the travel has expired / No est�s registrado o el viaje ha pasado");
         }
      } catch (UnsupportedOperationException e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }


      return result;

   }

   @RequestMapping(value = "/accept", method = RequestMethod.GET)
   public ModelAndView accept(@RequestParam int tripAId) {

      ModelAndView result;

      try {


         TripApplication tripApplication = tripApplicationService.findOne(tripAId);

         if(!tripApplication.getStatus().equals(Status.ACCEPTED)){
            tripApplication.setStatus(Status.ACCEPTED);
            tripApplicationService.save(tripApplication);
            tripApplicationService.sendChangeMezzage(tripApplication.getOwner(),managerService.findByPrincipal(),Status.ACCEPTED,tripApplication);
         }else{
            throw new UnsupportedOperationException("The aplication already accepted");
         }



         result = new ModelAndView("administrator/success");
         result.addObject("ok", "This application is setted ACCEPTED");
      } catch (UnsupportedOperationException e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }


      return result;

   }

   @RequestMapping(value = "/deny", method = RequestMethod.GET)
   public ModelAndView deny(@RequestParam int tripAId) {

      ModelAndView result;

      try {


         TripApplication tripApplication = tripApplicationService.findOne(tripAId);

         if(!tripApplication.getStatus().equals(Status.REJECTED)){
            tripApplication.setStatus(Status.REJECTED);
            tripApplicationService.save(tripApplication);
            tripApplicationService.sendChangeMezzage(tripApplication.getOwner(),managerService.findByPrincipal(),Status.REJECTED,tripApplication);
         }else{
            throw new UnsupportedOperationException("The aplication already deny");
         }



         result = new ModelAndView("administrator/success");
         result.addObject("ok", "This application is setted REJECTED ");
      } catch (UnsupportedOperationException e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }


      return result;

   }

   @RequestMapping(value = "/due", method = RequestMethod.GET)
   public ModelAndView due(@RequestParam int tripAId) {

      ModelAndView result;

      try {


         TripApplication tripApplication = tripApplicationService.findOne(tripAId);

         if(!tripApplication.getStatus().equals(Status.DUE)){
            tripApplication.setStatus(Status.DUE);
            tripApplicationService.save(tripApplication);
            tripApplicationService.sendChangeMezzage(tripApplication.getOwner(),managerService.findByPrincipal(),Status.DUE,tripApplication);
         }else{
            throw new UnsupportedOperationException("The aplication already due");
         }



         result = new ModelAndView("administrator/success");
         result.addObject("ok", "This application is setted DUE");
      } catch (UnsupportedOperationException e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }


      return result;

   }

   @RequestMapping(value = "/cancel", method = RequestMethod.GET)
   public ModelAndView cancel(@RequestParam int tripAId) {

      ModelAndView result;

      try {


         TripApplication tripApplication = tripApplicationService.findOne(tripAId);

         if(!tripApplication.getStatus().equals(Status.CANCELLED) && tripApplication.getStatus().equals(Status.ACCEPTED)){
             if (tripApplication.getTrip().getStartDate().after(new Date(System.currentTimeMillis()-100))){
                 result = createEditModelAndView2(tripApplication);
             }else{
                 throw new UnsupportedOperationException("The trip has started and this applicattion can not be cancelled");
             }

         }else{
             throw new UnsupportedOperationException("The aplication already accepted");
         }

      } catch (UnsupportedOperationException e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }


      return result;

   }


   @RequestMapping(value = "/editReason", method = RequestMethod.POST, params = "save")
   public ModelAndView editReason(@Valid TripApplication tripApplication, BindingResult binding) {
      ModelAndView result;

      if (binding.hasErrors()) {
         result = createEditModelAndView(tripApplication);
      } else {
         try {
            tripApplication.setStatus(Status.CANCELLED);
            tripApplicationService.save(tripApplication);
            tripApplicationService.sendChangeMezzage(tripApplication.getOwner(),managerService.findByPrincipal(),Status.CANCELLED,tripApplication);
             result = new ModelAndView("administrator/success");
             result.addObject("ok", "This application is cancelled");
         } catch (Throwable oops) {
            result = createEditModelAndView(tripApplication, "property.commit.error");
         }
      }
      return result;
   }



   // Edition ---------------------------------------------------------

   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int tripApplicationId) {
      ModelAndView result;
      TripApplication tripApplication;

      tripApplication = tripApplicationService.findOne(tripApplicationId);
      Assert.notNull(tripApplication);
      result = createEditModelAndView(tripApplication);
      result.addObject("tripApplication", tripApplication);


      return result;
   }

   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid TripApplication tripApplication, BindingResult binding) {
      ModelAndView result;

      if (binding.hasErrors()) {
         result = createEditModelAndView(tripApplication);
      } else {
         try {

            tripApplicationService.save(tripApplication);
            result = new ModelAndView("redirect:listMyTripApplications.do");
         } catch (Throwable oops) {
            result = createEditModelAndView(tripApplication, "property.commit.error");
         }
      }
      return result;
   }


   @RequestMapping(value = "/editStatus", method = RequestMethod.GET)
   public ModelAndView editstatus(@RequestParam int tripApplicationId) {
      ModelAndView result;
      TripApplication tripApplication;
      tripApplication = tripApplicationService.findOne(tripApplicationId);
      Assert.notNull(tripApplication);
      result = createEditModelAndView2(tripApplication);
      result.addObject("tripApplication", tripApplication);


      return result;
   }

   @RequestMapping(value = "/editStatus", method = RequestMethod.POST, params = "save")
   public ModelAndView savestatus(@Valid TripApplication tripApplication, BindingResult binding) {
      ModelAndView result;

      if (binding.hasErrors()) {
         result = createEditModelAndView2(tripApplication);
      } else {
         try {
            //  tripApplication.setCreationDate(new Date(System.currentTimeMillis()-100));
            tripApplicationService.save(tripApplication);
            result = new ModelAndView("redirect:listMy.do");
         } catch (Throwable oops) {
            result = createEditModelAndView2(tripApplication, "property.commit.error");
         }
      }
      return result;
   }








   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete(@RequestParam int tripApplicationId) {
      ModelAndView result;
      TripApplication p = tripApplicationService.findOne(tripApplicationId);

      try {
         tripApplicationService.delete(p);
         result = new ModelAndView("redirect:list.do");
      } catch (Throwable oops) {
         result = createEditModelAndView(p, "tripApplication.commit.error");
      }


      return result;
   }

   @RequestMapping(value = "/view", method = RequestMethod.GET)
   public ModelAndView view(@RequestParam int tripApplicationId) {
      ModelAndView res;
      TripApplication p = tripApplicationService.findOne(tripApplicationId);
      res = new ModelAndView("application/view");
      res.addObject("tripApplication", p);

      return res;
   }

   // Ancillary methods ------------------------------------------------

   protected ModelAndView createEditModelAndView(TripApplication tripApplication) {
      ModelAndView result;

      result = createEditModelAndView(tripApplication, null);

      return result;
   }

   protected ModelAndView createEditModelAndView(TripApplication tripApplication, String message) {
      ModelAndView result;

      result = new ModelAndView("application/edit");
      result.addObject("tripApplication", tripApplication);
      result.addObject("message", message);

      return result;

   }

   protected ModelAndView createEditModelAndView2(TripApplication tripApplication) {
      ModelAndView result;

      result = createEditModelAndView2(tripApplication, null);

      return result;
   }

   protected ModelAndView createEditModelAndView2(TripApplication tripApplication, String message) {
      ModelAndView result;

      result = new ModelAndView("application/editStatus");
      result.addObject("tripApplication", tripApplication);
      result.addObject("message", message);

      return result;

   }


}








