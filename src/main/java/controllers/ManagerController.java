/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;

import domain.Manager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.ManagerService;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/manageer")
public class ManagerController extends AbstractController {


   //Services ----------------------------------------------------------------

   @Autowired
   private ManagerService managerService;


   //Constructors----------------------------------------------

   public ManagerController() {
      super();
   }


   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView managerList() {

      ModelAndView result;
      Collection<Manager> manager;
      manager = managerService.findAll();
      result = new ModelAndView("manageer/list");
      result.addObject("manager", manager);
      result.addObject("requestURI", "manageer/list.do");

      return result;
   }


   //Create Method -----------------------------------------------------------

   protected static ModelAndView createEditModelAndView2(Manager manager) {
      ModelAndView result;

      result = createEditModelAndView2(manager, null);

      return result;
   }

   protected static ModelAndView createEditModelAndView2(Manager manager, String message) {
      ModelAndView result;

      result = new ModelAndView("manageer/register");
      result.addObject("manager", manager);
      result.addObject("message", message);

      return result;

   }

   // Edition ---------------------------------------------------------

   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int managerId) {
      ModelAndView result;
      Manager manager;

      manager = managerService.findOne(managerId);
      Assert.notNull(manager);
      result = createEditModelAndView(manager);
      result.addObject("manager", manager);


      return result;
   }

   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid Manager manager, BindingResult binding) {
      ModelAndView result;

      if (!binding.hasErrors()) {
         result = createEditModelAndView(manager);
      } else {
         try {

            managerService.save(manager);
            result = new ModelAndView("welcome/index");
         } catch (Throwable oops) {
            result = createEditModelAndView(manager, "property.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete(@RequestParam int managerId) {
      ModelAndView result;
      Manager p = managerService.findOne(managerId);

      try {
         managerService.delete(p);
         result = new ModelAndView("welcome/index");
      } catch (Throwable oops) {
         result = createEditModelAndView(p, "manager.commit.error");
      }


      return result;
   }

   @RequestMapping(value = "/view", method = RequestMethod.GET)
   public ModelAndView view(@RequestParam int managerId) {
      ModelAndView res;
      Manager p = managerService.findOne(managerId);
      res = new ModelAndView("manageer/view");
      res.addObject("manager", p);

      return res;
   }

   // Ancillary methods ------------------------------------------------

   protected ModelAndView createEditModelAndView(Manager manager) {
      ModelAndView result;

      result = createEditModelAndView(manager, null);

      return result;
   }

//   @RequestMapping(value = "/create", method = RequestMethod.GET)
//   public ModelAndView create() {
//
//      ModelAndView result;
//
//      Ranger manager = managerService.create();
//      result = createEditModelAndView(manager);
//
//      return result;
//
//   }

   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create2() {
      ModelAndView result;
      Manager manager = managerService.create();
      result = createEditModelAndView2(manager);
      return result;
   }

   @RequestMapping(value = "/register", method = RequestMethod.POST, params = "save")
   public ModelAndView register(@Valid Manager manager, BindingResult binding) {
      ModelAndView result;

      if (!binding.hasErrors()) {
         result = createEditModelAndView2(manager);
      } else {
         try {
            managerService.registerAsMAnager(manager);
            result = new ModelAndView("welcome/index");
         } catch (Throwable oops) {
            result = createEditModelAndView2(manager, "general.commit.error");
         }
      }
      return result;
   }
   // Edition ---------------------------------------------------------

   protected ModelAndView createEditModelAndView(Manager manager, String message) {
      ModelAndView result;

      result = new ModelAndView("manageer/edit");
      result.addObject("manager", manager);
      result.addObject("message", message);

      return result;

   }


}








