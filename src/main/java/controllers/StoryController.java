/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;


import domain.Comment;
import domain.Explorer;
import domain.Story;
import domain.Trip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.*;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/story")
public class StoryController extends AbstractController {

   //Services ----------------------------------------------------------------

   @Autowired
   private StoryService storyService;
   @Autowired
   private ActorService actorService;
   @Autowired
   private ExplorerService explorerService;
   @Autowired
   private TripService tripService;

   //Constructors----------------------------------------------

   public StoryController() {
      super();
   }

   protected static ModelAndView createEditModelAndView(Story comment) {
      ModelAndView result;

      result = createEditModelAndView(comment, null);

      return result;
   }


   //Create Method -----------------------------------------------------------

   protected static ModelAndView createEditModelAndView(Story comment, String message) {
      ModelAndView result;

      result = new ModelAndView("trip/createStory");
      result.addObject("story", comment);
      result.addObject("message", message);

      return result;

   }



   @RequestMapping(value = "/myStories", method = RequestMethod.GET)
   public ModelAndView myStoriesList() {

      ModelAndView result;
      Collection<Story> comments;

      Explorer explorer = explorerService.findByPrincipal();
      comments = storyService.storiesbyExplorer(explorer);
      result = new ModelAndView("story/list");
      result.addObject("stories", comments);
      result.addObject("requestURI", "story/myStories.do");

      return result;
   }




   // Ancillary methods ------------------------------------------------

   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView creteStory(@RequestParam int tripId) {

      ModelAndView result;

      Trip trip = tripService.findOne(tripId);
      Explorer explorer = explorerService.findByPrincipal();
      Story story= storyService.create();
      story.setText("_");
      story.setTitle("_");
      story.setExplorer(explorer);
      story.setAssociatedTrip(trip);
      trip.getStories().add(story);
      storyService.flush();
      explorer.getStories().add(story);
      storyService.flush();

      result = createEditModelAndView(story);

      return result;

   }

//   @RequestMapping(value = "/edit", method = RequestMethod.GET)
//   public ModelAndView edit(@RequestParam int commentId) {
//      ModelAndView result;
//      Comment comment;
//
//      comment = commentService.findOne(commentId);
//      Assert.notNull(comment);
//      result = createEditModelAndView(comment);
//
//      return result;
//   }

   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid Story comment, BindingResult binding) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndView(comment);
      } else {
         try {

            storyService.save(comment);

            result = new ModelAndView("administrator/success");
            result.addObject("ok", "Story sucessfully created");
         } catch (Throwable oops) {
            result = createEditModelAndView(comment, "general.commit.error");
         }
      }
      return result;
   }

//   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
//   public ModelAndView delete(Comment comment) {
//      ModelAndView result;
//      try {
//         commentService.delete(comment);
//         result = new ModelAndView("redirect:list.do");
//      } catch (Throwable oops) {
//         result = createEditModelAndView(comment, "comment.commit.error");
//      }
//
//      return result;
//   }

}
