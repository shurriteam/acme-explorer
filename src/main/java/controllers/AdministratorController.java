/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;


import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.ActorService;
import services.AdministratorService;
import services.MezzageService;
import services.Utilservice;

import javax.validation.Valid;
import java.util.*;


@Controller
@RequestMapping("/administrator")
public class AdministratorController extends AbstractController {

    //Services ----------------------------------------------------------------


    @Autowired
    private AdministratorService administratorService;
    @Autowired
    private ActorService actorService;
    @Autowired
    private MezzageService mezzageService;
    @Autowired
    private Utilservice utilservice;

    //Constructors----------------------------------------------

    public AdministratorController() {
        super();
    }


    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView administratorList() {

        ModelAndView result;
        Collection<Administrator> administrators;

        administrators = administratorService.findAll();
        result = new ModelAndView("administrator/list");
        result.addObject("administrators", administrators);
        result.addObject("requestURI", "administrator/list.do");

        return result;
    }

    @RequestMapping(value = "/listsuspmanagerandrangers", method = RequestMethod.GET)
    public ModelAndView listsuspmanagerandrangers() {

        ModelAndView result;
        Collection<Actor> suspiciousPeople = administratorService.suspiciousActors();
        result = new ModelAndView("actor/list");
        result.addObject("actors", suspiciousPeople);
       result.addObject("requestURI", "administrator/listsuspmanagerandrangers.do");

        return result;
    }


    //Create Method -----------------------------------------------------------

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView create() {

        ModelAndView result;

        Administrator administrator = administratorService.create();
        result = createEditModelAndView(administrator);

        return result;

    }

    // Edition ---------------------------------------------------------

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public ModelAndView edit(@RequestParam int administratorId) {
        ModelAndView result;
        Administrator administrator;

        administrator = administratorService.findOne(administratorId);
        Assert.notNull(administrator);
        result = createEditModelAndView(administrator);

        return result;
    }


    @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
    public ModelAndView save(@Valid Administrator administrator, BindingResult binding) {
        ModelAndView result;

        if (binding.hasErrors()) {
            result = createEditModelAndView(administrator);
        } else {
            try {
                administratorService.save(administrator);
                result = new ModelAndView("redirect:list.do");
            } catch (Throwable oops) {
                result = createEditModelAndView(administrator, "administrator.commit.error");
            }
        }
        return result;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
    public ModelAndView delete(Administrator administrator) {
        ModelAndView result;
        try {
            administratorService.delete(administrator);
            result = new ModelAndView("redirect:list.do");
        } catch (Throwable oops) {
            result = createEditModelAndView(administrator, "administrator.commit.error");
        }

        return result;
    }

    // Ancillary methods ------------------------------------------------

    protected ModelAndView createEditModelAndView(Administrator administrator) {
        ModelAndView result;

        result = createEditModelAndView(administrator, null);

        return result;
    }

    protected ModelAndView createEditModelAndView(Administrator administrator, String message) {
        ModelAndView result;

        result = new ModelAndView("administrator/edit");
        result.addObject("administrator", administrator);
        result.addObject("message", message);

        return result;

    }


    @RequestMapping(value = "/ban", method = RequestMethod.GET)
    public ModelAndView banUser(int userId) {

        ModelAndView res;
        Actor actor = actorService.findOne(userId);
        administratorService.banUser(actor);
        res = new ModelAndView("actor/list");
        res.addObject("actors", actorService.findAll());


        return res;


    }
    @RequestMapping(value = "/unban", method = RequestMethod.GET)
    public ModelAndView unbanUser(int userId) {

        ModelAndView res;
        Actor user = actorService.findOne(userId);

        administratorService.unbanUser(user);
        res = new ModelAndView("actor/list");
        res.addObject("actors", actorService.findAll());


        return res;


    }

    @RequestMapping(value = "/createBM", method = RequestMethod.GET)
    public ModelAndView createBM() {

        ModelAndView result;

        Mezzage mezzage = mezzageService.create();
        mezzage.setSenderEmail(actorService.findByPrincipal().getEmail());
        mezzage.setMoment(new Date(System.currentTimeMillis() - 100));

        result = createEditModelAndViewBM(mezzage);
        return result;

    }

    @RequestMapping(value = "/saveBM", method = RequestMethod.POST, params = "save")
    public ModelAndView saveBM(@Valid Mezzage mezzage, BindingResult binding) {
        ModelAndView result;
        if (binding.hasErrors()) {
            result = createEditModelAndViewBM(mezzage);
        } else {
            try {
                mezzage.setSender(actorService.findByPrincipal());
               mezzage.setBroadcast(true);
                mezzage.setSenderEmail(actorService.findByPrincipal().getEmail());
                mezzage.setMoment(new Date(System.currentTimeMillis() - 100));
                mezzage.setSubject("BROADCAST: "+mezzage.getSubject());
                List<Folder> folders = new ArrayList<>(actorService.findByPrincipal().getFolders());
                mezzage.setFolder(folders.get(5));
                folders.get(5).getMezzages().add(mezzage);
                administratorService.storeBroadCastMessage(mezzage);
                result = new ModelAndView("folder/list");
            } catch (Throwable oops) {
                result = createEditModelAndViewBM1(mezzage, "general.commit.error");


            }
        }
        return result;
    }



    protected static ModelAndView createEditModelAndViewBM(Mezzage mezzage) {
        ModelAndView result;

        result = createEditModelAndViewBM1(mezzage, null);

        return result;
    }


    protected static ModelAndView createEditModelAndViewBM1(Mezzage mezzage, String message) {
        ModelAndView result;


        result = new ModelAndView("administrator/bm");
        result.addObject("mezzage", mezzage);
        result.addObject("message", message);


        return result;

    }




    //CHANGE CACHE

    @RequestMapping(value = "/changeCache", method = RequestMethod.GET)
    public ModelAndView changeCache() {

        ModelAndView result;

        List<AdministratorUtils> administratorUtils =  new ArrayList<>(utilservice.findAll());

        result = createEditModelAndViewCache(administratorUtils.get(0));
        return result;

    }

    @RequestMapping(value = "/saveCache", method = RequestMethod.POST, params = "save")
    public ModelAndView saveCache(@Valid AdministratorUtils mezzage, BindingResult binding) {
        ModelAndView result;
        if (binding.hasErrors()) {
            result = createEditModelAndViewCache(mezzage);
        } else {
            try {
                utilservice.save(mezzage);
                result = new ModelAndView("administrator/success");
                result.addObject("ok", "Value succesfully change");
            } catch (Throwable oops) {
                result = createEditModelAndViewCache2(mezzage, "general.commit.error");


            }
        }
        return result;
    }


   //CHANGE CACHE

   protected static ModelAndView createEditModelAndViewBanner(AdministratorUtils mezzage) {
      ModelAndView result;

      result = createEditModelAndViewBanner(mezzage, null);

      return result;
   }

   protected static ModelAndView createEditModelAndViewBanner(AdministratorUtils mezzage, String message) {
      ModelAndView result;


      result = new ModelAndView("administrator/banner");
      result.addObject("administratorUtil", mezzage);
      result.addObject("message", message);


      return result;

   }

   protected static ModelAndView createEditModelAndViewCache(AdministratorUtils mezzage) {
        ModelAndView result;

        result = createEditModelAndViewCache2(mezzage, null);

        return result;
    }

   @RequestMapping(value = "/changeBanner", method = RequestMethod.GET)
   public ModelAndView changeBanner() {

      ModelAndView result;

      List<AdministratorUtils> administratorUtils = new ArrayList<>(utilservice.findAll());

      result = createEditModelAndViewBanner(administratorUtils.get(0));
      return result;

   }

   @RequestMapping(value = "/saveBanner", method = RequestMethod.POST, params = "save")
   public ModelAndView saveBanner(@Valid AdministratorUtils mezzage, BindingResult binding) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndViewBanner(mezzage);
      } else {
         try {
            utilservice.save(mezzage);
            result = new ModelAndView("administrator/success");
            result.addObject("ok", "Value succesfully change");
         } catch (Throwable oops) {
            result = createEditModelAndViewBanner(mezzage, "general.commit.error");


         }
      }
      return result;
   }


    protected static ModelAndView createEditModelAndViewCache2(AdministratorUtils mezzage, String message) {
        ModelAndView result;


        result = new ModelAndView("administrator/cache");
        result.addObject("administratorUtil", mezzage);
        result.addObject("message", message);


        return result;

    }

    //Chage number


    @RequestMapping(value = "/changeNumber", method = RequestMethod.GET)
    public ModelAndView changeNumber() {

        ModelAndView result;

        List<AdministratorUtils> administratorUtils =  new ArrayList<>(utilservice.findAll());

        result = createEditModelAndViewN(administratorUtils.get(0));
        return result;

    }

    protected static ModelAndView createEditModelAndViewN(AdministratorUtils mezzage) {
        ModelAndView result;

        result = createEditModelAndViewN2(mezzage, null);

        return result;
    }


    protected static ModelAndView createEditModelAndViewN2(AdministratorUtils mezzage, String message) {
        ModelAndView result;


        result = new ModelAndView("administrator/number");
        result.addObject("administratorUtil", mezzage);
        result.addObject("message", message);


        return result;

    }

    //Change VAT

    @RequestMapping(value = "/changeVAT", method = RequestMethod.GET)
    public ModelAndView changeVat() {

        ModelAndView result;

        List<AdministratorUtils> administratorUtils =  new ArrayList<>(utilservice.findAll());

        result = createEditModelAndViewV(administratorUtils.get(0));
        return result;

    }

    protected static ModelAndView createEditModelAndViewV(AdministratorUtils mezzage) {
        ModelAndView result;

        result = createEditModelAndViewV2(mezzage, null);

        return result;
    }


    protected static ModelAndView createEditModelAndViewV2(AdministratorUtils mezzage, String message) {
        ModelAndView result;


        result = new ModelAndView("administrator/vat");
        result.addObject("administratorUtil", mezzage);
        result.addObject("message", message);


        return result;

    }


    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public ModelAndView dashboard() {

        ModelAndView result;

        try {
            Double q1 = administratorService.averageNumberOfApplicationsPerTrip();
            int q2 = administratorService.maximumNumberOfAppicationsPerTrip();
            int q3 = administratorService.minimumNumberOfApplicationsPerTrip();
            Double q4 = administratorService.standardDeviationOfTheNumberOfApplicationsPerTrip();
            Double q5 = administratorService.averageNumberOfTripsManagerPerManager();
            int q6 = administratorService.maximumNumberOfTripsManagerPerManager();
            int q7 = administratorService.minimumNumberOfTripsManagerPerManager();
            Double q8 = administratorService.standartDeviationOfNumberOfTripsManagerPerManager();
            Double q9 = administratorService.averageNumberOfTripsPrice();
            int q10 = administratorService.maximumNumberOfTripsPrice();
            int q11 = administratorService.minimumNumberOfTripsPrice();
            Double q12 = administratorService.standartDeviationOfNumberOfTripsPrice();
            Double q13 = administratorService.averageNumberOfTripsGuidedPerRanger();
            int q14 = administratorService.maximumNumberOfTripsGuidedPerRanger();
            int q15 = administratorService.minimumNumberOfTripsGuidedPerRanger();
            Double q16 = administratorService.standartDeviationOfNumberOfTripsGuidedPerRanger();
            Double q18 = administratorService.numberOfAppsWithStatusPending();
            Double q19 = administratorService.ratioPendingVsAll();
            Double q20 = administratorService.numberOfAppsWithStatusDue();
            Double q21 = administratorService.ratioPendingVsDue();
            Double q22 = administratorService.ratioPendingVsAccepted();
            Double q23 = administratorService.numberOfAppsWithStatusCancelled();
            Double q24 = administratorService.ratioPendingVsCancelled();
            Double q25 = administratorService.numberOfCancelledTrips();
            Double q26 = administratorService.numberOfTrips();
            Double q27 = administratorService.ratioCancelledTrips();


            Collection<Trip> q28 = administratorService.tripsThatHaveGotAtLeast10PercMoreApplicationsThanTheAverageOrdereByNumberOfApplications();
            Map<LegalText, Long> q29 = administratorService.numberOfTimesEachLegalTextHasBeenReferenced();


            Double q30 = administratorService.averageNumberOfNotesPerTrip();
            int q31 = administratorService.maximumNumberOfNotesPerTrip();
            int q32 = administratorService.minimumNumberOfNotesPerTrip();
            Double q33 = administratorService.standartDeviationOfNotesPerTrip();
            Double q34 = administratorService.averageNumberOfAuditsPerTrip();
            int q35 = administratorService.maxNumberOfAuditsPerTrip();
            int q36 = administratorService.minNumberOfAuditsPerTrip();
            Double q37 = administratorService.standartDeviationOfAuditsPerTrip();
            Integer q38 = administratorService.numberOfTripsWithoutAudits();
            Double q39 = administratorService.ratioOfTripsWithAnAuditRecord();
            int q40 = administratorService.numberOfRangersWithoutCurricula();
            Double q41 = administratorService.ratioOfRangersWhoHasntRegisteredTheirCurricula();
            Double q42 = administratorService.rangerWithoutEndorsedCurricula();
            Double q43 = administratorService.ratioOfRangersWithoutEndorsedCurricula();
            int q44 = administratorService.numerOfSuspiciousManagers();
            int q45 = administratorService.ratioOfSuspiciousManagers();
            Double q46 = administratorService.ratioOfSuspiciousRangers();

            result = new ModelAndView("administrator/dashboard");


            result.addObject("q1", q1);
            result.addObject("q2", q2);
            result.addObject("q3", q3);
            result.addObject("q4", q4);
            result.addObject("q5", q5);
            result.addObject("q6", q6);
            result.addObject("q7", q7);
            result.addObject("q8", q8);
            result.addObject("q9", q9);
            result.addObject("q10", q10);
            result.addObject("q11", q11);
            result.addObject("q12", q12);
            result.addObject("q13", q13);
            result.addObject("q14", q14);
            result.addObject("q15", q15);
            result.addObject("q16", q16);
            result.addObject("q18", q18);
            result.addObject("q19", q19);
            result.addObject("q20", q20);
            result.addObject("q21", q21);
            result.addObject("q22", q22);
            result.addObject("q23", q23);
            result.addObject("q24", q24);
            result.addObject("q25", q25);
            result.addObject("q26", q26);
            result.addObject("q27", q27);

            result.addObject("q28", q28);
            result.addObject("q29", q29);

            result.addObject("q30", q30);
            result.addObject("q31", q31);
            result.addObject("q32", q32);
            result.addObject("q33", q33);
            result.addObject("q34", q34);
            result.addObject("q35", q35);
            result.addObject("q36", q36);
            result.addObject("q37", q37);
            result.addObject("q38", q38);
            result.addObject("q39", q39);
            result.addObject("q40", q40);
            result.addObject("q41", q41);
            result.addObject("q42", q42);
            result.addObject("q43", q43);
            result.addObject("q44", q44);
            result.addObject("q45", q45);
            result.addObject("q46", q46);
        } catch (Exception e) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", e.getMessage());
        }


        return result;

    }



}
