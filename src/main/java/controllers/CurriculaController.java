package controllers;


import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.*;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/curricula")

public class CurriculaController  extends AbstractController{

    //----------Services-----------
    @Autowired
    private CurriculaService curriculaService;
   @Autowired
   private RangerService rangerService;
   @Autowired
   private PersonalRecordService personalRecordService;
   @Autowired
   private EducationRecordService educationRecordService;
   @Autowired
   private MiscellaRecordService miscellaRecordService;
   @Autowired
   private ProfessionalRecordService professionalRecordService;
   @Autowired
   private EndorseRecordService endorseRecordService;



    //Constructors----------------------------------------------

    public CurriculaController() {super();}

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView curriculaList() {

        ModelAndView result;
        Collection<Curricula> curricula;
        curricula= curriculaService.findAll();
        result = new ModelAndView("curricula/list");
        result.addObject("curricula", curricula);
        result.addObject("requestURI", "curricula/list.do");

        return result;
    }


    //Create Method -----------------------------------------------------------

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView create(@RequestParam int rangerId) {

        ModelAndView result;

        try {
            Ranger ranger = rangerService.findOne(rangerId);
            Curricula curricula = curriculaService.create();
            PersonalRecord personalRecord = personalRecordService.create();
            personalRecord.setEmail(ranger.getEmail());
            personalRecord.setPhoto(ranger.getImage());
            personalRecord.setPhone(ranger.getPhoneNumber());
            personalRecord.setName(ranger.getName());
           PersonalRecord pr2 = personalRecordService.save(personalRecord);
           curricula.setPersonalRecords(pr2);

           pr2.setCurricula(curricula);
           personalRecordService.save(pr2);
           ranger.setCurricula(curricula);
            curriculaService.save(curricula);
            result = new ModelAndView("administrator/success");
            result.addObject("ok", "Curricula created / Curriculum creado");
        } catch (Exception e) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", e.getMessage());
        }


        return result;

    }

    // Edition ---------------------------------------------------------

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public ModelAndView edit() {
        ModelAndView result;


        try {
            result = new ModelAndView("curricula/edit");
            result.addObject("expId", rangerService.findByPrincipal().getId());

            if (rangerService.findByPrincipal().getCurricula()!=null){
                int curridulaId = rangerService.findByPrincipal().getCurricula().getId();
                Curricula curricula = curriculaService.findOne(curridulaId);
                Assert.notNull(curridulaId);
                result.addObject("cid", curridulaId);
                result.addObject("pr", curricula.getPersonalRecords());
                result.addObject("PRO", curricula.getProfessionalRecords());
                result.addObject("EDU", curricula.getEducationRecords());
                result.addObject("END", curricula.getEndorseRecords());
                result.addObject("MIS", curricula.getMiscellaRecords());
            }else {
                result.addObject("noc", true);
            }

        } catch (Exception e) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", e.getMessage());
        }


        return result;
    }



    @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
    public ModelAndView save(@Valid Curricula curricula, BindingResult binding) {
        ModelAndView result;

        if (binding.hasErrors()) {
            result = createEditModelAndView(curricula);
        } else {
            try {
                curriculaService.save(curricula);
                result = new ModelAndView("redirect:edit.do");
            } catch (Throwable oops) {
                result = createEditModelAndView(curricula, " property.commit.error");
            }
        }
        return result;
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public ModelAndView delete(@RequestParam int curriculaId) {
        ModelAndView result;
        Curricula p = curriculaService.findOne(curriculaId);

        try {
            p.setPersonalRecords(null);
            p.setEducationRecords(null);
            p.setEndorseRecords(null);
            p.setMiscellaRecords(null);
            p.setProfessionalRecords(null);
            rangerService.findByPrincipal().setCurricula(null);
            curriculaService.save(p);
            curriculaService.flush();

            curriculaService.delete(p);
            result = new ModelAndView("redirect:edit.do");
        } catch (Throwable oops) {
            result = createEditModelAndView(p, "general.commit.error");
        }


        return result;
    }
    // Ancillary methods ------------------------------------------------

    protected ModelAndView createEditModelAndView(Curricula curricula) {
        ModelAndView result;

        result = createEditModelAndView(curricula, null);

        return result;
    }

    protected ModelAndView createEditModelAndView(Curricula curricula, String message) {
        ModelAndView result;

        result = new ModelAndView("curricula/edit");
        result.addObject("curricula", curricula);
        result.addObject("message", message);

        return result;

    }

    //DIFERENTS RECORDS

    //PERSONAL RECORD

    @RequestMapping(value = "/editPR", method = RequestMethod.GET)
    public ModelAndView editPersonalRecord() {
        ModelAndView result;


        try {
            PersonalRecord personalRecord = rangerService.findByPrincipal().getCurricula().getPersonalRecords();
            result = createEditModelAndViewPR(personalRecord);
        } catch (Exception e) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", e.getMessage());
        }



        return result;
    }

    @RequestMapping(value = "/savePR", method = RequestMethod.POST, params = "save")
    public ModelAndView savePR(@Valid PersonalRecord curricula, BindingResult binding) {
        ModelAndView result;

        if (binding.hasErrors()) {
            result = createEditModelAndViewPR(curricula);
        } else {
            try {
                personalRecordService.save(curricula);
                result = new ModelAndView("redirect:edit.do");
            } catch (Throwable oops) {
                result = createEditModelAndViewPR(curricula, " property.commit.error");
            }
        }
        return result;
    }

    protected ModelAndView createEditModelAndViewPR(PersonalRecord curricula) {
        ModelAndView result;

        result = createEditModelAndViewPR(curricula, null);

        return result;
    }

    protected ModelAndView createEditModelAndViewPR(PersonalRecord curricula, String message) {
        ModelAndView result;

        result = new ModelAndView("curricula/editPersonal");
        result.addObject("curricula", curricula);
        result.addObject("message", message);

        return result;

    }


    //EDUCATION RECORD

    @RequestMapping(value = "/editEDU", method = RequestMethod.GET)
    public ModelAndView editEDU(@RequestParam int curriculaId) {
        ModelAndView result;

        try {
            Curricula curricula = curriculaService.findOne(curriculaId);
            EducationRecord educationRecord = educationRecordService.create();
            educationRecord.setCurricula(curricula);
            result = createEditModelAndViewEDU(educationRecord);
        } catch (Exception e) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", e.getMessage());
        }



        return result;
    }

    @RequestMapping(value = "/saveEDU", method = RequestMethod.POST, params = "save")
    public ModelAndView saveEDU(@Valid EducationRecord curricula, BindingResult binding) {
        ModelAndView result;

        if (binding.hasErrors()) {
            result = createEditModelAndViewEDU(curricula);
        } else {
            try {
                curricula.getCurricula().getEducationRecords().add(curricula);
                educationRecordService.flush();
//                educationRecordService.save(curricula);
                result = new ModelAndView("redirect:edit.do");
            } catch (Throwable oops) {
                result = createEditModelAndViewEDU(curricula, " property.commit.error");
            }
        }
        return result;
    }

    protected ModelAndView createEditModelAndViewEDU(EducationRecord curricula) {
        ModelAndView result;

        result = createEditModelAndViewEDU(curricula, null);

        return result;
    }

    protected ModelAndView createEditModelAndViewEDU(EducationRecord curricula, String message) {
        ModelAndView result;

        result = new ModelAndView("curricula/editEducation");
        result.addObject("curricula", curricula);
        result.addObject("message", message);

        return result;

    }

    //PROFESIONAL RECORD

    @RequestMapping(value = "/editPRO", method = RequestMethod.GET)
    public ModelAndView editPRO(@RequestParam int curriculaId) {
        ModelAndView result;

        try {
            Curricula curricula = curriculaService.findOne(curriculaId);
            ProfessionalRecord educationRecord = professionalRecordService.create();
            educationRecord.setCurricula(curricula);
            result = createEditModelAndViewPRO(educationRecord);
        } catch (Exception e) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", e.getMessage());
        }



        return result;
    }

    @RequestMapping(value = "/savePRO", method = RequestMethod.POST, params = "save")
    public ModelAndView savePRO(@Valid ProfessionalRecord curricula, BindingResult binding) {
        ModelAndView result;

        if (binding.hasErrors()) {
            result = createEditModelAndViewPRO(curricula);
        } else {
            try {
                curricula.getCurricula().getProfessionalRecords().add(curricula);
                professionalRecordService.flush();
                result = new ModelAndView("redirect:edit.do");
            } catch (Throwable oops) {
                result = createEditModelAndViewPRO(curricula, " property.commit.error");
            }
        }
        return result;
    }

    protected ModelAndView createEditModelAndViewPRO(ProfessionalRecord curricula) {
        ModelAndView result;

        result = createEditModelAndViewPRO(curricula, null);

        return result;
    }

    protected ModelAndView createEditModelAndViewPRO(ProfessionalRecord curricula, String message) {
        ModelAndView result;

        result = new ModelAndView("curricula/editProfessional");
        result.addObject("curricula", curricula);
        result.addObject("message", message);

        return result;

    }

    //ENDORSER RECORD

    @RequestMapping(value = "/editEND", method = RequestMethod.GET)
    public ModelAndView editEND(@RequestParam int curriculaId) {
        ModelAndView result;

        try {
            Curricula curricula = curriculaService.findOne(curriculaId);
            EndorseRecord educationRecord = endorseRecordService.create();
            educationRecord.setCurricula(curricula);
            result = createEditModelAndViewEND(educationRecord);
        } catch (Exception e) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", e.getMessage());
        }



        return result;
    }

    @RequestMapping(value = "/saveEND", method = RequestMethod.POST, params = "save")
    public ModelAndView saveEND(@Valid EndorseRecord curricula, BindingResult binding) {
        ModelAndView result;

        if (binding.hasErrors()) {
            result = createEditModelAndViewEND(curricula);
        } else {
            try {
                curricula.getCurricula().getEndorseRecords().add(curricula);
                endorseRecordService.flush();
//                endorseRecordService.save(curricula);
                result = new ModelAndView("redirect:edit.do");
            } catch (Throwable oops) {
                result = createEditModelAndViewEND(curricula, " property.commit.error");
            }
        }
        return result;
    }

    protected ModelAndView createEditModelAndViewEND(EndorseRecord curricula) {
        ModelAndView result;

        result = createEditModelAndViewEND(curricula, null);

        return result;
    }

    protected ModelAndView createEditModelAndViewEND(EndorseRecord curricula, String message) {
        ModelAndView result;

        result = new ModelAndView("curricula/editEndorser");
        result.addObject("curricula", curricula);
        result.addObject("message", message);

        return result;

    }


    //MISC RECORD

    @RequestMapping(value = "/editMIS", method = RequestMethod.GET)
    public ModelAndView editMIS(@RequestParam int curriculaId) {
        ModelAndView result;

        try {
            Curricula curricula = curriculaService.findOne(curriculaId);
            MiscellaRecord educationRecord = miscellaRecordService.create();
            educationRecord.setCurricula(curricula);
            result = createEditModelAndViewMIS(educationRecord);
        } catch (Exception e) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", e.getMessage());
        }



        return result;
    }

    @RequestMapping(value = "/saveMIS", method = RequestMethod.POST, params = "save")
    public ModelAndView saveMIS(@Valid MiscellaRecord curricula, BindingResult binding) {
        ModelAndView result;

        if (binding.hasErrors()) {
            result = createEditModelAndViewMIS(curricula);
        } else {
            try {
                curricula.getCurricula().getMiscellaRecords().add(curricula);
                miscellaRecordService.flush();
                //miscellaRecordService.save(curricula);
                result = new ModelAndView("redirect:edit.do");
            } catch (Throwable oops) {
                result = createEditModelAndViewMIS(curricula, " property.commit.error");
            }
        }
        return result;
    }

    protected ModelAndView createEditModelAndViewMIS(MiscellaRecord curricula) {
        ModelAndView result;

        result = createEditModelAndViewMIS(curricula, null);

        return result;
    }

    protected ModelAndView createEditModelAndViewMIS(MiscellaRecord curricula, String message) {
        ModelAndView result;

        result = new ModelAndView("curricula/editMisc");
        result.addObject("curricula", curricula);
        result.addObject("message", message);

        return result;

    }

}
