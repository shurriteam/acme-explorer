/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;

import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import security.Authority;
import services.*;

import javax.validation.Valid;
import java.util.*;


@Controller
@RequestMapping("/searcher")
public class SearchController extends AbstractController {

    @Autowired
    private SearchService searchService;
    @Autowired
    private ActorService actorService;
    @Autowired
    private ExplorerService explorerService;
    @Autowired
    private TripService tripService;
   @Autowired
   private Search2Service search2Service;
   @Autowired
   private Utilservice utilservice;

    public SearchController() {
        super();
    }

    //Create Method -----------------------------------------------------------

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView create() {

        ModelAndView result;

        Search search = searchService.create();

        result = createEditModelAndView(search);

        return result;

    }

    @RequestMapping(value = "/createna", method = RequestMethod.GET)
    public ModelAndView createna() {

        ModelAndView result;

        Search search = searchService.create();

        result = createEditModelAndView2(search);

        return result;

    }

    @RequestMapping(value = "/search", method = RequestMethod.POST, params = "save")
    public ModelAndView save(@Valid Search search, BindingResult binding) {
        ModelAndView result;

        if (binding.hasErrors()) {
            result = createEditModelAndView(search);
        } else {
           try {
           Search search1 = searchService.save(search);
            List<AdministratorUtils> administratorUtils  = new ArrayList<>(utilservice.findAll());

            int max = administratorUtils.get(0).getSearchNumber();

               Set<Trip> tripssearched = new HashSet<>(search2Service.deprecatedFinder(search1));

           if (isExplorer()) {
               search1.setSearcher(explorerService.findByPrincipal());
               search1.setTrips(tripssearched);
               search1.setSearchDate(new Date(System.currentTimeMillis() - 100));
               searchService.save(search1);
           }
               result = new ModelAndView("search/result");

               if (tripssearched.size() > max) {
                   Set<Trip> tripssearchedtrunked = new HashSet<>(search2Service.deprecatedFinder(search1).subList(0, max - 1));
                   result.addObject("trips", tripssearchedtrunked);
               } else {
                   result.addObject("trips", tripssearched);
               }


            } catch (Throwable oops) {
                result = createEditModelAndView(search, "general.commit.error");
            }
        }
        return result;
    }

   @RequestMapping(value = "/searchAn", method = RequestMethod.POST, params = "save")
   public ModelAndView savean(@Valid Search search, BindingResult binding) {
      ModelAndView result;

      if (binding.hasErrors()) {
         result = createEditModelAndView2(search);
      } else {
          try{
         Search search1 = searchService.save(search);
          List<AdministratorUtils> administratorUtils  = new ArrayList<>(utilservice.findAll());
          int max = administratorUtils.get(0).getSearchNumber();

              result = new ModelAndView("search/result");

              Set<Trip> tripssearched = new HashSet<>(search2Service.deprecatedFinder(search1));
              if (tripssearched.size() > max) {
                  Set<Trip> tripssearchedtrunked = new HashSet<>(search2Service.deprecatedFinder(search1).subList(0, max - 1));
                  result.addObject("trips", tripssearchedtrunked);
              } else {
                  result.addObject("trips", tripssearched);
              }





            } catch (Throwable oops) {
                result = createEditModelAndView(search, "general.commit.error");
            }
        }
      return result;
   }
    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public ModelAndView view(@RequestParam int searchId) {


        ModelAndView result;

        try {
            Assert.notNull(searchId);

            Search search = searchService.findOne(searchId);

            result = createEditModelAndView(search);
        } catch (Exception e) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", e.getMessage());
        }

        return result;

    }


    protected ModelAndView createEditModelAndView(Search search) {
        ModelAndView result;

        result = createEditModelAndView(search, null);

        return result;
    }

    protected ModelAndView createEditModelAndView(Search search, String message) {
        ModelAndView result;

        result = new ModelAndView("search/view");


        if (isExplorer()) {

            Explorer explorer = explorerService.findByPrincipal();
            Collection<Search> searches = searchService.trunkateSearchByExplorer(explorer);
            result.addObject("mySearchs", searches);

        }


        result.addObject("search", search);
        result.addObject("message", message);

        return result;

    }

    protected ModelAndView createEditModelAndView2(Search search) {
        ModelAndView result;

        result = createEditModelAndView2(search, null);

        return result;
    }

    protected ModelAndView createEditModelAndView2(Search search, String message) {
        ModelAndView result;

       result = new ModelAndView("search/viewAn");

        result.addObject("search", search);
        result.addObject("message", message);

        return result;

    }


    private boolean isExplorer() {

        Boolean res = false;
        Actor actor = actorService.findByPrincipal();
        Authority authority = new ArrayList<>(actor.getUserAccount().getAuthorities()).get(0);
        if (authority.getAuthority().equals("EXPLORER")) {
            res = true;
        }
        return res;
    }


}
