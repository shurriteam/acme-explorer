package controllers;


import domain.SurvivalClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.*;

import javax.validation.Valid;
import java.util.Collection;


/*
 private String title;
    private String description;
    private  Date startDate;
    private Sponsor sponsors;
    private Trip trip;
    private Location location;
    private Collection<Explorer> explorers;



 */
@Controller
@RequestMapping("/survivalClass")
public class SurvivalClassManagerController extends AbstractController {

    @Autowired
    private SurvivalClassService survivalClassService;

    @Autowired
    private ExplorerService explorerService;
   @Autowired
   private ManagerService managerService;
   @Autowired
   private TripService tripService;
   @Autowired
   private SponsorService sponsorService;

    public SurvivalClassManagerController(){super();}

   @RequestMapping(value = "/listManager")
    public ModelAndView list() {
        ModelAndView result;
      Collection<SurvivalClass> survivalClasses = managerService.survivalClassesFromManager(managerService.findByPrincipal());


      result = new ModelAndView("survivalClass/list");
      result.addObject("survivalClasses", survivalClasses);
      result.addObject("requestURI", "survivalClass/listManager.do");

        return result;


    }
//   @RequestMapping(value = "/listExplorer")
//   public ModelAndView listExplorer() {
//      ModelAndView result;
//
//
//
//      result = new ModelAndView("survivalClass/list");
//      result.addObject("survivalClasses", survivalClasses);
//      result.addObject("requestURI", "trip/listShowables.do");
//
//
//
//      result.addObject("requestURI", "survivalClass/listManager.do");
//      return result;
//
//
//   }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public ModelAndView edit(@RequestParam int survivalClassId) {
        ModelAndView result;
        SurvivalClass survivalClass = survivalClassService.findOne(survivalClassId);
        Assert.notNull(survivalClass);
        result = createEditModelAndView(survivalClass);
       result.addObject("trips", managerService.findByPrincipal().getOrganizedTrips());
       result.addObject("sponsors", sponsorService.findAll());
        return result;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
    public ModelAndView save(@Valid SurvivalClass survivalClass, BindingResult binding) {
        ModelAndView result;

        if (binding.hasErrors()) {
            result = createEditModelAndView(survivalClass);
        } else {
            try {

                survivalClassService.save(survivalClass);
               result = new ModelAndView("redirect: listManager.do");


            } catch (Throwable oops) {
                result = createEditModelAndView(survivalClass, "comment.commit.error");
            }
        }

        return result;
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView create() {
        ModelAndView r;
        SurvivalClass m;
        m = survivalClassService.create();

        r = createEditModelAndView(m);
       r.addObject("trips", managerService.findByPrincipal().getOrganizedTrips());
       r.addObject("sponsors", sponsorService.findAll());
        return r;
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public ModelAndView delete(@RequestParam int survivalClassId) {
        ModelAndView result;
       SurvivalClass survivalClass = survivalClassService.findOne(survivalClassId);
       survivalClass.setExplorers(null);
       survivalClass.setTrip(null);

        survivalClassService.delete(survivalClass);
       result = new ModelAndView("redirect: listManager.do");

        return result;
    }


   @RequestMapping(value = "/enroll")
   public ModelAndView enrol(@RequestParam int survivalClassId) {
      ModelAndView result;
      SurvivalClass survivalClass = survivalClassService.findOne(survivalClassId);
      try {
          if (!survivalClass.getExplorers().contains(explorerService.findByPrincipal())) {
              survivalClass.getExplorers().add(explorerService.findByPrincipal());
              explorerService.findByPrincipal().getSurvivalClasses().add(survivalClass);
              survivalClassService.save(survivalClass);
              result = new ModelAndView("administrator/success");
              result.addObject("ok", "you have been registered sucessfully / Has sido registrdo satisfactoriamente");
          } else {
              throw new UnsupportedOperationException("You are registered to this survival class / Ya est�s registrado en esta clase de supervivencia");

          }
      } catch (Throwable oops) {
          result = new ModelAndView("administrator/error");
          result.addObject("trace", oops.getMessage());
      }

      return result;

   }


   @RequestMapping(value = "/unenroll")
   public ModelAndView disenrol(@RequestParam int survivalClassId) {
      ModelAndView result;
      SurvivalClass survivalClass = survivalClassService.findOne(survivalClassId);
      try {
          if(survivalClass.getExplorers().contains(explorerService.findByPrincipal())){
              explorerService.findByPrincipal().getSurvivalClasses().remove(survivalClass);
              survivalClass.getExplorers().remove(explorerService.findByPrincipal());
              survivalClassService.save(survivalClass);
              result = new ModelAndView("administrator/success");
              result.addObject("ok", "you have been unregistered sucessfully / Has sido desregistrdo satisfactoriamente");
          }else {
              throw new UnsupportedOperationException("You are not registered to this survival class / No est�s registrado en esta clase de supervivencia");
          }

      } catch (Throwable oops) {
          result = new ModelAndView("administrator/error");
          result.addObject("trace", oops.getMessage());      }
      return result;

   }




    protected ModelAndView createEditModelAndView(SurvivalClass survivalClass) {
        ModelAndView result;

        result = createEditModelAndView(survivalClass, null);

        return result;
    }

    protected ModelAndView createEditModelAndView(SurvivalClass survivalClass, String message) {
        ModelAndView result;
        result = new ModelAndView("survivalClass/edit");
        result.addObject("survivalClass", survivalClass);


        return result;


    }
}




