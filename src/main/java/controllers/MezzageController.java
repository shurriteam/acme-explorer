/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;


import domain.Actor;
import domain.Folder;
import domain.Mezzage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.ActorService;
import services.MezzageService;
import services.Utilservice;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Controller
@RequestMapping("/mezzage")
public class MezzageController extends AbstractController {

   //Services ----------------------------------------------------------------

   @Autowired
   private MezzageService mezzageService;
   @Autowired
   private ActorService actorService;
   @Autowired
   private Utilservice utilservice;


   //Constructors----------------------------------------------

   public MezzageController() {
      super();
   }

   protected static ModelAndView createEditModelAndView(Mezzage mezzage) {
      ModelAndView result;

      result = createEditModelAndView(mezzage, null);

      return result;
   }


   protected static ModelAndView createEditModelAndView(Mezzage mezzage, String message) {
      ModelAndView result;


      result = new ModelAndView("mezzage/edit");
      result.addObject("mezzage", mezzage);
      result.addObject("message", message);


      return result;

   }


   //Create Method -----------------------------------------------------------


   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView messagesList() {

      ModelAndView result;
      Collection<Mezzage> mezzages;

      mezzages = mezzageService.findAll();
      result = new ModelAndView("mezzage/list");
      result.addObject("mezzages", mezzages);
      result.addObject("requestURI", "mezzage/list.do");

      return result;
   }


   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create() {

      ModelAndView result;

      Mezzage mezzage = mezzageService.create();
      mezzage.setSenderEmail(actorService.findByPrincipal().getEmail());
       mezzage.setMoment(new Date(System.currentTimeMillis() - 100));

      result = createEditModelAndView(mezzage);
      return result;

   }


   // Ancillary methods ------------------------------------------------


   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int mezzageId) {
      ModelAndView result;
      Mezzage mezzage;

      mezzage = mezzageService.findOne(mezzageId);

      Assert.notNull(mezzage);
      result = createEditModelAndView(mezzage);

      return result;
   }


   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid Mezzage mezzage, BindingResult binding) {
      ModelAndView result;
      if (!binding.hasErrors()) {
         result = createEditModelAndView(mezzage);
      } else {
         try {
            mezzage.setSender(actorService.findByPrincipal());
            mezzage.setSenderEmail(actorService.findByPrincipal().getEmail());
             mezzage.setMoment(new Date(System.currentTimeMillis() - 100));
            String recipMail =  mezzage.getReceiverEmail();
             mezzage.setRecipient(actorService.actorByEmail(recipMail));
             mezzageService.textMessage(mezzage);

            result = new ModelAndView("folder/list");
         } catch (Throwable oops) {
            result = createEditModelAndView(mezzage, "general.mezzage.error");
            Collection<Actor> users = actorService.findAll();
            result.addObject("users", users);

         }
      }
      return result;
   }


   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete2(@Valid int mezzageId) {
      ModelAndView result;
      Mezzage mezzage = mezzageService.findOne(mezzageId);


      if (mezzage.getFolder().getName().equals("trashbox")) {

         mezzage.setSender(null);
          mezzage.setRecipient(null);
         mezzage.setFolder(null);
         mezzageService.delete(mezzage);
      } else {
          Folder f = mezzageService.folderByName(actorService.findByPrincipal(), "Trashbox");
         mezzage.setFolder(f);
         mezzageService.save(mezzage);
      }


      result = new ModelAndView("welcome/index");


      return result;
   }


   @RequestMapping(value = "/view", method = RequestMethod.GET)
   public ModelAndView messageView(@RequestParam int mezzageId) {

      ModelAndView result;
      Mezzage mezzage = mezzageService.findOne(mezzageId);
      Collection<Folder> folders = actorService.findByPrincipal().getFolders();

      result = new ModelAndView("mezzage/view");
      result.addObject("senderEmail", mezzage.getSenderEmail());
      result.addObject("receiverEmail", mezzage.getReceiverEmail());
       result.addObject("moment", mezzage.getMoment());
      result.addObject("priority", mezzage.getPriority());
      result.addObject("mezzageId", mezzage.getId());
      result.addObject("subject",mezzage.getSubject());
      result.addObject("body",mezzage.getBody());
      result.addObject("mezzage", mezzage);
      result.addObject("folders", folders);

      result.addObject("isBroadc", new ArrayList<>(utilservice.findAll()).get(0).getBroadcastedMezzanges().contains(mezzage));

      result.addObject("requestURI", "mezzage/view.do");


      return result;
   }

   @RequestMapping(value = "/broadcast", method = RequestMethod.POST, params = "save")
   public ModelAndView broadcast(@Valid Mezzage mezzage, BindingResult binding) {
      ModelAndView result;

      Actor a = actorService.findByPrincipal();
       Folder f = mezzageService.folderByName(a, "Outbox");
      mezzage.setFolder(f);
       mezzage.setMoment(new Date(System.currentTimeMillis() - 100));


      mezzageService.save(mezzage);


      result = new ModelAndView("administrator/action-1");

      return result;
   }


   @RequestMapping(value = "/reply", method = RequestMethod.GET)
   public ModelAndView reply(@RequestParam int mezzageId) {
      ModelAndView result;


      Mezzage mezzage2 = mezzageService.findOne(mezzageId);

      Mezzage mezzage = mezzageService.create();
      mezzage.setSenderEmail(actorService.findByPrincipal().getEmail());
       mezzage.setMoment(new Date(System.currentTimeMillis() - 1000));
      mezzage.setReceiverEmail(mezzage2.getSenderEmail());
      mezzage.setSubject("RE: " + mezzage2.getSubject());

      result = createEditModelAndView(mezzage);
      return result;


   }


   @RequestMapping(value = "/forward", method = RequestMethod.GET)
   public ModelAndView forward(@RequestParam int mezzageId) {
      ModelAndView result;


      Mezzage mezzage2 = mezzageService.findOne(mezzageId);

      Mezzage mezzage = mezzageService.create();
      mezzage.setSenderEmail(actorService.findByPrincipal().getEmail());
       mezzage.setMoment(new Date(System.currentTimeMillis() - 100));
      mezzage.setSubject("FWD: " + mezzage2.getSubject());
      mezzage.setBody(mezzage2.getBody());

      result = createEditModelAndView(mezzage);
      return result;


   }


   @RequestMapping(value = "/movef", method = RequestMethod.POST, params = "save")
   public ModelAndView moveF(@Valid Mezzage mezzage) {

      ModelAndView result;
      mezzageService.save(mezzage);
      result = new ModelAndView("folder/list");
      return result;


   }

}
