/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;


import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import services.*;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/actor")
public class ActorController extends AbstractController {

   //Services ----------------------------------------------------------------

   @Autowired
   private ActorService actorService;
   @Autowired
   private SponsorService sponsorService;
   @Autowired
   private ManagerService managerService;
   @Autowired
   private AdministratorService administratorService;
   @Autowired
   private ExplorerService explorerService;
   @Autowired
   private RangerService rangerService;
   @Autowired
   private AuditorService auditorService;


   //Constructors----------------------------------------------

   public ActorController() {
      super();
   }


   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView actorList() {

      ModelAndView result;
      Collection<Actor> actors;

      actors = actorService.findAll();
      result = new ModelAndView("actor/list");
      result.addObject("actors", actors);
      result.addObject("requestURI", "actor/list.do");

      return result;
   }


   @RequestMapping(value = "/editRanger", method = RequestMethod.GET)
   public ModelAndView editRanger() {
      ModelAndView result;
      Ranger actor;
      actor = rangerService.findByPrincipal();
      Assert.notNull(actor);
      result = createEditModelAndViewRanger(actor);

      return result;
   }

   @RequestMapping(value = "/editAdministrator", method = RequestMethod.GET)
   public ModelAndView editAdmin() {
      ModelAndView result;
      Administrator actor;
      actor = administratorService.findByPrincipal();
      Assert.notNull(actor);
      result = createEditModelAndViewAdministrator(actor);

      return result;
   }

   @RequestMapping(value = "/editManager", method = RequestMethod.GET)
   public ModelAndView editManager() {
      ModelAndView result;
      Manager actor;
      actor = managerService.findByPrincipal();
      Assert.notNull(actor);
      result = createEditModelAndViewManager(actor);

      return result;
   }

   @RequestMapping(value = "/editSponsor", method = RequestMethod.GET)
   public ModelAndView editSponsor() {
      ModelAndView result;
      Sponsor actor;
      actor = sponsorService.findByPrincipal();
      Assert.notNull(actor);
      result = createEditModelAndViewSponsor(actor);

      return result;
   }

   @RequestMapping(value = "/editExplorer", method = RequestMethod.GET)
   public ModelAndView editExplorer() {
      ModelAndView result;
      Explorer actor;
      actor = explorerService.findByPrincipal();
      Assert.notNull(actor);
      result = createEditModelAndViewExplorer(actor);

      return result;
   }

   @RequestMapping(value = "/editAuditor", method = RequestMethod.GET)
   public ModelAndView editAuditor() {
      ModelAndView result;
      Auditor actor;
      actor = auditorService.findByPrincipal();
      Assert.notNull(actor);
      result = createEditModelAndViewAuditor(actor);

      return result;
   }

   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid Actor actor, BindingResult binding) {
      ModelAndView result;

      if (binding.hasErrors()) {
         result = createEditModelAndView(actor);
      } else {
         try {

            actorService.save(actor);
            result = new ModelAndView("/profile.do");
         } catch (Throwable oops) {
            result = createEditModelAndView(actor, "general.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/editAdministrator", method = RequestMethod.POST, params = "save")
   public ModelAndView saveAdministator(@Valid Administrator administrator, BindingResult binding) {
      ModelAndView result;

      if (binding.hasErrors()) {
         result = createEditModelAndViewAdministrator(administrator);
      } else {
         try {
            administratorService.save(administrator);
            result = new ModelAndView("welcome/index");
         } catch (Throwable oops) {
            result = createEditModelAndViewAdministrator(administrator, "general.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/editRanger", method = RequestMethod.POST, params = "save")
   public ModelAndView saveRanger(@Valid Ranger ranger, BindingResult binding) {
      ModelAndView result;

      if (binding.hasErrors()) {
         result = createEditModelAndViewRanger(ranger);
      } else {
         try {
            rangerService.save(ranger);
            result = new ModelAndView("welcome/index");
         } catch (Throwable oops) {
            result = createEditModelAndViewRanger(ranger, "general.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/editExplorer", method = RequestMethod.POST, params = "save")
   public ModelAndView saveExplorer(@Valid Explorer explorer, BindingResult binding) {
      ModelAndView result;

      if (binding.hasErrors()) {
         result = createEditModelAndViewExplorer(explorer);
      } else {
         try {
            explorerService.save(explorer);
            result = new ModelAndView("welcome/index");
         } catch (Throwable oops) {
            result = createEditModelAndViewExplorer(explorer, "general.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/editSponsor", method = RequestMethod.POST, params = "save")
   public ModelAndView saveSponsor(@Valid Sponsor actor, BindingResult binding) {
      ModelAndView result;

      if (binding.hasErrors()) {
         result = createEditModelAndViewSponsor(actor);
      } else {
         try {
            sponsorService.save(actor);
            result = new ModelAndView("welcome/index");
         } catch (Throwable oops) {
            result = createEditModelAndViewSponsor(actor, "general.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/editManager", method = RequestMethod.POST, params = "save")
   public ModelAndView saveManager(@Valid Manager manager, BindingResult binding) {
      ModelAndView result;

      if (binding.hasErrors()) {
         result = createEditModelAndViewManager(manager);
      } else {
         try {
            managerService.save(manager);
            result = new ModelAndView("welcome/index");
         } catch (Throwable oops) {
            result = createEditModelAndViewManager(manager, "actor.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/editAuditor", method = RequestMethod.POST, params = "save")
   public ModelAndView saveAuditor(@Valid Auditor manager, BindingResult binding) {
      ModelAndView result;

      if (binding.hasErrors()) {
         result = createEditModelAndViewAuditor(manager);
      } else {
         try {
            auditorService.save(manager);
            result = new ModelAndView("welcome/index");
         } catch (Throwable oops) {
            result = createEditModelAndViewAuditor(manager, "actor.commit.error");
         }
      }
      return result;
   }

//   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
//   public ModelAndView delete(Actor actor) {
//      ModelAndView result;
//      try {
//         actorService.delete(actor);
//         result = new ModelAndView("redirect:list.do");
//      } catch (Throwable oops) {
//         result = createEditModelAndView(actor, "actor.commit.error");
//      }
//
//      return result;
//   }


   // Ancillary methods ------------------------------------------------

   protected ModelAndView createEditModelAndView(Actor actor) {
      ModelAndView result;

      result = createEditModelAndView(actor, null);

      return result;
   }

   protected ModelAndView createEditModelAndView(Actor actor, String message) {
      ModelAndView result;

      result = new ModelAndView("actor/edit");
      result.addObject("actor", actor);
      result.addObject("message", message);

      return result;

   }

   protected ModelAndView createEditModelAndViewRanger(Ranger actor) {
      ModelAndView result;

      result = createEditModelAndViewRanger(actor, null);

      return result;
   }

   protected ModelAndView createEditModelAndViewRanger(Ranger actor, String message) {
      ModelAndView result;

      result = new ModelAndView("actor/editRanger");
      result.addObject("ranger", actor);
      result.addObject("message", message);

      return result;

   }

   protected ModelAndView createEditModelAndViewManager(Manager actor) {
      ModelAndView result;

      result = createEditModelAndViewManager(actor, null);

      return result;
   }

   protected ModelAndView createEditModelAndViewManager(Manager actor, String message) {
      ModelAndView result;

      result = new ModelAndView("actor/editManager");
      result.addObject("manager", actor);
      result.addObject("message", message);

      return result;

   }

   protected ModelAndView createEditModelAndViewExplorer(Explorer actor) {
      ModelAndView result;

      result = createEditModelAndViewExplorer(actor, null);

      return result;
   }

   protected ModelAndView createEditModelAndViewExplorer(Explorer actor, String message) {
      ModelAndView result;

      result = new ModelAndView("actor/editExplorer");
      result.addObject("explorer", actor);
      result.addObject("message", message);

      return result;

   }

   protected ModelAndView createEditModelAndViewSponsor(Sponsor actor) {
      ModelAndView result;

      result = createEditModelAndViewSponsor(actor, null);

      return result;
   }

   protected ModelAndView createEditModelAndViewSponsor(Sponsor actor, String message) {
      ModelAndView result;

      result = new ModelAndView("actor/editSponsor");
      result.addObject("sponsor", actor);
      result.addObject("message", message);

      return result;

   }

   protected ModelAndView createEditModelAndViewAdministrator(Administrator actor) {
      ModelAndView result;

      result = createEditModelAndViewAdministrator(actor, null);

      return result;
   }

   protected ModelAndView createEditModelAndViewAdministrator(Administrator actor, String message) {
      ModelAndView result;

      result = new ModelAndView("actor/editAdministrator");
      result.addObject("administrator", actor);
      result.addObject("message", message);

      return result;

   }

   protected ModelAndView createEditModelAndViewAuditor(Auditor actor) {
      ModelAndView result;

      result = createEditModelAndViewAuditor(actor, null);

      return result;
   }

   protected ModelAndView createEditModelAndViewAuditor(Auditor actor, String message) {
      ModelAndView result;

      result = new ModelAndView("actor/editAuditor");
      result.addObject("auditor", actor);
      result.addObject("message", message);

      return result;

   }
}

