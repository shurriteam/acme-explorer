/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;

import domain.Category;
import domain.Trip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.CategoryService;
import services.TripService;

import javax.validation.Valid;
import java.util.Collection;


@Controller
@RequestMapping("/category")
public class CategoryController extends AbstractController {

   @Autowired
   private CategoryService categoryService;
   @Autowired
   private TripService tripService;

   public CategoryController() {
      super();
   }

   @RequestMapping(value = "/list")
   public ModelAndView list() {
      ModelAndView result;
      Collection<Category> aux = categoryService.findAll();
      result = new ModelAndView("category/list");
      result.addObject("masterId", categoryService.categoryMaster());
      result.addObject("categories", aux);
      result.addObject("father", categoryService.findAll());
      result.addObject("sons", categoryService.findAll());
      result.addObject("requestURI", "category/list.do");
      return result;


   }


   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int categoryId) {
      ModelAndView result;
      Category categorie = categoryService.findOne(categoryId);
      Assert.notNull(categorie);
      result = createEditModelAndView(categorie);
      result.addObject("father", categoryService.findAll());
      result.addObject("sons", categoryService.findAll());
      return result;
   }

   @RequestMapping(value = "/view", method = RequestMethod.GET)
   public ModelAndView view(@RequestParam int categoryId) {

      ModelAndView result;
      try {
         Category categorie = categoryService.findOne(categoryId);
         Assert.notNull(categorie);
         result = new ModelAndView("category/view");
         result.addObject("trips", categorie.getRelatedTrips());
         result.addObject("name", categorie.getName());
         result.addObject("sons", categorie.getSons());
         result.addObject("id", categorie.getId());
         result.addObject("requestURI", "category/view.do");
         return result;
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }

      return result;
   }

   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid Category categorie, BindingResult binding) {
      ModelAndView result;

      if (binding.hasErrors()) {
         result = createEditModelAndView(categorie);
      } else {
         try {

            categoryService.save(categorie);
            result = new ModelAndView("redirect:list.do");

         } catch (Throwable oops) {
            result = createEditModelAndView(categorie, "comment.commit.error");
            result.addObject("father", categoryService.findAll());
            result.addObject("sons", categoryService.findAll());
         }
      }

      return result;
   }

   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create(@RequestParam int categoryId) {
      ModelAndView r;
      try {
         Category m;
         Category father = categoryService.findOne(categoryId);
         m = categoryService.create();
         m.setName("_");
         m.setFather(father);
         father.getSons().add(m);
         r = createEditModelAndView(m);
         r.addObject("father", categoryService.findAll());
         r.addObject("sons", categoryService.findAll());
      } catch (Exception e) {
         r = new ModelAndView("administrator/error");
         r.addObject("trace", e.getMessage());
      }
      return r;
   }

   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete(@RequestParam int categoryId) {
      ModelAndView result;
      try {
      if (categoryId == categoryService.categoryMaster()) {
         throw new UnsupportedOperationException("That is the main top category, it can't be deleted / Esta es la categor�a padre del �rbol, no puede ser borrada");
      }
      Category category = categoryService.findOne(categoryId);
         categoryService.deleteCategory(category);
         categoryService.delete(category);
         categoryService.flush();
         result = new ModelAndView("redirect: list.do");
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }

      return result;
   }


   @RequestMapping(value = "/viewTrips")
   public ModelAndView viewTrips(@RequestParam int categoryId) {
      ModelAndView result;
      Category category = categoryService.findOne(categoryId);

      Collection<Trip> trips =  tripService.tripsByCategory(category);

      result = new ModelAndView("trip/list");
      result.addObject("trips", trips);
      result.addObject("requestURI", "trips/list.do");
      return result;


   }



   protected ModelAndView createEditModelAndView(Category categorie) {
      ModelAndView result;

      result = createEditModelAndView(categorie, null);

      return result;
   }

   protected ModelAndView createEditModelAndView(Category categorie, String message) {
      ModelAndView result;
      result = new ModelAndView("category/edit");
      result.addObject("category", categorie);


      return result;


   }
}
