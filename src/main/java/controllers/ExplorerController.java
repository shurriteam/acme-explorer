/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;

import domain.Explorer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.ExplorerService;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/explorer")
public class ExplorerController extends AbstractController {


   //Services ----------------------------------------------------------------

   @Autowired
   private ExplorerService explorerService;


   //Constructors----------------------------------------------

   public ExplorerController() {
      super();
   }


   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView explorerList() {

      ModelAndView result;
      Collection<Explorer> explorer;
      explorer = explorerService.findAll();
      result = new ModelAndView("explorer/list");
      result.addObject("explorer", explorer);
      result.addObject("requestURI", "explorer/list.do");

      return result;
   }


   //Create Method -----------------------------------------------------------

//   @RequestMapping(value = "/create", method = RequestMethod.GET)
//   public ModelAndView create() {
//
//      ModelAndView result;
//
//      Explorer explorer = explorerService.create();
//      result = createEditModelAndView(explorer);
//
//      return result;
//
//   }

   protected static ModelAndView createEditModelAndView2(Explorer user) {
      ModelAndView result;

      result = createEditModelAndView2(user, null);

      return result;
   }

   protected static ModelAndView createEditModelAndView2(Explorer user, String message) {
      ModelAndView result;

      result = new ModelAndView("explorer/register");
      result.addObject("explorer", user);
      result.addObject("message", message);

      return result;

   }

   // Edition ---------------------------------------------------------

   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int explorerId) {
      ModelAndView result;
      Explorer explorer;

      explorer = explorerService.findOne(explorerId);
      Assert.notNull(explorer);
      result = createEditModelAndView(explorer);
      result.addObject("explorer", explorer);


      return result;
   }

   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid Explorer explorer, BindingResult binding) {
      ModelAndView result;

      if (binding.hasErrors()) {
         result = createEditModelAndView(explorer);
      } else {
         try {
            explorerService.save(explorer);
            result = new ModelAndView("redirect:list.do");
         } catch (Throwable oops) {
            result = createEditModelAndView(explorer, "property.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete(@RequestParam int explorerId) {
      ModelAndView result;
      Explorer p = explorerService.findOne(explorerId);

      try {
         explorerService.delete(p);
         result = new ModelAndView("redirect:list.do");
      } catch (Throwable oops) {
         result = createEditModelAndView(p, "explorer.commit.error");
      }


      return result;
   }

   @RequestMapping(value = "/view", method = RequestMethod.GET)
   public ModelAndView view(@RequestParam int explorerId) {
      ModelAndView res;
      Explorer p = explorerService.findOne(explorerId);
      res = new ModelAndView("explorer/view");
      res.addObject("explorer", p);

      return res;
   }

   // Ancillary methods ------------------------------------------------

   protected ModelAndView createEditModelAndView(Explorer explorer) {
      ModelAndView result;

      result = createEditModelAndView(explorer, null);

      return result;
   }

   protected ModelAndView createEditModelAndView(Explorer explorer, String message) {
      ModelAndView result;

      result = new ModelAndView("explorer/edit");
      result.addObject("explorer", explorer);
      result.addObject("message", message);

      return result;

   }

   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create2(){
      ModelAndView result;
      Explorer user = explorerService.create();
      result = createEditModelAndView2(user);
      return result;
   }
   // Edition ---------------------------------------------------------

   @RequestMapping(value = "/register", method = RequestMethod.POST, params = "save")
   public ModelAndView register(@Valid Explorer user, BindingResult binding) {
      ModelAndView result;

      if (!binding.hasErrors()) {
         result = createEditModelAndView2(user);
      } else {
         try {
            explorerService.registerAsExplorer(user);
            result = new ModelAndView("welcome/index");
         } catch (Throwable oops) {
            result = createEditModelAndView2(user, "general.commit.error");
         }
      }
      return result;
   }
}








