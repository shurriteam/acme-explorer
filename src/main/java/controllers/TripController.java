package controllers;


import converters.CategoryToStringConverter;
import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import security.Authority;
import services.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static org.hibernate.jpa.internal.QueryImpl.LOG;

@Controller
@RequestMapping("/trip")

public class TripController extends AbstractController {


    @Autowired
    private TripService tripService;
    @Autowired
    private ManagerService managerService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private RangerService rangerService;
    @Autowired
    private LegalTextService legalTextService;
   @Autowired
   private StageService stageService;
   @Autowired
   private TagService tagService;
   @Autowired
   private RequirementService requirementService;
   @Autowired
   private ActorService actorService;
   @Autowired
   private SponsorService sponsorService;
   @Autowired
   private ExplorerService explorerService;
   @Autowired
   private TripApplicationService tripApplicationService;


    //Constructors----------------------------------------------

    public TripController() {
        super();
    }


    @RequestMapping(value = "/listShowables", method = RequestMethod.GET)
    public ModelAndView tripList() {

        ModelAndView result;

        Collection<Trip> allTrips = tripService.showableTrip();

        result = new ModelAndView("trip/list");
        result.addObject("trips", allTrips);
        result.addObject("requestURI", "trip/listShowables.do");

        return result;

    }

    @RequestMapping(value = "/myTrips", method = RequestMethod.GET)
    public ModelAndView ManagerTripList() {

        ModelAndView result;
        Manager manager = managerService.findByPrincipal();

        Collection<Trip> myTrips = manager.getOrganizedTrips();

        Collection<Trip> allTrips = tripService.showableTrip();



        result = new ModelAndView("trip/list");
        result.addObject("trips", allTrips);
        result.addObject("myTrips", myTrips);
        result.addObject("requestURI", "trip/myTrips.do");

        return result;

    }
//    //Create Method -----------------------------------------------------------
//
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView create() {

        ModelAndView result;

       Trip note = tripService.create();

       result = createEditModelAndView(note);

        result.addObject("categories",categoryService.findAll());
        result.addObject("guidess",rangerService.findAll());
        result.addObject("legalTexts", legalTextService.publishedLT());
       result.addObject("stages", stageService.findAll());
       result.addObject("tags", tagService.findAll());
       result.addObject("requirements", requirementService.findAll());
       result.addObject("tag",tagService.findAll());


       return result;

    }

    // Edition ---------------------------------------------------------

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public ModelAndView edit(@RequestParam int tripId) {
        ModelAndView result;
        Trip note;

       try {
          note = tripService.findOne(tripId);
          Assert.notNull(note);
          Assert.isTrue(managerService.findByPrincipal().getOrganizedTrips().contains(note), "That trip it's not yours / Este viaje no es tuyo");
          Assert.isTrue(!(note.getPublicationDate().before(new Date(System.currentTimeMillis() - 100))) || (note.getPublicationDate().after(new Date(System.currentTimeMillis() - 100)) &&
                          note.isCancelled()),
                  "Trip is already published or not cancelled / El viaje est� publicado o no se ha cancelado");

          result = createEditModelAndView(note);
          result.addObject("trip", note);
          result.addObject("categories", categoryService.findAll());
          result.addObject("guidess", rangerService.findAll());
          result.addObject("legalTexts", legalTextService.findAll());
          result.addObject("stages", stageService.findAll());
          result.addObject("tags", tagService.findAll());
          result.addObject("requirements", requirementService.findAll());
          result.addObject("cancelled", note.isCancelled());
       } catch (Exception e) {
          result = new ModelAndView("administrator/error");
          result.addObject("trace", e.getMessage());
       }

       return result;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
    public ModelAndView save(@Valid Trip note, BindingResult binding) {
       ModelAndView result;
       if (binding.hasErrors()) {
          result = createEditModelAndView(note);
       } else {
          try {

             Manager manager = managerService.findByPrincipal();
             note.setManager(manager);
             String ticker = tripService.tickerGenerator(note);
             note.setPrice(0.0);
             note.setTicker(ticker);
             Collection<LegalText> lt = note.getLegalTexts();
             note.setLegalTexts(lt);
             tripService.save(note);

             result = new ModelAndView("redirect: myTrips.do");
          } catch (Throwable oops) {
             result = createEditModelAndView(note, "general.commit.error");
          }
       }
       return result;
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public ModelAndView delete(@RequestParam int tripId) {
        ModelAndView result;
        Trip n = tripService.findOne(tripId);


        try {
           Assert.isTrue(!n.getPublicationDate().before(new Date(System.currentTimeMillis() - 100))
                   || n.isCancelled(), "Trip is already published or not cancelled / El viaje est� publicado o no se ha cancelado");
           n.setSurvivalClasses(null);
           n.setSearches(null);
           n.setAudits(null);
           n.setNotes(null);
           n.setSponsorships(null);
           n.setTripApplications(null);
           n.setStages(null);
           n.setManager(null);
           n.setLegalTexts(null);
           n.setTags(null);
            tripService.delete(n);
           result = new ModelAndView("redirect: myTrips.do");
        } catch (Throwable oops) {
           result = new ModelAndView("administrator/error");
           result.addObject("trace", oops.getMessage());
        }


        return result;
    }

   @RequestMapping(value = "/viewFromTripApp", method = RequestMethod.GET)
   public ModelAndView view222222(@RequestParam int tripApplicationId) {
      ModelAndView result;
      result = new ModelAndView("trip/view");

      try {
         Assert.notNull(tripApplicationId, "Trip ID null");
         LOG.info("Loading trip View... of trip with id" + tripApplicationId);
         TripApplication ta = tripApplicationService.findOne(tripApplicationId);
         Trip trip = ta.getTrip();

         if (actorService.findByPrincipal() != null) {
            Actor actor = actorService.findByPrincipal();
            LOG.info("Comprobamos la identidad del actor que est� autentificado para comprobar segun que cosas");


            Authority authority = new ArrayList<>(actor.getUserAccount().getAuthorities()).get(0);
            switch (authority.getAuthority()) {
               case "SPONSOR":
                  Sponsor sponsor = sponsorService.findByPrincipal();
                  for (Sponsorship sp : trip.getSponsorships()) {
                     if (sp.getSponsors().equals(sponsor)) {
                        result.addObject("isMySponsorship", true);
                     }
                  }
                  break;
               case "MANAGER":
                  Manager manager = managerService.findByPrincipal();
                  if (manager.getOrganizedTrips().contains(trip)) {
                     Date monthAgo = new Date(System.currentTimeMillis() - 30 * 24 * 3600 * 1000l);
                     if (trip.getStartDate().after(monthAgo)) {
                        result.addObject("nextM", true);
                     }
                     result.addObject("isMy", true);
                     result.addObject("applications", trip.getTripApplications());

                  }
                  break;
               case "EXPLORER":
                  Explorer explorer = explorerService.findByPrincipal();
                  if (tripService.explorersbyTrip(trip.getId()).contains(explorer)) {
                     result.addObject("registered", true);

                     if (tripApplicationService.tripAExplorerTrip(explorer, trip).getStatus().equals(Status.ACCEPTED)) {
                        result.addObject("writer", true);
                     }
                  }
                  break;
            }
         }

         CategoryToStringConverter categoryToStringConverter = new CategoryToStringConverter();
//            Header box information
         result.addObject("image", trip.getImage());
         result.addObject("title", trip.getTitle());
         result.addObject("ticker", trip.getTicker());
         result.addObject("isCancelled", trip.isCancelled());
         result.addObject("tripid", trip.getId());

         String nameCategory = categoryToStringConverter.convert(trip.getCategory());

         result.addObject("category1", trip.getCategory().getName());
         result.addObject("cancelledReason", trip.getCancelReason());
         result.addObject("description", trip.getDescription());
         result.addObject("price", trip.getPrice().toString());
         result.addObject("startDate", trip.getStartDate());
         result.addObject("endDate", trip.getEndDate());
         result.addObject("gimg", trip.getGuide().getImage());
         result.addObject("raangerId", trip.getGuide().getId());
//            MAIN BOX INFORMATION
         result.addObject("guideName", trip.getGuide().getName());
         result.addObject("guideSurname", trip.getGuide().getSurname());

         result.addObject("stages", trip.getStages());
         if (!trip.getSponsorships().isEmpty()) {
            result.addObject("sponsorships", randomSponsorship(trip.getSponsorships()));
         }
         result.addObject("survivalClasses", trip.getSurvivalClasses());
//
//
         result.addObject("notes", trip.getNotes());
         result.addObject("audits", trip.getAudits());
         result.addObject("stories", trip.getStories());
//

         result.addObject("publicationDate", trip.getPublicationDate());
         result.addObject("manager", trip.getManager());
         result.addObject("requirements", trip.getRequirements());
         result.addObject("legalTexts", trip.getLegalTexts());
         result.addObject("tags", trip.getTags());
         result.addObject("requestURI", "trip/viewFromTripApp.do");


      } catch (Exception e) {
         e.printStackTrace();
      }

      return result;
   }


    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public ModelAndView view(@RequestParam int tripId) {
        ModelAndView result;
        result = new ModelAndView("trip/view");

        try {
           Assert.notNull(tripId, "Trip ID null");
           LOG.info("Loading trip View... of trip with id" + tripId);
           Trip trip = tripService.findOne(tripId);

            if (actorService.findByPrincipal() != null) {
                Actor actor = actorService.findByPrincipal();
                LOG.info("Comprobamos la identidad del actor que est� autentificado para comprobar segun que cosas");


                Authority authority = new ArrayList<>(actor.getUserAccount().getAuthorities()).get(0);
                switch (authority.getAuthority()) {
                    case "SPONSOR":
                        Sponsor sponsor = sponsorService.findByPrincipal();
                        for (Sponsorship sp : trip.getSponsorships()) {
                            if (sp.getSponsors().equals(sponsor)) {
                                result.addObject("isMySponsorship", true);
                            }
                        }
                        break;
                    case "MANAGER":
                        Manager manager = managerService.findByPrincipal();
                        if (manager.getOrganizedTrips().contains(trip)) {
                            Date monthAgo = new Date(System.currentTimeMillis() - 30 * 24 * 3600 * 1000l);
                            if(trip.getStartDate().after(monthAgo)){
                                result.addObject("nextM", true);
                            }
                            result.addObject("isMy", true);
                            result.addObject("applications", trip.getTripApplications());

                        }
                        break;
                    case "EXPLORER":
                        Explorer explorer = explorerService.findByPrincipal();
                        if (tripService.explorersbyTrip(tripId).contains(explorer)) {
                            result.addObject("registered", true);

                            if(tripApplicationService.tripAExplorerTrip(explorer,trip).getStatus().equals(Status.ACCEPTED)){
                                result.addObject("writer", true);
                            }

                            if (tripService.explorersInSv(trip).contains(explorer)) {
                                result.addObject("rolled", true);
                            }
                        }
                        break;
                }
            }

            CategoryToStringConverter categoryToStringConverter =  new CategoryToStringConverter();
//            Header box information
            result.addObject("image",trip.getImage());
            result.addObject("title", trip.getTitle());
            result.addObject("ticker",trip.getTicker());
            result.addObject("isCancelled",trip.isCancelled());
            result.addObject("tripid", trip.getId());

            String nameCategory = categoryToStringConverter.convert(trip.getCategory());

           result.addObject("category1", trip.getCategory().getName());
            result.addObject("cancelledReason",trip.getCancelReason());
            result.addObject("description",trip.getDescription());
            result.addObject("price", trip.getPrice().toString());
            result.addObject("startDate",trip.getStartDate());
            result.addObject("endDate",trip.getEndDate());
           result.addObject("gimg", trip.getGuide().getImage());
           result.addObject("raangerId", trip.getGuide().getId());
//            MAIN BOX INFORMATION
           result.addObject("guideName", trip.getGuide().getName());
           result.addObject("guideSurname", trip.getGuide().getSurname());

            result.addObject("stages",trip.getStages());
           if (!trip.getSponsorships().isEmpty()) {
              result.addObject("sponsorships", randomSponsorship(trip.getSponsorships()));
           }
            result.addObject("survivalClasses", trip.getSurvivalClasses());
//
//
           result.addObject("notes", trip.getNotes());
            result.addObject("audits", trip.getAudits());
            result.addObject("stories", trip.getStories());
//

            result.addObject("publicationDate", trip.getPublicationDate());
            result.addObject("manager", trip.getManager());
            result.addObject("requirements", trip.getRequirements());
            result.addObject("legalTexts", trip.getLegalTexts());
            result.addObject("tags",trip.getTags());
           result.addObject("requestURI", "trip/view.do");


        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

   @RequestMapping(value = "/viewAnonimous", method = RequestMethod.GET)
   public ModelAndView viewAnonimous(@RequestParam int tripId) {
      ModelAndView result;
      result = new ModelAndView("trip/view");

      try {

         Trip trip = tripService.findOne(tripId);


         CategoryToStringConverter categoryToStringConverter = new CategoryToStringConverter();
//            Header box information
         result.addObject("image", trip.getImage());
         result.addObject("title", trip.getTitle());
         result.addObject("ticker", trip.getTicker());
         result.addObject("isCancelled", trip.isCancelled());

         String nameCategory = categoryToStringConverter.convert(trip.getCategory());

         result.addObject("category1", trip.getCategory().getName());
         result.addObject("cancelledReason", trip.getCancelReason());
         result.addObject("description", trip.getDescription());
         result.addObject("price", trip.getPrice().toString());
         result.addObject("startDate", trip.getStartDate());
         result.addObject("endDate", trip.getEndDate());
         result.addObject("gimg", trip.getGuide().getImage());
          result.addObject("tripid", trip.getId());


//            MAIN BOX INFORMATION
         result.addObject("guideName", trip.getGuide().getName());
         result.addObject("guideSurname", trip.getGuide().getSurname());

         result.addObject("stages", trip.getStages());
         if (!trip.getSponsorships().isEmpty()) {
            result.addObject("sponsorships", randomSponsorship(trip.getSponsorships()));
         }
         result.addObject("survivalClasses", trip.getSurvivalClasses());

         result.addObject("notes", trip.getNotes());
         result.addObject("audits", trip.getAudits());
         result.addObject("stories", trip.getStories());

          result.addObject("publicationDate", trip.getPublicationDate());
          result.addObject("manager", trip.getManager());
          result.addObject("requirements", trip.getRequirements());
          result.addObject("legalTexts", trip.getLegalTexts());
          result.addObject("tags",trip.getTags());
         result.addObject("requestURI", "trip/viewAnonimous.do");

      } catch (Exception e) {
         e.printStackTrace();
      }

      return result;
   }

   private Collection<Sponsorship> randomSponsorship(Collection<Sponsorship> sponsorships){
       List<Sponsorship> res = new ArrayList<Sponsorship>();

       try {
           List<Sponsorship> sponsorships1 =  new ArrayList<>(sponsorships);

          int randomNum = ThreadLocalRandom.current().nextInt(0, sponsorships.size());

           res.add(sponsorships1.get(randomNum));


       } catch (Exception e) {
           e.printStackTrace();
       }
       return res;
   }

   @RequestMapping(value = "/cancel2", method = RequestMethod.GET)
   public ModelAndView cancel2(@RequestParam int tripId) {
      ModelAndView result;
      Trip note;

      try {
         note = tripService.findOne(tripId);
         Assert.notNull(note);
         Assert.isTrue(note.getStartDate().after(new Date(System.currentTimeMillis() - 100)),
                 "Trip has started already / El viaje ya comenz�");

         result = new ModelAndView("trip/cancel");
         result.addObject("trip", note);
         result.addObject("requestURI", "trip/cancel2.do");
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }

      return result;
   }

   @RequestMapping(value = "/cancel2", method = RequestMethod.POST, params = "cancel2")
   public ModelAndView cancel2(@Valid Trip note, BindingResult binding) {
      ModelAndView result;

      if (binding.hasErrors()) {
         result = createEditModelAndView3(note);
      } else {
         try {
 //           Assert.isTrue(note.getPublicationDate().after(new Date(System.currentTimeMillis() - 100))
   //                 && note.getStartDate().before(new Date(System.currentTimeMillis() - 100)), "Trip has started already / El viaje ya comenz�");
//            String ticker = tripService.tickerGenerator(note);
//            note.setTicker(ticker);
//            note.setCancelled(true);
            // Trip note2 = note;
            note.setCancelled(true);
            tripService.save(note);
            result = new ModelAndView("redirect: myTrips.do");
         } catch (Throwable oops) {
            result = createEditModelAndView3(note, "general.commit.error");
         }
      }
      return result;
   }

//
//    @RequestMapping(value = "/view", method = RequestMethod.GET)
//    public ModelAndView view(@RequestParam int propertyId) {
//        ModelAndView res;
//        Note e = noteService.findOne(propertyId);
//        res = new ModelAndView("note/view");
//        res.addObject("note", e);
//
//        return res;
//    }
//
//    // Ancillary methods ------------------------------------------------
//
    protected ModelAndView createEditModelAndView(Trip note) {
        ModelAndView result;

        result = createEditModelAndView(note, null);


        return result;
    }

    protected ModelAndView createEditModelAndView(Trip note, String message) {
        ModelAndView result;

        result = new ModelAndView("trip/edit");
        result.addObject("trip", note);
        result.addObject("message", message);

        return result;

    }

   protected ModelAndView createEditModelAndView3(Trip note) {
      ModelAndView result;

      result = createEditModelAndView3(note, null);


      return result;
   }

   protected ModelAndView createEditModelAndView3(Trip note, String message) {
      ModelAndView result;

      result = new ModelAndView("trip/cancel");
      result.addObject("trip", note);
      result.addObject("message", message);

      return result;

   }


   //Manage Stages

    @RequestMapping(value = "/createStage", method = RequestMethod.GET)
    public ModelAndView createStage(@RequestParam int tripId) {

        ModelAndView result;

        Stage stage = stageService.create();
        Trip trip = tripService.findOne(tripId);
        int stageSize = trip.getStages().size();
        stage.setNumberOfStage(stageSize+1);
        stage.setTitle("_");
        stage.setDescription("_");
        stage.setPrice(0.0);
        stage.setTrip(trip);


        result = createEditModelAndViewStage(stage);




        return result;

    }

    protected ModelAndView createEditModelAndViewStage(Stage note) {
        ModelAndView result;

        result = createEditModelAndViewStage(note, null);


        return result;
    }

    protected ModelAndView createEditModelAndViewStage(Stage note, String message) {
        ModelAndView result;

        result = new ModelAndView("trip/createStage");
        result.addObject("stage", note);
        result.addObject("message", message);

        return result;

    }

    @RequestMapping(value = "/editStage", method = RequestMethod.POST, params = "save")
    public ModelAndView editStage(@Valid Stage note, BindingResult binding) {
        ModelAndView result;

        if (binding.hasErrors()) {
            result = createEditModelAndViewStage(note);
        } else {
            try {
                note.getTrip().getStages().add(note);
                stageService.flush();
                //stageService.save(note);
                result = new ModelAndView("redirect: myTrips.do");
            } catch (Throwable oops) {
                result = createEditModelAndViewStage(note, "general.commit.error");
            }
        }
        return result;
    }

}
