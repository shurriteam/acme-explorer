package controllers;


import domain.Note;
import domain.Trip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.AuditorService;
import services.ManagerService;
import services.NoteService;
import services.TripService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Controller
@RequestMapping("/note")

public class NoteController extends AbstractController{


    @Autowired
    private NoteService noteService;
   @Autowired
   private ManagerService managerService;
   @Autowired
   private AuditorService auditorService;
   @Autowired
   private TripService tripService;

    //Constructors----------------------------------------------

    public NoteController() {
        super();
    }


    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView noteList() {

        ModelAndView result;
        Collection<Note> notes;
       notes = managerService.findByPrincipal().getNotes();
       result = new ModelAndView("note/list");
       result.addObject("notes", notes);
        result.addObject("requestURI", "note/list.do");

        return result;
    }

   @RequestMapping(value = "/listAuditor", method = RequestMethod.GET)
   public ModelAndView noteListAuditor() {

      ModelAndView res;
      Collection<Note> notes;
      notes = auditorService.findByPrincipal().getNotes();
      res = new ModelAndView("note/list");
      res.addObject("notes", notes);
      res.addObject("requestURI", "note/list.do");

      return res;
   }
    //Create Method -----------------------------------------------------------

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView create() {

        ModelAndView result;

        Note note = noteService.create();
       result = createEditModelAndViewAuditor(note);
       result.addObject("trips", tripService.showableTrip());

        return result;

    }


    // Edition ---------------------------------------------------------

//    @RequestMapping(value = "/edit", method = RequestMethod.GET)
//    public ModelAndView edit(@RequestParam int noteId) {
//        ModelAndView result;
//        Note note;
//
//        note = noteService.findOne(noteId);
//        Assert.notNull(note);
//        result = createEditModelAndView(note);
//        result.addObject("note", note);
//
//
//        return result;
//    }

   @RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid Note note, BindingResult binding) {
      ModelAndView result;

      if (binding.hasErrors()) {
         result = createEditModelAndView(note);
      } else {
         try {
            Assert.isTrue(!note.getTrips().isEmpty(), "No est� asociando ning�n viaje / You're not associating it to any trip");
            note.setCreationMomment(new Date(System.currentTimeMillis() - 100));
            note.setOwner(auditorService.findByPrincipal());
            Trip asso = new ArrayList<Trip>(note.getTrips()).get(0);
            note.setManager(asso.getManager());
            noteService.save(note);
            result = new ModelAndView("redirect: listAuditor.do");
         } catch (Throwable oops) {
            result = createEditModelAndView(note, "general.commit.error");
         }
      }
      return result;
   }


   @RequestMapping(value = "/view", method = RequestMethod.GET)
   public ModelAndView view(@RequestParam int propertyId) {
      ModelAndView res;
      Note e = noteService.findOne(propertyId);
      res = new ModelAndView("note/view");
      res.addObject("note", e);

      return res;
   }


   @RequestMapping(value = "/reply", method = RequestMethod.GET)
   public ModelAndView reply(@RequestParam int noteId) {
        ModelAndView result;
        Note note;

      note = noteService.findOne(noteId);
        Assert.notNull(note);
        result = createEditModelAndView(note);
        result.addObject("note", note);
      result.addObject("remark", note.getRemark());


        return result;
    }

   @RequestMapping(value = "/reply", method = RequestMethod.POST, params = "replyy")
   public ModelAndView reply(@Valid Note note, BindingResult binding) {
        ModelAndView result;

      if (binding.hasErrors()) {
            result = createEditModelAndView(note);
        } else {
            try {
               note.setReplyDate(new Date(System.currentTimeMillis() - 100));
                noteService.save(note);
                result = new ModelAndView("redirect: list.do");
            } catch (Throwable oops) {
               result = createEditModelAndView(note, "general.commit.error");
            }
        }
        return result;
    }


   // Ancillary methods ------------------------------------------------

    protected ModelAndView createEditModelAndView(Note note) {
        ModelAndView result;

        result = createEditModelAndView(note, null);

        return result;
    }

   protected ModelAndView createEditModelAndView(Note note, String message) {
      ModelAndView result;

      result = new ModelAndView("note/edit");
      result.addObject("note", note);
      result.addObject("message", message);

      return result;

   }

   protected ModelAndView createEditModelAndViewAuditor(Note note) {
      ModelAndView result;

      result = createEditModelAndViewAuditor(note, null);

      return result;
   }

   protected ModelAndView createEditModelAndViewAuditor(Note note, String message) {
      ModelAndView result;

      result = new ModelAndView("note/create");
      result.addObject("note", note);
      result.addObject("message", message);

      return result;

   }


}
