package controllers;


import domain.EducationRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.EducationRecordService;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/educationRecord")
public class EducationRecordController  extends AbstractController{


    //Services ----------------------------------------------------------------

    @Autowired
    private EducationRecordService educationRecordService;


    //Constructors----------------------------------------------

    public EducationRecordController() {
        super();
    }


    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView educationRecordList() {

        ModelAndView result;
        Collection<EducationRecord> educationRecords;
        educationRecords = educationRecordService.findAll();
        result = new ModelAndView("educationRecord/list");
        result.addObject("educationRecord", educationRecords);
       result.addObject("requestURI", "educationRecord/list.do");

        return result;
    }


    //Create Method -----------------------------------------------------------

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView create() {

        ModelAndView result;

        EducationRecord educationRecord = educationRecordService.create();
        result = createEditModelAndView(educationRecord);

        return result;

    }

    // Edition ---------------------------------------------------------

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public ModelAndView edit(@RequestParam int propertyId) {
        ModelAndView result;
        EducationRecord educationRecord;

        educationRecord = educationRecordService.findOne(propertyId);
        Assert.notNull(educationRecord);
        result = createEditModelAndView(educationRecord);
        result.addObject("educationRecord", educationRecord);


        return result;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
    public ModelAndView save(@Valid EducationRecord educationRecord, BindingResult binding) {
        ModelAndView result;

        if (binding.hasErrors()) {
            result = createEditModelAndView(educationRecord);
        } else {
            try {
                educationRecordService.save(educationRecord);
                result = new ModelAndView("redirect: list.do");
            } catch (Throwable oops) {
                result = createEditModelAndView(educationRecord , "property.commit.error");
            }
        }
        return result;
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public ModelAndView delete(@RequestParam int propertyId) {
        ModelAndView result;
        EducationRecord e = educationRecordService.findOne(propertyId);

        try {
            educationRecordService.delete(e);
            result = new ModelAndView("redirect:list.do");
        } catch (Throwable oops) {
            result = createEditModelAndView(e, "educationRecord.commit.error");
        }


        return result;
    }

    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public ModelAndView view(@RequestParam int propertyId) {
        ModelAndView res;
        EducationRecord e = educationRecordService.findOne(propertyId);
        res = new ModelAndView("educationRecord/view");
        res.addObject("educationRecord", e);

        return res;
    }

    // Ancillary methods ------------------------------------------------

    protected ModelAndView createEditModelAndView(EducationRecord educationRecord) {
        ModelAndView result;

        result = createEditModelAndView(educationRecord, null);

        return result;
    }

    protected ModelAndView createEditModelAndView(EducationRecord educationRecord, String message) {
        ModelAndView result;

        result = new ModelAndView("educationRecord/edit");
        result.addObject("educationRecord", educationRecord);
        result.addObject("message", message);

        return result;

    }


}










