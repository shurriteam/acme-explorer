/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;


import domain.LegalStatus;
import domain.LegalText;
import domain.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.*;

import javax.naming.OperationNotSupportedException;
import javax.validation.Valid;
import java.util.Collection;
import java.util.Date;

@Controller
@RequestMapping("/legaltext")
public class LegalTextController extends AbstractController {

   //Services ----------------------------------------------------------------

   @Autowired
   private LegalTextService legalTextService;
   @Autowired
   private FolderService folderService;
   @Autowired
   private ActorService actorService;
   @Autowired
   private MezzageService mezzageService;
   @Autowired
   private ExplorerService eplorerService;


   //Constructors----------------------------------------------

   public LegalTextController() {
      super();
   }

   protected static ModelAndView createEditModelAndView(LegalText folder) {
      ModelAndView result;

      result = createEditModelAndView(folder, null);

      return result;
   }


   protected static ModelAndView createEditModelAndView(LegalText folder, String message) {
      ModelAndView result;

      result = new ModelAndView("legaltext/edit");
      result.addObject("legaltext", folder);
      result.addObject("message", message);

      return result;

   }


   //Create Method -----------------------------------------------------------


   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView listLegalText() {
      ModelAndView result;
      Collection<LegalText> folders = legalTextService.findAll();
      result = new ModelAndView("legaltext/list");
      result.addObject("legaltexts", folders);
      result.addObject("requestURI", "legaltext/list.do");
      return result;
   }


   @RequestMapping(value = "/view")
   public ModelAndView insideFolder(@RequestParam int legaltextId) {
      ModelAndView result;


      LegalText legalText = legalTextService.findOne(legaltextId);
      result = new ModelAndView("legaltext/view");
      result.addObject("title", legalText.getTitle());
      result.addObject("registrationDate", legalText.getRegistrationDate());
      result.addObject("numberOfAplicableLaws", legalText.getNumberOfAplicableLaws());
      result.addObject("body", legalText.getBody());
      result.addObject("status", legalText.getStatus());
     return result;
  }


   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create() {

      ModelAndView result;
      LegalText folder = legalTextService.create();

      result = createEditModelAndView(folder);
      return result;

   }


   // Ancillary methods ------------------------------------------------


   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int legaltextId) {
      ModelAndView result;
      LegalText legalText;

      try {
         legalText = legalTextService.findOne(legaltextId);
         Assert.notNull(legalText);

         if (legalText.getStatus() == LegalStatus.DRAFT){
            result = createEditModelAndView(legalText);
         }else {
            throw new OperationNotSupportedException("The legal text is in FINAL mode ant it can't be edited.");
         }
      } catch (OperationNotSupportedException e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }

      return result;
   }


   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid LegalText legalText, BindingResult binding) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndView(legalText);
      } else {
         try {
            legalText.setStatus(LegalStatus.DRAFT);
            legalText.setRegistrationDate(new Date(System.currentTimeMillis() - 100));
            legalTextService.save(legalText);
            result = new ModelAndView("redirect:list.do");
         } catch (Throwable oops) {
            result = createEditModelAndView(legalText, "general.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete(@RequestParam int legalTextId) {
      ModelAndView result;
      try {
         LegalText legalText = legalTextService.findOne(legalTextId);

         if (legalText.getStatus().equals(LegalStatus.DRAFT)) {
               legalText.setTrips(null);
               legalTextService.delete(legalText);
               result = new ModelAndView("redirect:list.do");
            }else {
               throw new OperationNotSupportedException("The legal text is in FINAL mode ant it can't be delete.");
            }

      } catch (Throwable oops) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", oops.getMessage());
      }

      return result;
   }

   @RequestMapping(value = "/publish", method = RequestMethod.GET)
   public ModelAndView publish(@RequestParam int legalTextId){
      ModelAndView result;
      try {
         LegalText legalText = legalTextService.findOne(legalTextId);
         legalText.setStatus(LegalStatus.PUBLISHED);
         legalTextService.save(legalText);
         result = new ModelAndView("redirect:list.do");
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }

      return result;

   }


}
