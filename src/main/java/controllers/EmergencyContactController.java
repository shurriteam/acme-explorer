package controllers;


import domain.EmergencyContact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.EmergencyContactService;

import javax.validation.Valid;
import java.util.Collection;



@Controller
@RequestMapping("/emergencyContact")
public class EmergencyContactController  extends AbstractController {

    //Services------------------

    @Autowired
    private EmergencyContactService emergencyContactService;

    //Constructors----------------------------------------------

    public EmergencyContactController() {
        super();
    }

    @RequestMapping(value = "/list",method = RequestMethod.GET)

    public ModelAndView emergencyContactList(){

        ModelAndView result;
        Collection<EmergencyContact> emergencyContacts;
        emergencyContacts = emergencyContactService.findAll();
        result = new ModelAndView("emergencyContactlist");
        result.addObject("emerencyContact", emergencyContacts);
       result.addObject("requestURI", "emergencyContact/list.do");

        return result;
    }


    //Create Method -----------------------------------------------------------

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView create() {

        ModelAndView result;

        EmergencyContact emergencyContact = emergencyContactService.create();
        result = createEditModelAndView(emergencyContact);

        return result;

    }

    // Edition ---------------------------------------------------------

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public ModelAndView edit(@RequestParam int propertyId) {
        ModelAndView result;
        EmergencyContact emergencyContact;

        emergencyContact = emergencyContactService.findOne(propertyId);
        Assert.notNull(emergencyContact);
        result = createEditModelAndView(emergencyContact);
        result.addObject("emergencyContact", emergencyContact);


        return result;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
    public ModelAndView save(@Valid EmergencyContact emergencyContact, BindingResult binding) {
        ModelAndView result;

        if (binding.hasErrors()) {
            result = createEditModelAndView(emergencyContact);
        } else {
            try {
                emergencyContactService.save( emergencyContact);
                result = new ModelAndView("redirect: list.do");
            } catch (Throwable oops) {
                result = createEditModelAndView(emergencyContact , "property.commit.error");
            }
        }
        return result;
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public ModelAndView delete(@RequestParam int propertyId) {
        ModelAndView result;
        EmergencyContact e = emergencyContactService.findOne(propertyId);

        try {
            emergencyContactService.delete(e);
            result = new ModelAndView("redirect:list.do");
        } catch (Throwable oops) {
            result = createEditModelAndView(e, "creditCard.commit.error");
        }


        return result;
    }

    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public ModelAndView view(@RequestParam int propertyId) {
        ModelAndView res;
        EmergencyContact e = emergencyContactService.findOne(propertyId);
        res = new ModelAndView("emergencyContact/view");
        res.addObject("emergencyContact", e);

        return res;
    }

    // Ancillary methods ------------------------------------------------

    protected ModelAndView createEditModelAndView(EmergencyContact emergencyContact) {
        ModelAndView result;

        result = createEditModelAndView(emergencyContact, null);

        return result;
    }

    protected ModelAndView createEditModelAndView(EmergencyContact emergencyContact, String message) {
        ModelAndView result;

        result = new ModelAndView("emergencyContact/edit");
        result.addObject("emergencyContact", emergencyContact);
        result.addObject("message", message);

        return result;

    }

}






















