/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;


import domain.AdministratorUtils;
import domain.Folder;
import domain.Mezzage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Controller
@RequestMapping("/folder")
public class FolderController extends AbstractController {

   //Services ----------------------------------------------------------------

   @Autowired
   private FolderService folderService;
   @Autowired
   private ActorService actorService;
   @Autowired
   private MezzageService mezzageService;
   @Autowired
   private ExplorerService eplorerService;
   @Autowired
   private Utilservice utilservice;

   //Constructors----------------------------------------------

   public FolderController() {
      super();
   }

   protected static ModelAndView createEditModelAndView(Folder folder) {
      ModelAndView result;

      result = createEditModelAndView(folder, null);

      return result;
   }


   protected static ModelAndView createEditModelAndView(Folder folder, String message) {
      ModelAndView result;

      result = new ModelAndView("folder/edit");
      result.addObject("folder", folder);
      result.addObject("message", message);

      return result;

   }


   //Create Method -----------------------------------------------------------


   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView listFolder() {
      ModelAndView result;
      Collection<Folder> folders = actorService.findByPrincipal().getFolders();
      result = new ModelAndView("folder/list");
      result.addObject("folders", folders);
      result.addObject("requestURI", "folder/list.do");
      return result;
   }


   @RequestMapping(value = "/view")
   public ModelAndView insideFolder(@RequestParam int folderId) {
      ModelAndView result;


      Folder folder = folderService.findOne(folderId);
      Boolean isTrashBox = folder.getName().equals("trashbox");
      Boolean isInbox = folder.getName().equals("inbox");

      if(isInbox){
         List<AdministratorUtils> administratorUtilsl = new ArrayList<AdministratorUtils>(utilservice.findAll());
         AdministratorUtils administratorUtils = administratorUtilsl.get(0);
         folder.getMezzages().addAll(administratorUtils.getBroadcastedMezzanges());


//         for (Mezzage m : administratorUtils.getBroadcastedMezzanges()){
//            Mezzage my = mezzageService.create();
//            my.setSubject(m.getSubject());
//            my.setMoment(m.getMoment());
//            my.setSenderEmail(m.getSenderEmail());
//            my.setBody(m.getBody());
//            my.setPriority(m.getPriority());
//            my.setReceiverEmail(actorService.findByPrincipal().getEmail());
//            my.setSender(m.getSender());
//            my.setRecipient(actorService.findByPrincipal());
//            my.setFolder(folder);
//            mezzageService.save(my);
//            folder.getMezzages().add(my);
//         }
      }


      Collection<Mezzage> mezzages = folder.getMezzages();


      result = new ModelAndView("mezzage/list");
      result.addObject("mezzages", mezzages);
      result.addObject("isTrashBox", isTrashBox);
     return result;
  }


   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create() {

      ModelAndView result;
      Folder folder = folderService.create();
      result = createEditModelAndView(folder);
      return result;

   }


   // Ancillary methods ------------------------------------------------


   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int folderId) {
      ModelAndView result;
      Folder folder;

      folder = folderService.findOne(folderId);
      Assert.notNull(folder);
      result = createEditModelAndView(folder);

      return result;
   }


   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid Folder folder, BindingResult binding) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndView(folder);
      } else {
         try {
            folder.setOwner(actorService.findByPrincipal());
            folderService.save(folder);
            result = new ModelAndView("redirect:list.do");
         } catch (Throwable oops) {
            result = createEditModelAndView(folder, "general.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete(@RequestParam int folderId) {
      ModelAndView result;
      Folder folder = folderService.findOne(folderId);
      try {

         Assert.isTrue((!folder.getName().contains("inbox") && !folder.getName().contains("outbox") && !folder.getName().contains("spambox") &&
                 !folder.getName().contains("notificationbox") && !folder.getName().contains("trashbox")), "A system folder can't be erased / Una carpeta del sistema no puede ser eliminada");
         folder.setOwner(null);
         folderService.delete(folder);
         result = new ModelAndView("welcome/index");
      } catch (Throwable oops) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", oops.getMessage());

      }

      return result;
   }


}
