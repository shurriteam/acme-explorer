package controllers;

import domain.Tag;
import domain.Trip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.TagService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;

@Controller
@RequestMapping("/tag")


public class TagController extends AbstractController {
    @Autowired
    private TagService tagService;


    //Constructors----------------------------------------------

    public TagController() {
        super();
    }

//Create Method -----------------------------------------------------------


    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView listTag() {
        ModelAndView result;
        Collection<Tag> tags = tagService.findAll();
        result = new ModelAndView("tag/list");
        result.addObject("tags", tags);
        result.addObject("requestURI", "tag/list.do");
        return result;
    }

    @RequestMapping(value = "/view")
    public ModelAndView insideFolder(@RequestParam int tagId) {
        ModelAndView result;

        Tag tag = tagService.findOne(tagId);

        result = new ModelAndView("tag/list");

        return result;
    }


    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView create() {

        ModelAndView result;
        Tag tag = tagService.create();
        result = createEditModelAndView(tag);
        return result;

    }


    // Ancillary methods ------------------------------------------------


    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public ModelAndView edit(@RequestParam int tagId) {
        ModelAndView result;
        Tag tag;

        tag = tagService.findOne(tagId);
        Assert.notNull(tag);
        result = createEditModelAndView(tag);

        return result;
    }


    @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
    public ModelAndView save(@Valid Tag tag, BindingResult binding) {
        ModelAndView result;
        if (binding.hasErrors()) {
            result = createEditModelAndView(tag);
        } else {
            try {

                tagService.save(tag);
                result = new ModelAndView("redirect:list.do");
            } catch (Throwable oops) {
                result = createEditModelAndView(tag, "general.commit.error");
            }
        }
        return result;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
    public ModelAndView delete(@Valid Tag tag) {
        ModelAndView result;
        try {

            tagService.delete(tag);
            result = new ModelAndView("redirect:list.do");
        } catch (Throwable oops) {

            result = createEditModelAndView(tag, "general.commit.error");
        }

        return result;
    }




    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public ModelAndView delete(@RequestParam int tagId) {
        ModelAndView result;
        Tag t = tagService.findOne(tagId);

        try {
            tagService.delete(t);
            result = new ModelAndView("redirect:list.do");
        } catch (Throwable oops) {
            result = createEditModelAndView(t, "tag.commit.error");
        }
        return result;
    }

    // Ancillary methods ------------------------------------------------

    protected ModelAndView createEditModelAndView(Tag tag) {
        ModelAndView result;

        result = createEditModelAndView(tag, null);

        return result;
    }

    protected ModelAndView createEditModelAndView(Tag tag, String message) {
        ModelAndView result;

        result = new ModelAndView("tag/edit");
        result.addObject("tag", tag);
        result.addObject("message", message);

        return result;

    }


}




