/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;

import domain.Curricula;
import domain.Ranger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.CurriculaService;
import services.RangerService;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/ranger")
public class RangerController extends AbstractController {


   //Services ----------------------------------------------------------------

   @Autowired
   private RangerService rangerService;
   @Autowired
   private CurriculaService curriculaService;


   //Constructors----------------------------------------------

   public RangerController() {
      super();
   }


   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView rangerList() {

      ModelAndView result;
      Collection<Ranger> ranger;
      ranger = rangerService.findAll();
      result = new ModelAndView("ranger/list");
      result.addObject("ranger", ranger);
      result.addObject("requestURI", "ranger/list.do");

      return result;
   }


   //Create Method -----------------------------------------------------------

   protected static ModelAndView createEditModelAndView2(Ranger user) {
      ModelAndView result;

      result = createEditModelAndView2(user, null);

      return result;
   }

   protected static ModelAndView createEditModelAndView2(Ranger user, String message) {
      ModelAndView result;

      result = new ModelAndView("ranger/register");
      result.addObject("ranger", user);
      result.addObject("message", message);

      return result;

   }

   // Edition ---------------------------------------------------------

   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int rangerId) {
      ModelAndView result;
      Ranger ranger;

      ranger = rangerService.findOne(rangerId);
      Assert.notNull(ranger);
      result = createEditModelAndView(ranger);
      result.addObject("ranger", ranger);


      return result;
   }

   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid Ranger ranger, BindingResult binding) {
      ModelAndView result;

      if (binding.hasErrors()) {
         result = createEditModelAndView(ranger);
      } else {
         try {

            rangerService.save(ranger);
            result = new ModelAndView("redirect:list.do");
         } catch (Throwable oops) {
            result = createEditModelAndView(ranger, "property.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete(@RequestParam int rangerId) {
      ModelAndView result;
      Ranger p = rangerService.findOne(rangerId);

      try {
         rangerService.delete(p);
         result = new ModelAndView("redirect:list.do");
      } catch (Throwable oops) {
         result = createEditModelAndView(p, "ranger.commit.error");
      }


      return result;
   }

   @RequestMapping(value = "/view", method = RequestMethod.GET)
   public ModelAndView view(@RequestParam int rangerId) {
      ModelAndView res;
      Ranger p = rangerService.findOne(rangerId);

      res = new ModelAndView("ranger/view");
      res.addObject("name", p.getName());
      res.addObject("surname", p.getSurname());
      res.addObject("email", p.getEmail());
      res.addObject("phoneNumber", p.getPhoneNumber());
      res.addObject("trips", p.getTripsToGuide());
      if (p.getCurricula() != null) {
         res.addObject("persPhoto", p.getCurricula().getPersonalRecords().getPhoto());
         res.addObject("persName", p.getCurricula().getPersonalRecords().getPhoto());
         res.addObject("persEmail", p.getCurricula().getPersonalRecords().getPhoto());
         res.addObject("persPhone", p.getCurricula().getPersonalRecords().getPhoto());
         res.addObject("persLinked", p.getCurricula().getPersonalRecords().getPhoto());

         Curricula curricula = p.getCurricula();

         res.addObject("PRO", curricula.getProfessionalRecords());
         res.addObject("EDU", curricula.getEducationRecords());
         res.addObject("END", curricula.getEndorseRecords());
         res.addObject("MIS", curricula.getMiscellaRecords());
      }

      res.addObject("ranger", p);
      res.addObject("requestURI", "ranger/view.do");



       res.addObject("requestURI", "trip/view.do");

      return res;
   }

   // Ancillary methods ------------------------------------------------

   protected ModelAndView createEditModelAndView(Ranger ranger) {
      ModelAndView result;

      result = createEditModelAndView(ranger, null);

      return result;
   }


   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create2() {
      ModelAndView result;
      Ranger user = rangerService.create();
      result = createEditModelAndView2(user);
      return result;
   }

   @RequestMapping(value = "/register", method = RequestMethod.POST, params = "save")
   public ModelAndView register(@Valid Ranger user, BindingResult binding) {
      ModelAndView result;

      if (!binding.hasErrors()) {
         result = createEditModelAndView2(user);
      } else {
         try {
            rangerService.registerAsRanger(user);
            result = new ModelAndView("welcome/index");
         } catch (Throwable oops) {
            result = createEditModelAndView2(user, "general.commit.error");
         }
      }
      return result;
   }
   // Edition ---------------------------------------------------------

   protected ModelAndView createEditModelAndView(Ranger ranger, String message) {
      ModelAndView result;

      result = new ModelAndView("ranger/edit");
      result.addObject("ranger", ranger);
      result.addObject("message", message);

      return result;

   }


}








