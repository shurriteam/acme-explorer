package domain;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
@Access(AccessType.PROPERTY)

public class Location extends  DomainEntity{
    private Double longitude;
    private Double latitude;
    private String longitudeDir;
    private String latitudeDir;

    private SurvivalClass survivalClass;




    @NotNull
    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @NotNull
    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }


    @Pattern(regexp = "[a-zA-Z]{1}")
    public String getLongitudeDir() {
        return longitudeDir;
    }

    public void setLongitudeDir(String longitudeDir) {
        this.longitudeDir = longitudeDir;
    }

    @Pattern(regexp = "[a-zA-Z]{1}")
    public String getLatitudeDir() {
        return latitudeDir;
    }

    public void setLatitudeDir(String latitudeDir) {
        this.latitudeDir = latitudeDir;
    }

    @OneToOne(cascade = CascadeType.ALL)
    public SurvivalClass getSurvivalClass() {
        return survivalClass;
    }

    public void setSurvivalClass(SurvivalClass survivalClass) {
        this.survivalClass = survivalClass;
    }


    @Override
    public String toString() {
       return "long: " + longitude + " (" + longitudeDir + ")" + " lat: " + latitude + " (" + latitudeDir + ")";
    }
}
