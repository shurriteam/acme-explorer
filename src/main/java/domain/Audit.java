package domain;


import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Date;

@Entity
@Access(AccessType.PROPERTY)
public class Audit extends DomainEntity{

   private Date momment;
   private String title, description;
   private Collection<Attachment> attachment;
   private Trip trip;
   private Auditor auditor;
   private AuditStatus auditStatus;


   @Temporal(TemporalType.TIMESTAMP)
   @DateTimeFormat(pattern = "yyyy/mm/dd")
   public Date getMomment() {
      return momment;
   }

   public void setMomment(Date momment) {
      this.momment = momment;
   }

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getTitle() {
      return title;
   }

   public void setTitle(String title) {
      this.title = title;
   }

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getDescription() {
      return description;
   }

   public void setDescription(String description) {
      this.description = description;
   }

   @OneToMany(cascade = CascadeType.PERSIST)
   public Collection<Attachment> getAttachment() {
      return attachment;
   }

   public void setAttachment(Collection<Attachment> attachment) {
      this.attachment = attachment;
   }

   @ManyToOne(cascade = CascadeType.PERSIST)
   public Trip getTrip() {
      return trip;
   }

   public void setTrip(Trip trip) {
      this.trip = trip;
   }

   @ManyToOne(cascade = CascadeType.PERSIST)
   public Auditor getAuditor() {
      return auditor;
   }

   public void setAuditor(Auditor auditor) {
      this.auditor = auditor;
   }

   @NotNull
   public AuditStatus getAuditStatus() {
      return auditStatus;
   }

   public void setAuditStatus(AuditStatus auditStatus) {
      this.auditStatus = auditStatus;
   }
}
