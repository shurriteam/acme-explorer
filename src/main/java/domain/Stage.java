package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
public class Stage extends DomainEntity {
   private String title;
   private String description;
   private Double price;
   private Trip trip;
   private int numberOfStage;

   @NotNull
   public int getNumberOfStage() {
      return numberOfStage;
   }

   public void setNumberOfStage(int numberOfStage) {
      this.numberOfStage = numberOfStage;
   }

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getTitle() {
      return title;
   }

   public void setTitle(String title) {
      this.title = title;
   }

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getDescription() {
      return description;
   }

   public void setDescription(String description) {
      this.description = description;
   }

   @NotNull
   public Double getPrice() {
      return price;
   }

   public void setPrice(Double price) {
      this.price = price;
   }

   @ManyToOne(cascade = CascadeType.PERSIST)
   public Trip getTrip() {
      return trip;
   }

   public void setTrip(Trip trip) {
      this.trip = trip;
   }
}
