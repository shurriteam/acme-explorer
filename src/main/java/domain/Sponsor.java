package domain;


import javax.persistence.*;
import java.util.Collection;

@Entity
@Access(AccessType.PROPERTY)
    public class Sponsor  extends Actor{

    private CreditCard creditCard;
    private Collection<SurvivalClass> survivalClassCollection;
    private Collection<Sponsorship>sponsorships;


    @OneToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    @OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, mappedBy = "sponsors")
    public Collection<Sponsorship> getSponsor() {
        return sponsorships;
    }

    public void setSponsor(Collection<Sponsorship> sponsor) {
        this.sponsorships = sponsor;
    }

    @OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, mappedBy = "sponsors")
    public Collection<SurvivalClass> getSurvivalClassCollection() {
        return survivalClassCollection;
    }

    public void setSurvivalClassCollection(Collection<SurvivalClass> survivalClassCollection) {
        this.survivalClassCollection = survivalClassCollection;
    }


}
