package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;


import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Access(AccessType.PROPERTY)
public class EndorseRecord extends DomainEntity {
    private String name;
    private String email;
    private String link;
    private String comments;
    private Curricula curricula;

    @NotBlank
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    @NotBlank
    @Email
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    @URL
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
    @ManyToOne(cascade = CascadeType.PERSIST)
    public Curricula getCurricula() {
        return curricula;
    }

    public void setCurricula(Curricula curricula) {
        this.curricula = curricula;
    }

    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
}
