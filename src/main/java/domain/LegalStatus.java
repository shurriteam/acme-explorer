package domain;

public enum LegalStatus {
   PUBLISHED, DRAFT
}
