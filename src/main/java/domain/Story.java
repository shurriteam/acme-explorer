package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Access(AccessType.PROPERTY)

public class Story extends DomainEntity {
   private String title, text;
   private Collection<Attachment> attachments;

   private Trip associatedTrip;
   private Explorer explorer;

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getTitle() {
      return title;
   }

   public void setTitle(String title) {
      this.title = title;
   }

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getText() {
      return text;
   }

   public void setText(String text) {
      this.text = text;
   }

   @OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
   public Collection<Attachment> getAttachments() {
      return attachments;
   }

   public void setAttachments(Collection<Attachment> attachments) {
      this.attachments = attachments;
   }

   @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
   public Trip getAssociatedTrip() {
      return associatedTrip;
   }

   public void setAssociatedTrip(Trip associatedTrip) {
      this.associatedTrip = associatedTrip;
   }

   @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
   public Explorer getExplorer() {
      return explorer;
   }

   public void setExplorer(Explorer explorer) {
      this.explorer = explorer;
   }
}
