package domain;


import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.URL;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;

@Entity
@Access(AccessType.PROPERTY)

public class Attachment extends DomainEntity {
   private String name, url;
   private int friendId;

   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getName() {
      return name;
   }
   public void setName(String name) {
      this.name = name;
   }

   @URL
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getUrl() {
      return url;
   }

   public void setUrl(String url) {
      this.url = url;
   }

   public int getFriendId() {
      return friendId;
   }

   public void setFriendId(int friendId) {
      this.friendId = friendId;
   }

   @Override
   public String toString() {
      return name + ": " + url;
   }
}
