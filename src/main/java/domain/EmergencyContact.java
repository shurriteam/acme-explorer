package domain;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import javax.validation.constraints.Pattern;

@Entity
@Access(AccessType.PROPERTY)
public class EmergencyContact extends DomainEntity {
   private String name;
   private String phoneNumber;
   private String email;
   private Explorer owner;


   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   //@Pattern(regexp = "(\\+\\d{1,3})?(\\(\\d{3}\\))?([0-9a-zA-z][ -]?){4,}")
   public String getPhoneNumber() {
      return phoneNumber;
   }

   public void setPhoneNumber(String phoneNumber) {
      this.phoneNumber = phoneNumber;
   }


   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   @Email
   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }

    @ManyToOne(cascade = CascadeType.ALL)
   public Explorer getOwner() {
      return owner;
   }

   public void setOwner(Explorer owner) {
      this.owner = owner;
   }
}
