package domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;


    @Entity
    @Access(AccessType.PROPERTY)
    public class Curricula  extends DomainEntity {
        private String ticker;
        private PersonalRecord personalRecords;
        private Collection<EducationRecord> educationRecords;
        private Collection<ProfessionalRecord> professionalRecords;
        private Collection<EndorseRecord> endorseRecords;
        private Collection<MiscellaRecord> miscellaRecords;



        @GeneratedValue
        public String getTicker() {
            return ticker;
        }
        public void setTicker(String ticker) {
            this.ticker = ticker;

        }


        @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, mappedBy = "curricula")
        public Collection<MiscellaRecord> getMiscellaRecords() {
            return miscellaRecords;
        }

        public void setMiscellaRecords(Collection<MiscellaRecord> miscellaRecords) {
            this.miscellaRecords = miscellaRecords;
        }

        @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, mappedBy = "curricula")
        public Collection<ProfessionalRecord> getProfessionalRecords() {
            return professionalRecords;
        }
        public void setProfessionalRecords(Collection<ProfessionalRecord> professionalRecords) {
            this.professionalRecords = professionalRecords;
        }


        @NotNull
        @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, targetEntity = PersonalRecord.class, mappedBy = "curricula")
        public PersonalRecord getPersonalRecords() {
            return personalRecords;
        }

        public void setPersonalRecords(PersonalRecord personalRecords) {
            this.personalRecords = personalRecords;
        }

        @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, mappedBy = "curricula")
        public Collection<EducationRecord> getEducationRecords() {
            return educationRecords;
        }
        public void setEducationRecords(Collection<EducationRecord> educationRecords) {
            this.educationRecords = educationRecords;
        }

        @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, mappedBy = "curricula")
        public Collection<EndorseRecord> getEndorseRecords() {
            return endorseRecords;
        }
        public void setEndorseRecords(Collection<EndorseRecord> endorseRecords) {
            this.endorseRecords = endorseRecords;
        }






    }


