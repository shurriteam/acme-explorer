/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package domain;

import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Collection;

@Entity
@Access(AccessType.PROPERTY)
public class Category extends DomainEntity {

   private String name;
   private Category father;
   private Collection<Category> sons;
   private Collection<Trip> relatedTrips;


   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   @OneToOne(cascade = CascadeType.PERSIST)
   public Category getFather() {
      return father;
   }

   public void setFather(Category father) {
      this.father = father;
   }

   @OneToMany(cascade = CascadeType.PERSIST)
   public Collection<Category> getSons() {
      return sons;
   }

   public void setSons(Collection<Category> sons) {
      this.sons = sons;
   }

   @Size(min = 0, max = 5)
   @OneToMany(cascade = CascadeType.PERSIST)
   public Collection<Trip> getRelatedTrips() {
      return relatedTrips;
   }

   public void setRelatedTrips(Collection<Trip> relatedTrips) {
      this.relatedTrips = relatedTrips;
   }


   @Override
   public String toString() {
      return name;
   }


}
