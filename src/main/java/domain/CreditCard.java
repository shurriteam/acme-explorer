/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package domain;

import org.hibernate.validator.constraints.CreditCardNumber;
import org.hibernate.validator.constraints.Range;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.validation.constraints.Pattern;


@Entity
@Access(AccessType.PROPERTY)
public class CreditCard extends DomainEntity {

   private String holderName;
   private String number;
   private Integer year;
   private Integer month;
   private String CVV;
   private CreditCardType type;
   private boolean valid;


   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getHolderName() {
      return holderName;
   }

   public void setHolderName(String holderName) {
      this.holderName = holderName;
   }


   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   @CreditCardNumber
   public String getNumber() {
      return number;
   }

   public void setNumber(String number) {
      this.number = number;
   }


   //@Pattern(regexp = "^\\d{4}$")
   @Range(min = 1900, max = 2100)
   public Integer getYear() {
      return year;
   }

   public void setYear(Integer year) {
      this.year = year;
   }


   // @Pattern(regexp = "^(0?[1-9]|1[012])$")

   @Range(min = 01, max = 12)
   public Integer getMonth() {
      return month;
   }

   public void setMonth(Integer month) {
      this.month = month;
   }

   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   @Pattern(regexp = "\\d{3}$")
   public String getCVV() {
      return CVV;
   }

   public void setCVV(String CVV) {
      this.CVV = CVV;
   }


   public CreditCardType getType() {
      return type;
   }

   public void setType(CreditCardType type) {
      this.type = type;
   }


   public boolean isValid() {
      return valid;
   }

   public void setValid(boolean valid) {
      this.valid = valid;
   }

   @Override
   public String toString() {
      return "CreditCard{" +
              "holderName='" + holderName + '\'' +
              ", number='" + number + '\'' +
              ", year=" + year +
              ", month=" + month +
              '}';
   }
}
