package domain;

public enum AuditStatus {

    DRAFT, PUBLISHED
}
