package domain;


import javax.persistence.*;
import java.util.Collection;

@Entity
@Access(AccessType.PROPERTY)
public class Explorer extends Actor {

   private CreditCard creditCard;
   private Collection<TripApplication> tripApplications;
   private Collection<EmergencyContact> emergencyContacts;
   private Collection<Search> searches;
   private Collection<Story> Stories;
   private Collection<Comment> comments;
   private Collection<SurvivalClass> survivalClasses;


   @OneToOne(cascade = CascadeType.PERSIST)
   public CreditCard getCreditCard() {
      return creditCard;
   }

   public void setCreditCard(CreditCard creditCard) {
      this.creditCard = creditCard;
   }

    @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "owner")
   public Collection<TripApplication> getTripApplications() {
      return tripApplications;
   }

   public void setTripApplications(Collection<TripApplication> tripApplications) {
      this.tripApplications = tripApplications;
   }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "owner")
   public Collection<EmergencyContact> getEmergencyContacts() {
      return emergencyContacts;
   }

   public void setEmergencyContacts(Collection<EmergencyContact> emergencyContacts) {
      this.emergencyContacts = emergencyContacts;
   }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "searcher")
   public Collection<Search> getSearches() {
      return searches;
   }

   public void setSearches(Collection<Search> searches) {
      this.searches = searches;
   }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "explorer")
   public Collection<Story> getStories() {
      return Stories;
   }

   public void setStories(Collection<Story> stories) {
      Stories = stories;
   }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "owner")
   public Collection<Comment> getComments() {
      return comments;
   }

   public void setComments(Collection<Comment> comments) {
      this.comments = comments;
   }

   @ManyToMany(cascade = CascadeType.PERSIST, mappedBy = "explorers")
   public Collection<SurvivalClass> getSurvivalClasses() {
      return survivalClasses;
   }

   public void setSurvivalClasses(Collection<SurvivalClass> survivalClasses) {
      this.survivalClasses = survivalClasses;
   }
}

