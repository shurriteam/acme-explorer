/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Access(AccessType.PROPERTY)
public class Mezzage extends DomainEntity {


   private String senderEmail;
   private String receiverEmail;
   private Actor sender;
   private Actor recipient;
   private Date moment;
   private String subject;
   private String body;
   private Priority priority;
   private Folder folder;
   private boolean isBroadcast;


   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getSenderEmail() {
      return senderEmail;
   }

   public void setSenderEmail(String senderEmail) {
      this.senderEmail = senderEmail;
   }

   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getReceiverEmail() {
      return receiverEmail;
   }

   public void setReceiverEmail(String receiverEmail) {
      this.receiverEmail = receiverEmail;
   }

   @ManyToOne(targetEntity = Actor.class, cascade = CascadeType.ALL)
   public Actor getSender() {
      return sender;
   }

   public void setSender(Actor sender) {
      this.sender = sender;
   }

   @ManyToOne(targetEntity = Actor.class, cascade = CascadeType.ALL)
   public Actor getRecipient() {
      return recipient;
   }

   public void setRecipient(Actor receiver) {
      this.recipient = receiver;
   }


   @Temporal(TemporalType.TIMESTAMP)
   @DateTimeFormat(pattern = "yyyy/mm/dd HH:mm:ss")
   public Date getMoment() {
      return moment;
   }

   public void setMoment(Date sendDate) {
      this.moment = sendDate;
   }

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getSubject() {
      return subject;
   }

   public void setSubject(String subject) {
      this.subject = subject;
   }

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getBody() {
      return body;
   }

   public void setBody(String body) {
      this.body = body;
   }

   public Priority getPriority() {
      return priority;
   }

   public void setPriority(Priority priority) {
      this.priority = priority;
   }

   @ManyToOne(optional = false, cascade = CascadeType.ALL)
   public Folder getFolder() {
      return folder;
   }

   public void setFolder(Folder folder) {
      this.folder = folder;
   }


   public boolean isBroadcast() {
      return isBroadcast;
   }

   public void setBroadcast(boolean broadcast) {
      isBroadcast = broadcast;
   }

   @Override
   public String toString() {
      return "Message{" +
              "senderEmail='" + senderEmail + '\'' +
              ", receiverEmail='" + receiverEmail + '\'' +
              ", sendDate=" + moment +
              ", subject='" + subject + '\'' +
              ", body='" + body + '\'' +
              ", priority=" + priority +
              '}';
   }
}
