package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
@Access(AccessType.PROPERTY)
public class LegalText extends DomainEntity {

   private String title;
   private String body;
   private int numberOfAplicableLaws;
   private Date registrationDate;
   private Collection<Trip> trips;
   private LegalStatus status;


   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getTitle() {
      return title;
   }

   public void setTitle(String title) {
      this.title = title;
   }

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getBody() {
      return body;
   }

   public void setBody(String body) {
      this.body = body;
   }

   @Range(min = 1, max = 10)
   public int getNumberOfAplicableLaws() {
      return numberOfAplicableLaws;
   }

   public void setNumberOfAplicableLaws(int numberOfAplicableLaws) {
      this.numberOfAplicableLaws = numberOfAplicableLaws;
   }

   public Date getRegistrationDate() {
      return registrationDate;
   }

   public void setRegistrationDate(Date registrationDate) {
      this.registrationDate = registrationDate;
   }

   @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
   public Collection<Trip> getTrips() {
      return trips;
   }

   public void setTrips(Collection<Trip> trips) {
      this.trips = trips;
   }


   public LegalStatus getStatus() {
      return status;
   }

   public void setStatus(LegalStatus status) {
      this.status = status;
   }

   @Override
   public String toString() {
      return "LegalText{" +
              "title='" + title + '\'' +
              ", numberOfAplicableLaws=" + numberOfAplicableLaws +
              '}';
   }
}