package domain;


import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Collection;
import java.util.Date;

@Entity
@Access(AccessType.PROPERTY)
//@Indexed
public class Trip extends DomainEntity {

   private String ticker;
   private Ranger guide;
   private String title;
   private String description;
   private Double price;
   private Date publicationDate, startDate, endDate;
   private boolean isCancelled;
   private String cancelReason;
   private String image;


   private Collection<Requirement> requirements;
   private Collection<Stage> stages;
   private Collection<LegalText> legalTexts;
   private Collection<Story> stories;
   private Collection<TripApplication> tripApplications;
   private Manager manager;
   private Category category;
   private Collection<Tag> tags;
   private Collection<Sponsorship> sponsorships;
   private Collection<Audit> audits;
   private Collection<Note> notes;
   private Collection<Search> searches;
   private Collection<SurvivalClass> survivalClasses;



   @GeneratedValue()
   @Pattern(regexp = "^[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])-([^a-zA-Z]*[A-Za-z]){3}[\\s\\S]")
   // @Field(index = Index.YES, analyze= Analyze.YES, store= Store.NO)
   public String getTicker() {
      return ticker;
   }

   public void setTicker(String ticker) {
      this.ticker = ticker;
   }



   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   // @Field(index = Index.YES, analyze= Analyze.YES, store= Store.NO)

   public String getTitle() {
      return title;
   }

   public void setTitle(String title) {
      this.title = title;
   }

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   // @Field(index = Index.YES, analyze= Analyze.YES, store= Store.NO)
   public String getDescription() {
      return description;
   }

   public void setDescription(String description) {
      this.description = description;
   }

   public Double getPrice() {
      Double price = 0.0;
      if (this.stages != null) {
         for (Stage tp : this.stages) {
            price = price + tp.getPrice();
         }
      } else {
         price = 0.0;
      }
      return price;

   }

   public void setPrice(Double price) {
      this.price = price;
   }

   @NotNull
   @Temporal(TemporalType.DATE)
   public Date getPublicationDate() {
      return publicationDate;
   }

   public void setPublicationDate(Date publicationDate) {
      this.publicationDate = publicationDate;
   }

   @NotNull
   @Temporal(TemporalType.DATE)
   @Future
   public Date getStartDate() {
      return startDate;
   }

   public void setStartDate(Date startDate) {
      this.startDate = startDate;
   }

   @NotNull
   @Temporal(TemporalType.DATE)
   @Future
   public Date getEndDate() {
      return endDate;
   }

   public void setEndDate(Date endDate) {
      this.endDate = endDate;
   }

   public boolean isCancelled() {
      return isCancelled;
   }

   public void setCancelled(boolean cancelled) {
      isCancelled = cancelled;
   }

   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getCancelReason() {
      return cancelReason;
   }

   public void setCancelReason(String cancelReason) {
      this.cancelReason = cancelReason;
   }

   @URL
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getImage() {
      return image;
   }

   public void setImage(String image) {
      this.image = image;
   }

   @OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, mappedBy = "associatedTrip")
   public Collection<Story> getStories() {
      return stories;
   }

   public void setStories(Collection<Story> stories) {
      this.stories = stories;
   }


   @OrderBy("numberOfStage desc")
   @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "trip")
   public Collection<Stage> getStages() {
      return stages;
   }

   public void setStages(Collection<Stage> stages) {
      this.stages = stages;
   }

   @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
   public Manager getManager() {
      return manager;
   }

   public void setManager(Manager manager) {
      this.manager = manager;
   }

   @ManyToOne(cascade = CascadeType.PERSIST)
   public Category getCategory() {
      return category;
   }

   public void setCategory(Category category) {
      this.category = category;
   }

   @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY, mappedBy = "trips")
   public Collection<LegalText> getLegalTexts() {
      return legalTexts;
   }

   public void setLegalTexts(Collection<LegalText> legalTexts) {
      this.legalTexts = legalTexts;
   }

   @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "trip")
   public Collection<TripApplication> getTripApplications() {
      return tripApplications;
   }


   @OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, mappedBy = "associatedTrip")
   public Collection<Requirement> getRequirements() {
      return requirements;
   }

   public void setRequirements(Collection<Requirement> requirements) {
      this.requirements = requirements;
   }

   public void setTripApplications(Collection<TripApplication> tripApplications) {
      this.tripApplications = tripApplications;
   }

   @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
   public Ranger getGuide() {
      return guide;
   }

   public void setGuide(Ranger guide) {
      this.guide = guide;
   }

   @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, mappedBy = "trip")
   public Collection<Tag> getTags() {
      return tags;
   }

   public void setTags(Collection<Tag> tags) {
      this.tags = tags;
   }


   @OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, mappedBy = "trips")
   public Collection<Sponsorship> getSponsorships() {
      return sponsorships;
   }

   public void setSponsorships(Collection<Sponsorship> sponsorships) {
      this.sponsorships = sponsorships;
   }

   @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "trip")
   public Collection<Audit> getAudits() {
      return audits;
   }

   public void setAudits(Collection<Audit> audits) {
      this.audits = audits;
   }

   @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
   public Collection<Note> getNotes() {
      return notes;
   }

   public void setNotes(Collection<Note> notes) {
      this.notes = notes;
   }

   @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
   public Collection<Search> getSearches() {
      return searches;
   }

   public void setSearches(Collection<Search> searches) {
      this.searches = searches;
   }

   @OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, mappedBy = "trip")
   public Collection<SurvivalClass> getSurvivalClasses() {
      return survivalClasses;
   }

   public void setSurvivalClasses(Collection<SurvivalClass> survivalClasses) {
      this.survivalClasses = survivalClasses;
   }

   @Override
   public String toString() {
      return ticker + " - " + title + " (" + description + ") -" + " [" + category + "] " + "[" + startDate + "/" + endDate + "] " + price;
   }
}
