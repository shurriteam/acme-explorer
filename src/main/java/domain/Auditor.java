package domain;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Access(AccessType.PROPERTY)
public class Auditor extends Actor{

   private Collection<Audit> audits;
   private Collection<Note> notes;


   @OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, mappedBy = "auditor")
   public Collection<Audit> getAudits() {

      return audits;
   }

   public void setAudits(Collection<Audit> audits) {
      this.audits = audits;
   }

   @OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, mappedBy = "owner")
   public Collection<Note> getNotes() {
      return notes;
   }

   public void setNotes(Collection<Note> notes) {
      this.notes = notes;
   }
}
