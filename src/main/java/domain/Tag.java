package domain;


import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Access(AccessType.PROPERTY)
public class Tag  extends DomainEntity{
   private String name;
   private Collection<Trip> trip;



   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getName() {
      return this.name;
   }

   public void setName(String name) {
      this.name = name;
   }


   @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
   public Collection<Trip> getTrip() {
      return trip;
   }

   public void setTrip(Collection<Trip> trip) {
      this.trip = trip;
   }
}

