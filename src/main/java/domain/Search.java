package domain;

import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
@Access(AccessType.PROPERTY)
public class Search extends DomainEntity{

    private String keyWord;
    private Double highestPrice;
    private Double lowestPrice;
    private  Date startDate;
    private  Date endDate;
    private Date searchDate;

    private Collection<Trip> trips;
    private Explorer searcher;



    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String key) {
        this.keyWord = key;
    }

    @Temporal(TemporalType.DATE)
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Temporal(TemporalType.DATE)
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }


    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, mappedBy = "searches")
    public Collection<Trip> getTrips() {
        return trips;
    }

    public void setTrips(Collection<Trip> trips) {
        this.trips = trips;
    }

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    public Explorer getSearcher() {
        return searcher;
    }

    public void setSearcher(Explorer searcher) {
        this.searcher = searcher;
    }


    public Double getHighestPrice() {
        return highestPrice;
    }

    public void setHighestPrice(Double highestPrice) {
        this.highestPrice = highestPrice;
    }

    public Double getLowestPrice() {
        return lowestPrice;
    }

    public void setLowestPrice(Double lowestPrice) {
        this.lowestPrice = lowestPrice;
    }

    @Temporal(TemporalType.DATE)
    public Date getSearchDate() {
        return searchDate;
    }

    public void setSearchDate(Date searchDate) {
        this.searchDate = searchDate;
    }
}
