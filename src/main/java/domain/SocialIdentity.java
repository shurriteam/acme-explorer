package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;

@Entity
@Access(AccessType.PROPERTY)
public class SocialIdentity extends DomainEntity {
   private String nickname;
   private String nameOfNetwork;
   private String link;
   private String picture;
   private Actor owner;

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getNickname() {
      return nickname;
   }

   public void setNickname(String nickname) {
      this.nickname = nickname;
   }

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getNameOfNetwork() {
      return nameOfNetwork;
   }

   public void setNameOfNetwork(String socialNet) {
      this.nameOfNetwork = socialNet;
   }

   @NotBlank
   @URL
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getLink() {
      return link;
   }

   public void setLink(String link) {
      this.link = link;
   }


   @URL
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getPicture() {
      return picture;
   }

   public void setPicture(String picture) {
      this.picture = picture;
   }

   @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
   public Actor getOwner() {
      return owner;
   }

   public void setOwner(Actor owner) {
      this.owner = owner;
   }
}
