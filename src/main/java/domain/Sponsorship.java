package domain;


import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;

@Entity
@Access(AccessType.PROPERTY)
public class Sponsorship  extends  DomainEntity{
    private String imageUrl;
    private String info;
    private String name ;
    private Sponsor sponsors;
    private  Trip trips;







    @URL
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @URL
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }


    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }


    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    public Sponsor getSponsors() {
        return sponsors;
    }

    public void setSponsors(Sponsor sponsors) {
        this.sponsors = sponsors;
    }

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    public Trip getTrips() {
        return trips;
    }

    public void setTrips(Trip trips) {
        this.trips = trips;
    }
}
