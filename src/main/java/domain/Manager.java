package domain;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Access(AccessType.PROPERTY)
public class Manager extends Actor {

   private Collection<Trip> organizedTrips;
   private Collection<TripApplication> applicationsToAccept;
   private Collection<Note> notes;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "manager")
   public Collection<Trip> getOrganizedTrips() {
      return organizedTrips;
   }

   public void setOrganizedTrips(Collection<Trip> organizedTrips) {
      this.organizedTrips = organizedTrips;
   }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "manager")
   public Collection<TripApplication> getApplicationsToAccept() {
      return applicationsToAccept;
   }

   public void setApplicationsToAccept(Collection<TripApplication> applicationsToAccept) {
      this.applicationsToAccept = applicationsToAccept;

   }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "manager")
   public Collection<Note> getNotes() {
      return notes;
   }

   public void setNotes(Collection<Note> notes) {
      this.notes = notes;
   }

   @Override
   public String toString() {
      return super.getName() + ", " + super.getSurname() + ". " + "[ "+ super.getPhoneNumber() + " ]" ;
   }
}
