package domain;

import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
@Access(AccessType.PROPERTY)
public class Note extends DomainEntity{

   private Date creationMomment, replyDate;
   private String remark, reply;
   private boolean isVisible;

   private Auditor owner;
   private Manager manager;
   Collection<Trip> trips;


   @Temporal(TemporalType.DATE)
   public Date getCreationMomment() {
      return creationMomment;
   }

   public void setCreationMomment(Date creationMomment) {
      this.creationMomment = creationMomment;
   }

   @Temporal(TemporalType.DATE)
   public Date getReplyDate() {
      return replyDate;
   }

   public void setReplyDate(Date replyDate) {
      this.replyDate = replyDate;
   }
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getRemark() {
      return remark;
   }

   public void setRemark(String remark) {
      this.remark = remark;
   }
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getReply() {
      return reply;
   }

   public void setReply(String reply) {
      this.reply = reply;
   }

   public boolean isVisible() {
      return isVisible;
   }

   public void setVisible(boolean visible) {
      isVisible = visible;
   }


   @ManyToOne(cascade = CascadeType.ALL)
   public Auditor getOwner() {
      return owner;
   }

   public void setOwner(Auditor owner) {
      this.owner = owner;
   }

   @ManyToOne(cascade = CascadeType.ALL)
   public Manager getManager() {
      return manager;
   }

   public void setManager(Manager manager) {
      this.manager = manager;
   }

   @ManyToMany(cascade = CascadeType.ALL, mappedBy = "notes")
   public Collection<Trip> getTrips() {
      return trips;
   }

   public void setTrips(Collection<Trip> trips) {
      this.trips = trips;
   }
}
