package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;

@Entity
@Access(AccessType.PROPERTY)

public class Requirement extends DomainEntity{

   private String name, description;
   private Trip associatedTrip;
   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }


   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getDescription() {
      return description;
   }

   public void setDescription(String description) {
      this.description = description;
   }

   @ManyToOne(cascade = CascadeType.ALL)
   public Trip getAssociatedTrip() {
      return associatedTrip;
   }

   public void setAssociatedTrip(Trip associatedTrip) {
      this.associatedTrip = associatedTrip;
   }
}
