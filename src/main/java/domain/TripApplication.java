package domain;

import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Date;

@Entity
@Access(AccessType.PROPERTY)
public class TripApplication extends DomainEntity {
   private Trip trip;
   private Manager manager;
   private Date creationDate;
   private Status status;
   private boolean isCancelled;
   private String cancellReason;
   private Collection<Comment> comments;
   private Explorer owner;



   @NotNull
   @Temporal(TemporalType.DATE)
   public Date getCreationDate() {
      return creationDate;
   }

   public void setCreationDate(Date creationDate) {
      this.creationDate = creationDate;
   }

   @NotNull
   public Status getStatus() {
      return status;
   }

   public void setStatus(Status status) {
      this.status = status;
   }

   @OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, targetEntity = Comment.class)
   public Collection<Comment> getComments() {
      return comments;
   }

   public void setComments(Collection<Comment> comments) {
      this.comments = comments;
   }

   @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
   public Explorer getOwner() {
      return owner;
   }

   public void setOwner(Explorer owner) {
      this.owner = owner;
   }


   @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
   public Trip getTrip() {
      return trip;
   }

   public void setTrip(Trip trip) {
      this.trip = trip;
   }

   @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
   public Manager getManager() {
      return manager;
   }

   public void setManager(Manager manager) {
      this.manager = manager;
   }


   public boolean isCancelled() {
      return isCancelled;
   }

   public void setCancelled(boolean cancelled) {
      isCancelled = cancelled;
   }

   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getCancellReason() {
      return cancellReason;
   }

   public void setCancellReason(String cancellReason) {
      this.cancellReason = cancellReason;
   }

   @Override
   public String toString() {
      return "TripApplication{" +
              "trip=" + trip +
              ", creationDate=" + creationDate +
              '}';
   }
}
