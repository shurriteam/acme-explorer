package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;


import org.hibernate.validator.constraints.*;

import javax.persistence.*;

@Entity
@Access(AccessType.PROPERTY)
public class PersonalRecord  extends DomainEntity{
    private String name;
    private String photo;
    private String email;
    private String phone;
    private String linked;
    private Curricula curricula;

    @OneToOne(cascade = CascadeType.PERSIST)
    public Curricula getCurricula() {
        return curricula;
    }

    public void setCurricula(Curricula curricula) {
        this.curricula = curricula;
    }

    @NotEmpty
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @URL
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }



    @Email
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    @URL
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    public String getLinked() {
        return linked;
    }
    public void setLinked(String linked) {
        this.linked = linked;
    }
}
