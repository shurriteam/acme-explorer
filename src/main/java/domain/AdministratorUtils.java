/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package domain;

import org.hibernate.validator.constraints.Range;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;


@Entity
@Access(AccessType.PROPERTY)
public class AdministratorUtils extends DomainEntity {

   private Collection<Mezzage> broadcastedMezzanges;
   private String banners;
   private int searchCache;
   private int searchNumber;
   private int VAX;

   @OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
   public Collection<Mezzage> getBroadcastedMezzanges() {
      return broadcastedMezzanges;
   }

   public void setBroadcastedMezzanges(Collection<Mezzage> broadcastedMezzanges) {
      this.broadcastedMezzanges = broadcastedMezzanges;
   }

   @URL
   @NotNull
   public String getBanners() {
      return banners;
   }

   public void setBanners(String banners) {
      this.banners = banners;
   }

   @NotNull
   @Range(min = 1, max = 25)
   public int getSearchCache() {
      return searchCache;
   }

   public void setSearchCache(int searchCache) {
      this.searchCache = searchCache;
   }


   @NotNull
   @Range(min = 1, max = 100)
   public int getSearchNumber() {
      return searchNumber;
   }

   public void setSearchNumber(int searchNumber) {
      this.searchNumber = searchNumber;
   }
   @NotNull
   @Range(min = 1, max = 100)
   public int getVAX() {
      return VAX;
   }

   public void setVAX(int VAX) {
      this.VAX = VAX;
   }
}
