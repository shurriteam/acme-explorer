package domain;


import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
@Access(AccessType.PROPERTY)
public class SurvivalClass extends DomainEntity{
    private String title;
    private String description;
    private  Date startDate;
    private Sponsor sponsors;
    private Trip trip;
    private Location location;
    private Collection<Explorer> explorers;




    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Temporal(TemporalType.TIMESTAMP)
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }


    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    public Sponsor getSponsors() {
        return sponsors;
    }

    public void setSponsors(Sponsor sponsors) {
        this.sponsors = sponsors;
    }


    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    @OneToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    public domain.Location getLocation() {
        return location;
    }

    public void setLocation(domain.Location location) {
        this.location = location;
    }

    @ManyToMany(cascade =CascadeType.PERSIST)
    public Collection<Explorer> getExplorers() {
        return explorers;
    }

    public void setExplorers(Collection<Explorer> explorers) {
        this.explorers = explorers;
    }
}