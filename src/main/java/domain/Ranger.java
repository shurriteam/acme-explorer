package domain;

        import javax.persistence.*;
        import java.util.Collection;

        @Entity
@Access(AccessType.PROPERTY)
public class Ranger extends Actor {
private Collection<Trip> tripsToGuide;
private  Curricula curricula;

          @OneToOne(cascade = CascadeType.PERSIST)
   public Curricula getCurricula() {
           return curricula;
        }

         public void setCurricula(Curricula curricula) {
           this.curricula = curricula;
        }

           @OneToMany(cascade = CascadeType.ALL, mappedBy = "guide")
   public Collection<Trip> getTripsToGuide() {
           return tripsToGuide;
        }

           public void setTripsToGuide(Collection<Trip> tripsToGuide) {
           this.tripsToGuide = tripsToGuide;
        }


        }