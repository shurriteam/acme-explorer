/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;


@Entity
@Access(AccessType.PROPERTY)
public class Comment extends DomainEntity {


   private String title;
   private String body;
   private Explorer owner;


@NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getTitle() {
      return title;
   }

   public void setTitle(String title) {
      this.title = title;
   }

   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getBody() {
      return body;
   }

   public void setBody(String text) {
      this.body = text;
   }

   @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
   public Explorer getOwner() {
      return owner;
   }

   public void setOwner(Explorer owner) {
      this.owner = owner;
   }


   @Override
   public String toString() {
      return "Comment{" +
              "title='" + title + '\'' +
              ", text='" + body + '\'' +
              '}';
   }
}
