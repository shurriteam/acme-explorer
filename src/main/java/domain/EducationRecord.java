package domain;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.util.Collection;
import java.util.Date;


@Entity
@Access(AccessType.PROPERTY)

public class EducationRecord  extends DomainEntity{
    private String title;
    private Date startDate;
    private Date endDate;
    private String expeditioninstitution;
    private String attachment;
    private String comments;
    private Curricula curricula;




    @NotNull
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    @Past
    @NotNull
    @Temporal(TemporalType.DATE)
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    @NotNull
    @Temporal(TemporalType.DATE)
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    @NotEmpty
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    public String getExpeditioninstitution() {
        return expeditioninstitution;
    }

    public void setExpeditioninstitution(String expeditioninstitution) {
        this.expeditioninstitution = expeditioninstitution;
    }
    @URL
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }
    @ManyToOne(cascade = CascadeType.PERSIST)
    public Curricula getCurricula() {
        return curricula;
    }

    public void setCurricula(Curricula curricula) {
        this.curricula = curricula;
    }

    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
}
