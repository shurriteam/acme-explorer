<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>


<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<div id="main-content">


    <div id="searcher">

        <form:form action="searcher/searchAn.do" modelAttribute="search">

            <form:hidden path="id"/>
            <form:hidden path="version"/>
            <form:hidden path="trips"/>
            <form:hidden path="searcher"/>


            <acme:textbox path="keyWord" code="search.keyword"/>
            <br/>
            <acme:textbox path="highestPrice" code="search.highestPrice"/>
            <br/>
            <acme:textbox path="lowestPrice" code="search.lowestPrice"/>
            <br/>
            <acme:textbox path="startDate" code="search.startDate"/>
            <br/>
            <acme:textbox path="endDate" code="search.endDate"/>
            <br/>


            <!---------------------------- BOTONES -------------------------->

            <acme:submit name="save" code="search.save"/>

            <a class="button" href="/welcome/index.do"><spring:message code="general.cancel"/></a>


        </form:form>

    </div>


    <div id="recent-Searches" style="left: auto">

        <display:table pagesize="5" class="displaytag" keepStatus="true"
                       name="mySearchs" requestURI="${requestURI}" id="row">


            <spring:message code="search.keyword" var="keyword"/>
            <display:column property="keyWord" title="${keyword}" sortable="true"/>
            <spring:message code="search.highestPrice" var="highestPrice"/>
            <display:column property="highestPrice" title="${highestPrice}" sortable="true"/>
            <spring:message code="search.lowestPrice" var="lowestPrice"/>
            <display:column property="lowestPrice" title="${lowestPrice}" sortable="true"/>
            <spring:message code="search.startDate" var="startDate"/>
            <display:column property="startDate" title="${startDate}" sortable="true"/>
            <spring:message code="search.endDate" var="endDate"/>
            <display:column property="endDate" title="${endDate}" sortable="true"/>


            <display:column>
                <a href="searcher/view.do?searchId=${row.id}"> <spring:message
                        code="general.view"/>
                </a>
            </display:column>


        </display:table>

    </div>

</div>