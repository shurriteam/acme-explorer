<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<div id="main-content">

    <div id="trips-searched">

        <spring:message code="search.trips" var="trips1"/>
        <h2><jstl:out value="${trips1}"/></h2>
        <hr>

        <c:forEach items="${trips}" var="trip">
            <div id="box-${trip.title}">

                <h2><jstl:out value="${trip.title}"/></h2>
                <h2><jstl:out value="${trip.startDate}"/></h2>
                <p>
                    <jstl:out value="${trip.description}"/>
                </p>
            </div>
            <hr>

        </c:forEach>

    </div>


</div>