<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>

<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<security:authorize access="hasRole('MANAGER')">
    <div>
        <H5>
            <a href="survivalClass/create.do"> <spring:message code="general.create"/>
            </a>
        </H5>
    </div>
</security:authorize>

<!-- Listing grid -->
<display:table pagesize="5" class="displaytag" keepStatus="true"
               name="survivalClasses" requestURI="${requestURI}" id="row">


    <!-- Attributes -->

    <security:authorize access="hasRole('MANAGER')">
        <display:column>
            <a href="survivalClass/edit.do?survivalClassId=${row.id}"> <spring:message
                    code="general.edit"/>
            </a>
        </display:column>
        <display:column>
            <a href="survivalClass/delete.do?survivalClassId=${row.id}"> <spring:message
                    code="general.delete"/>
            </a>
        </display:column>
    </security:authorize>


    <%--<security:authorize access="hasRole('EXPLORER')">--%>
    <%--<display:column>--%>
    <%--<a href="survivalClass/enroll.do?survivalClassId=${row.id}"> <spring:message--%>
    <%--code="general.enrol"/>--%>
    <%--</a>--%>
    <%--</display:column>--%>
    <%--</security:authorize>--%>
    <%--<security:authorize access="hasRole('EXPLORER')">--%>
    <%--<display:column>--%>
    <%--<a href="survivalClass/unroll.do?survivalClassId=${row.id}"> <spring:message--%>
    <%--code="general.disenrol"/>--%>
    <%--</a>--%>
    <%--</display:column>--%>
    <%--</security:authorize>--%>


    <spring:message code="survivalClass.title" var="title"/>
    <display:column property="title" title="${title}" sortable="true"/>

    <spring:message code="survivalClass.description" var="description"/>
    <display:column property="description" title="${description}" sortable="true"/>

    <spring:message code="survivalClass.startDate" var="startDate"/>
    <display:column property="startDate" title="${startDate}" sortable="true"/>
    <spring:message code="survivalClass.sponsors" var="sponsors"/>

    <display:column property="sponsors.name" title="${sponsors}" sortable="true"/>
    <spring:message code="survivalClass.trip" var="trip"/>

    <display:column property="trip.title" title="${trip}" sortable="true"/>
    <spring:message code="survivalClass.location" var="location"/>
    <display:column property="location" title="${location}" sortable="true"/>

</display:table>