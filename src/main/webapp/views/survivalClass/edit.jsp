<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>


<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<form:form action="survivalClass/edit.do" modelAttribute="survivalClass">

    <form:hidden path="id"/>
    <form:hidden path="version"/>
    <form:hidden path="explorers"/>


    <acme:textbox path="title" code="survivalClass.title"/>
    <br/>

    <acme:textarea path="description" code="survivalClass.description"/>
    <br/>
    <acme:textbox path="startDate" code="survivalClass.startDate"/>
    <br/>
    <acme:textbox path="location.longitude" code="location.longitude"/>
    <br/>
    <acme:textbox path="location.latitude" code="location.latitude"/>
    <br/>

    <acme:textbox path="location.longitudeDir" code="location.longitudeDir"/>
    <br/>

    <acme:textbox path="location.latitudeDir" code="location.latitudeDir"/>
    <br/>

    <acme:select path="trip" code="survivalClass.trip" items="${trips}" itemLabel="ticker"/>
    <br/>
    <acme:select path="sponsors" code="survivalClass.sponsors" items="${sponsors}" itemLabel="name"/>
    <br/>



    <!---------------------------- BOTONES -------------------------->
    <input class="button2" type="submit" name="save"
           value="<spring:message code="general.save" />"/>


    <a class="button" href="welcome/index.do"><spring:message code="general.cancel"/></a>


</form:form>