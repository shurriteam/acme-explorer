<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>




<spring:message code="audit.momment" var="name1"/>
<h3><jstl:out value="${name1}"/></h3>
<jstl:out value="${moment}"/>


<spring:message code="audit.title" var="name1"/>
<h3><jstl:out value="${name1}"/></h3>
<jstl:out value="${title}"/>

<spring:message code="audit.description" var="name1"/>
<h3><jstl:out value="${name1}"/></h3>
<jstl:out value="${description}"/>


<spring:message code="audit.attachment" var="name1"/>
<h3><jstl:out value="${name1}"/></h3>
<jstl:out value="${attachment}"/>


<spring:message code="audit.trip" var="name1"/>
<h3><jstl:out value="${name1}"/></h3>
<jstl:out value="${trip}"/>

<spring:message code="audit.auditStatus" var="name1"/>
<h3><jstl:out value="${name1}"/></h3>
<jstl:out value="${auditStatus}"/>

<br>
<a class="button" href="/audit/list.do"><spring:message code="general.cancel"/></a>