<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>

<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>



<!-- Listing grid -->
<display:table pagesize="5" class="displaytag" keepStatus="true"
               name="audits" requestURI="${requestURI}" id="row">


    <!-- Attributes -->
    <%--<spring:eval expression="row.auditStatus == T(domain.AuditStatus).DRAFT" var="draft"/>--%>
    <%--<jstl:if test="${draft}">--%>
        <%----%>

    <%--</jstl:if>--%>

    <spring:message code="audit.title" var="msg"/>
    <display:column property="title" title="${msg}" sortable="true"/>

    <spring:message code="audit.description" var="description"/>
    <display:column property="description" title="${description}" sortable="true"/>

    <spring:message code="audit.auditStatus" var="auditStatus"/>
    <display:column property="auditStatus" title="${auditStatus}" sortable="true"/>

    <spring:message code="audit.momment" var="momment"/>
    <display:column property="momment" title="${amomment}" sortable="true"/>

    <spring:message code="audit.auditor" var="auditor"/>
    <display:column property="auditor" title="${auditor}" sortable="true"/>

    <spring:message code="audit.trip" var="trip"/>
    <display:column property="trip" title="${trip}" sortable="true"/>

    <spring:message code="audit.attachment" var="attachment"/>
    <display:column property="attachment" title="${attachment}" sortable="true"/>

    <security:authorize access="hasRole('AUDITOR')">


        <display:column>
            <a href="audit/publish.do?auditId=${row.id}"> <spring:message
                    code="legaltext.publish"/>
            </a>
        </display:column>


        <display:column>
            <a href="audit/edit.do?auditId=${row.id}"> <spring:message
                    code="general.edit"/>
            </a>
        </display:column>

        <display:column>
            <a href="audit/delete.do?auditId=${row.id}"> <spring:message
                    code="general.delete"/>
            </a>
        </display:column>

    <display:column>
        <a href="audit/createA.do?auditId=${row.id}"> <spring:message
                code="audit.Attachment"/>
        </a>
    </display:column>


    </security:authorize>



</display:table>