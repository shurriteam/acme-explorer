<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>

<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<security:authorize access="hasRole('MANAGER')">
    <div>
        <H5>
            <a class="button2" href="trip/create.do"> <spring:message
                    code="general.create"/>
            </a>
        </H5>
    </div>
</security:authorize>


<display:table pagesize="5" class="displaytag" keepStatus="true"
               name="myTrips" requestURI="${requestURI}" id="row">


    <!-- Attributes -->
    <security:authorize access="hasRole('MANAGER')">
        <display:column>
            <a href="trip/edit.do?tripId=${row.id}"> <spring:message
                    code="general.edit"/>
            </a>
        </display:column>
        <display:column>
            <a href="trip/delete.do?tripId=${row.id}"> <spring:message
                    code="general.delete"/>
            </a>
        </display:column>
        <display:column>
            <a href="trip/cancel2.do?tripId=${row.id}"> <spring:message
                    code="general.cancel"/>
            </a>
        </display:column>
    </security:authorize>

    <spring:message code="trip.ticker" var="ticker"/>
    <display:column property="ticker" title="${ticker}" sortable="true"/>

    <spring:message code="trip.title" var="title"/>
    <display:column property="title" title="${title}" sortable="true"/>

    <spring:message code="trip.description" var="description"/>
    <display:column property="description" title="${description}" sortable="true"/>

    <spring:message code="trip.startDate" var="startDate"/>
    <display:column property="startDate" title="${startDate}" sortable="true"/>

    <spring:message code="trip.endDate" var="endDate"/>
    <display:column property="endDate" title="${endDate}" sortable="true"/>

    <spring:message code="trip.publicationDate" var="publicationDate"/>
    <display:column property="publicationDate" title="${publicationDate}" sortable="true"/>

    <spring:message code="trip.isCancelled" var="cancelled"/>
    <display:column property="cancelled" title="${cancelled}" sortable="true"/>
    <spring:message code="trip.manager" var="manager"/>
    <display:column property="manager" title="${manager}" sortable="true"/>

    <spring:message code="trip.category" var="category"/>
    <display:column property="category" title="${category}" sortable="true"/>


    <security:authorize access="isAuthenticated()">
        <display:column>
            <a href="trip/view.do?tripId=${row.id}"> <spring:message
                    code="general.view"/>
            </a>
        </display:column>
    </security:authorize>
    <security:authorize access="isAnonymous()">
        <display:column>
            <a href="trip/viewAnonimous.do?tripId=${row.id}"> <spring:message
                    code="general.view"/>
            </a>
        </display:column>
    </security:authorize>

</display:table>


<!-- Listing grid -->
<display:table pagesize="5" class="displaytag" keepStatus="true"
               name="trips" requestURI="${requestURI}" id="row">


    <!-- Attributes -->

    <spring:message code="trip.ticker" var="ticker"/>
    <display:column property="ticker" title="${ticker}" sortable="true"/>

    <spring:message code="trip.title" var="title"/>
    <display:column property="title" title="${title}" sortable="true"/>

    <spring:message code="trip.description" var="description"/>
    <display:column property="description" title="${description}" sortable="true"/>

    <spring:message code="trip.startDate" var="startDate"/>
    <display:column property="startDate" title="${startDate}" sortable="true"/>

    <spring:message code="trip.endDate" var="endDate"/>
    <display:column property="endDate" title="${endDate}" sortable="true"/>

    <spring:message code="trip.publicationDate" var="publicationDate"/>
    <display:column property="publicationDate" title="${publicationDate}" sortable="true"/>

    <spring:message code="trip.manager" var="manager"/>
    <display:column property="manager" title="${manager}" sortable="true"/>

    <spring:message code="trip.category" var="category"/>
    <display:column property="category" title="${category}" sortable="true"/>

    <security:authorize access="isAuthenticated()">
        <display:column>
            <a href="trip/view.do?tripId=${row.id}"> <spring:message
                    code="general.view"/>
            </a>
        </display:column>
    </security:authorize>
    <security:authorize access="isAnonymous()">
        <display:column>
            <a href="trip/viewAnonimous.do?tripId=${row.id}"> <spring:message
                    code="general.view"/>
            </a>
        </display:column>
    </security:authorize>

</display:table>



