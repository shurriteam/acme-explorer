<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>


<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<form:form action="trip/cancel2.do" modelAttribute="trip">

    <form:hidden path="id"/>
    <form:hidden path="version"/>
    <form:hidden path="price"/>
    <form:hidden path="manager"/>
    <form:hidden path="title"/>
    <form:hidden path="description"/>
    <form:hidden path="startDate"/>
    <form:hidden path="endDate"/>
    <form:hidden path="publicationDate"/>

    <form:hidden path="ticker"/>

    <form:hidden path="cancelled"/>
    <form:hidden path="requirements"/>
    <form:hidden path="stories"/>
    <form:hidden path="sponsorships"/>
    <form:hidden path="audits"/>
    <form:hidden path="notes"/>
    <form:hidden path="searches"/>
    <form:hidden path="survivalClasses"/>



    <form:hidden path="image"/>
    <form:hidden path="category"/>
    <form:hidden path="guide"/>
    <form:hidden path="legalTexts"/>
    <form:hidden path="tags"/>
    <form:hidden path="stages"/>


    <acme:textbox path="cancelReason" code="trip.cancelReason"/>
    <br/>

    <!---------------------------- BOTONES -------------------------->
    <input class="button2" type="submit" name="cancel2"
           value="<spring:message code="general.cancel" />"/>



</form:form>