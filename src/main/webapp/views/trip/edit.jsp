<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>


<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<form:form action="trip/edit.do" modelAttribute="trip">

    <form:hidden path="id"/>
    <form:hidden path="version"/>
    <%--<form:hidden path="price"/>--%>
    <form:hidden path="manager"/>
    <%--<form:hidden path="ticker"/>--%>
    <%--<form:hidden path="cancelled"/>--%>
    <form:hidden path="requirements"/>
    <%--<form:hidden path="stories"/>--%>
    <%--<form:hidden path="sponsorships"/>--%>
    <%--<form:hidden path="audits"/>--%>
    <%--<form:hidden path="notes"/>--%>
    <%--<form:hidden path="searches"/>--%>
    <%--<form:hidden path="survivalClasses"/>--%>
    <form:hidden path="stages"/>


    <acme:textbox path="title" code="trip.title"/>
    <br/>
    <acme:textarea path="description" code="trip.description"/>
    <br/>
    <acme:textbox path="startDate" code="trip.startDate"/>
    <br/>
    <acme:textbox path="endDate" code="trip.endDate"/>
    <br/>
    <acme:textbox path="publicationDate" code="trip.publicationDate"/>
    <br/>
    <acme:textbox path="image" code="trip.image"/>
    <br/>
    <acme:select path="category" code="trip.category" items="${categories}" itemLabel="name"/>
    <br/>
    <acme:select path="guide" code="trip.guide" items="${guidess}" itemLabel="name"/>
    <br/>
    <acme:select path="legalTexts" code="trip.legalText" items="${legalTexts}" itemLabel="title"/>
    <br/>
    <%--<acme:select path="stages" code="trip.stages" items="${stages}" itemLabel="title"/>--%>
    <%--<br/>--%>
    <acme:select path="tags" code="trip.tags" items="${tags}" itemLabel="name"/>
    <br/>
    <%--<acme:select path="tags" code="trip.tags" items="${tags}" itemLabel="name"/>--%>
    <%--<br/>--%>
    <%--<acme:select path="tags" code="trip.tags" items="${tags}" itemLabel="name"/>--%>
    <%--<br/>--%>
    <%--<acme:select path="requirements" code="trip.requirements" items="${requirements}" itemLabel="name"/>--%>
    <%--<br/>--%>

    <!---------------------------- BOTONES -------------------------->
    <input class="button2" type="submit" name="save"
           value="<spring:message code="general.save" />"/>


    <a class="button2" href="/welcome/index.do"><spring:message code="general.cancel"/></a>


</form:form>