<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<%--
  Created by IntelliJ IDEA.
  User: mruwzum
  Date: 1/3/17
  Time: 11:46
  To change this template use File | Settings | File Templates.
--%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<div id="header-trip">
    <div id="header-image">
        <img src="${image}" id="trip-image">
    </div>
    <div id="header-info">

        <h2><jstl:out value="${title}"/></h2>

        <h4 style="color:grey;"><jstl:out value="${ticker} "/></h4>

        <security:authorize access="hasRole('MANAGER')">
            <%--Comprobamos si el manager es propiertario del viaje--%>
            <jstl:if test="${isMy}">
                <a href="/welcome/index.do"><spring:message code="general.edit"/></a>
            </jstl:if>
        </security:authorize>


        <%--Se comprueba si el viaje est� cancelado para mostrar la etiqueta correspondiente--%>
        <jstl:if test="${isCancelled}">
            <spring:message code="trip.cancelled" var="cancelled"/>
            <p style="color:red;"><jstl:out value="${cancelled} "/></p>

        </jstl:if>

        <spring:message code="trip.category" var="categorilabel"/>
        <h3><jstl:out value="${categorilabel}"/></h3>
        <jstl:out value="${category1}"/>

        <%--Se comprueba si el viaje est� cancelado para mostrar el motivo de cancelaci�n--%>

        <jstl:if test="${isCancelled}">
            <p>
                <jstl:out value="${cancelledReason}"/>
            </p>

        </jstl:if>

        <jstl:if test="${not isCancelled}">
            <p>
                <jstl:out value="${description}"/>
            </p>

        </jstl:if>


        <h3><jstl:out value="${price}?"/></h3>


        <spring:message code="trip.startDate" var="startDate2"/>
        <h4><jstl:out value="${startDate2}"/></h4>
        <jstl:out value="${startDate}"/>

        <spring:message code="trip.endDate" var="endDate2"/>
        <h2><jstl:out value="${endDate2}"/></h2>
        <jstl:out value="${endDate}"/>

        <security:authorize access="hasRole('EXPLORER')">
            <%--Comprobamos si explorer ha aplicado o no al viaje y mostramos el bot�n correspondiente--%>
            <jstl:if test="${not registered}">
                <a class="button2" href="/tripApplication/apply.do?tripId=${tripid}"><spring:message code="trip.apply"/></a>
            </jstl:if>

            <jstl:if test="${registered}">
                <a class="button2" href="/tripApplication/unapply.do?tripId=${tripid}"><spring:message code="trip.unapply"/></a>
            </jstl:if>
        </security:authorize>

    </div>
</div>
<hr>


<div id="main-conent">
    <div id="guide-box">
        <spring:message code="trip.guide" var="guide1"/>
        <h2><jstl:out value="${guide1}"/></h2>

        <img src="${gimg}" id="guide-image">

        <h3><jstl:out value="${guideName}"/></h3>

        <h3><jstl:out value="${guideSurname}"/></h3>

            <a href="ranger/view.do?rangerId=${raangerId}"> <spring:message
                    code="general.viewRangersProfile"/>
            </a>

    </div>
    <hr>

    <div id="stages-box">

        <spring:message code="trip.stages" var="stages1"/>
        <h2><jstl:out value="${stages1}"/></h2>
        <security:authorize access="hasRole('MANAGER')">
            <%--Comprobamos si el manager es propiertario del viaje--%>
            <jstl:if test="${isMy}">
                <a class="button2" href="/trip/createStage.do?tripId=${tripid}"><spring:message code="trip.create.stage"/></a>
            </jstl:if>
        </security:authorize>
        <c:forEach items="${stages}" var="stage">
            <div id="${stage}-box">

                <h2><jstl:out value="${stage.numberOfStage}"/></h2>
                <h2><jstl:out value="${stage.title}"/></h2>
                <p>
                    <jstl:out value="${stage.description}"/>
                </p>
                <h3><jstl:out value="${stage.price}"/></h3>
            </div>
        </c:forEach>

    </div>
    <hr>

    <div id="sponsorship-box">
        <spring:message code="trip.sponsors" var="sponsors2"/>
        <h2><jstl:out value="${sponsors2}"/></h2>

        <c:forEach items="${sponsorships}" var="sponsorship">
            <div id="${sponsorship}-box">
                <security:authorize access="hasRole('SPONSOR')">
                    <%--Comprobamos si el sponsor es propiertario del anuncio--%>
                    <jstl:if test="${isMySponsorship}">
                        <a href="/welcome/index.do"><spring:message code="general.edit"/></a>
                    </jstl:if>
                </security:authorize>
                <h2><jstl:out value="${sponsorship.name}"/></h2>
                <p>
                    <jstl:out value="${sponsorship.info}"/>
                </p>
                <img src="${sponsorship.imageUrl}" id="sponsorhip-image">

            </div>

        </c:forEach>

    </div>
    <hr>

    <div id="survival-box">
        <spring:message code="trip.sv" var="sv2"/>
        <h2><jstl:out value="${sv2}"/></h2>

        <c:forEach items="${survivalClasses}" var="survivalClass">
            <div id="${survivalClass}-box">

                <h2><jstl:out value="${survivalClass.title}"/></h2>
                <p>
                    <jstl:out value="${survivalClass.description}"/>
                </p>

                <spring:message code="trip.startDate" var="startDate2"/>
                <h4><jstl:out value="${startDate2}"/></h4>
                <jstl:out value="${survivalClass.startDate}"/>

                <spring:message code="survivalClass.location" var="location2"/>
                <h4><jstl:out value="${location2}"/></h4>
                <jstl:out value="${survivalClass.location}"/>

                <display:table pagesize="15" class="displaytag" keepStatus="true"
                               name="${survivalClass.explorers}" id="row1">


                        <spring:message code="actor.name" var="name1"/>
                        <display:column property="name" title="${name1}" sortable="true"/>
                        <spring:message code="actor.surname" var="surname1"/>
                        <display:column property="surname" title="${surname1}" sortable="true"/>
                        <spring:message code="actor.email" var="email1"/>
                        <display:column property="email" title="${email1}" sortable="true"/>
                    </display:table>

                <security:authorize access="hasRole('EXPLORER')">
                    <jstl:if test="${rolled}">

                    <%--Comprobamos si es Ranger para que pueda inscribirse en una survival class--%>
                        <a class="button2"
                           href="survivalClass/unenroll.do?survivalClassId=${survivalClass.id}"><spring:message
                                code="trip.unregistersv"/></a>
                    </jstl:if>
                    <jstl:if test="${not rolled}">

                        <a class="button2"
                           href="survivalClass/enroll.do?survivalClassId=${survivalClass.id}"><spring:message
                                code="trip.registersv"/></a>
                    </jstl:if>

                </security:authorize>



                <security:authorize access="hasRole('MANAGER')">
                    <%--Comprobamos si el manager es propiertario del viaje--%>
                    <jstl:if test="${isMy}">
                        <a href="/survivalClass/edit.do"><spring:message code="general.edit"/></a>
                    </jstl:if>
                </security:authorize>


            </div>

        </c:forEach>

    </div>
</div>
<hr>


<div id="middle-box">
    <div id="notes-box">
        <spring:message code="trip.notes" var="notes2"/>
        <h2><jstl:out value="${notes2}"/></h2>

        <c:forEach items="${notes}" var="note">
            <%--Comprobamos si puede mostrarse o no--%>
            <jstl:if test="${not note.visible}">
                <div id="${note}-box">

                    <h2><jstl:out value="${note.owner}"/></h2>
                    <h4 style="color:grey;"><jstl:out value="${note.creationMomment}"/></h4>
                    <p>
                        <jstl:out value="${note.remark}"/>
                    </p>

                    <p>
                        <jstl:out value="${note.reply}"/>
                    </p>

                    <security:authorize access="hasRole('MANAGER')">
                        <%--Comprobamos si el manager es propiertario del viaje--%>
                        <jstl:if test="${isMy}">
                            <a href="/note/reply.do?noteId=${note.id}"><spring:message code="general.reply"/></a>
                        </jstl:if>
                    </security:authorize>




                </div>
            </jstl:if>
        </c:forEach>
        <security:authorize access="hasRole('AUDIT')">
            <%--Comprobamos si es auditor para que pueda escribir una nueva nota--%>
            <a class="button" href="/note/create.do"><spring:message code="general.edit"/></a>
        </security:authorize>
    </div>
    <hr>

    <div id="audit-box">
        <spring:message code="trip.audits" var="audits2"/>
        <h2><jstl:out value="${audits2}"/></h2>

        <c:forEach items="${audits}" var="audit">
            <div id="${audit}-box">

                <h2><jstl:out value="${audit.title}"/></h2>
                <h4 style="color:grey;"><jstl:out value="${audit.momment}"/></h4>
                <h4 style="color:grey;"><jstl:out value="${audit.auditor}"/></h4>
                <p>
                    <jstl:out value="${audit.description}"/>
                </p>

                    <%--&lt;%&ndash;TODO mirar como mostrar esta tabla con la variable que se est� recorriendo&ndash;%&gt;--%>
                <display:table pagesize="5" class="displaytag" keepStatus="true"
                               name="audit.attachment" requestURI="${requestURI}" id="row">

                    <spring:message code="attachment.name" var="name"/>
                    <display:column property="name" title="${name}" sortable="true"/>

                    <spring:message code="attachment.url" var="url"/>
                    <display:column property="url" title="${url}" sortable="true"/>

                </display:table>


            </div>
        </c:forEach>
        <security:authorize access="hasRole('AUDITOR')">
            <%--Comprobamos si es auditor para que pueda escribir una nueva auditor�a--%>
                <a href="audit/create.do?tripId=${tripid}" class="button2"> <spring:message
                    code="general.create"/>
            </a>
        </security:authorize>
    </div>
</div>
<hr>


<div id="stories-box">
    <spring:message code="trip.stories" var="stories2"/>
    <h2><jstl:out value="${stories2}"/></h2>
    <security:authorize access="hasRole('EXPLORER')">
        <%--Comprobamos si es explorer y si ha participado en este viaje para que pueda escribir una nueva historia--%>
        <jstl:if test="${writer}">
            <a class="button2" href="/story/create.do?tripId=${tripid}"><spring:message code="general.create"/></a>
        </jstl:if>

    </security:authorize>
    <c:forEach items="${stories}" var="story">
        <div id="${story}-box">


            <h2><jstl:out value="${story.title}"/></h2>
            <h4 style="color:grey;"><jstl:out value="${story.explorer}"/></h4>
            <p>
                <jstl:out value="${story.text}"/>
            </p>

                <%--TODO mirar como mostrar esta tabla con la variable que se est� recorriendo--%>
            <display:table pagesize="5" class="displaytag" keepStatus="true"
                           name="story.attachments" requestURI="${requestURI}" id="row">

                <spring:message code="attachment.name" var="name"/>
                <display:column property="name" title="${name}" sortable="true"/>

                <spring:message code="attachment.url" var="url"/>
                <display:column property="url" title="${url}" sortable="true"/>

            </display:table>

        </div>
    </c:forEach>
</div>

<hr>

<div id="application-box">
    <security:authorize access="hasRole('MANAGER')">
        <%--Comprobamos si el manager es propiertario del viaje--%>
        <jstl:if test="${isMy}">

            <spring:message code="trip.applications" var="applications2"/>
            <h2><jstl:out value="${applications2}"/></h2>

            <display:table pagesize="5" class="displaytag" keepStatus="true"
                           name="applications" requestURI="${requestURI}" id="row">

                <jstl:if test="${not empty applications}">
                    <spring:eval expression="row.status == T(domain.Status).PENDING" var="pend"/>
                    <spring:eval expression="row.status == T(domain.Status).ACCEPTED" var="accepeted"/>
                    <spring:eval expression="row.status == T(domain.Status).REJECTED" var="isrejected"/>
                    <spring:eval expression="row.status == T(domain.Status).DUE" var="isdue"/>
                    <spring:eval expression="row.status == T(domain.Status).CANCELLED" var="iscancel"/>
                </jstl:if>
                <spring:message code="application.owner" var="owner"/>
                <display:column property="owner" title="${owner}" sortable="true"/>

                <spring:message code="application.creationDate" var="creationDate"/>
                <display:column property="creationDate" title="${creationDate}" sortable="true"/>

                <jstl:if test="${accepeted}">
                <spring:message code="application.status" var="status"/>
                <display:column style="background-color: green;" property="status" title="${status}" sortable="true"/>
                </jstl:if>

                <jstl:if test="${iscancel}">
                    <spring:message code="application.status" var="status"/>
                    <display:column style="background-color: cyan;" property="status" title="${status}" sortable="true"/>
                </jstl:if>

                <jstl:if test="${isdue}">
                    <spring:message code="application.status" var="status"/>
                    <display:column style="background-color: yellow;" property="status" title="${status}" sortable="true"/>
                </jstl:if>

                <jstl:if test="${isrejected}">
                    <spring:message code="application.status" var="status"/>
                    <display:column style="background-color: grey;" property="status" title="${status}" sortable="true"/>
                </jstl:if>

                <jstl:if test="${pend}">
                    <jstl:if test="${nextM}">
                    <spring:message code="application.status" var="status"/>
                    <display:column style="background-color: red;" property="status" title="${status}" sortable="true"/>
                    </jstl:if>

                    <jstl:if test="${not nextM}">
                        <spring:message code="application.status" var="status"/>
                        <display:column  property="status" title="${status}" sortable="true"/>
                    </jstl:if>
                </jstl:if>

                    <display:column >
                        <a href="tripApplication/cancel.do?tripAId=${row.id}"> <spring:message
                                code="general.cancel"/>
                        </a>
                    </display:column>
                    <display:column>
                        <a href="tripApplication/accept.do?tripAId=${row.id}"> <spring:message
                                code="tripa.accept"/>
                        </a>
                    </display:column>
                    <display:column>
                        <a href="tripApplication/deny.do?tripAId=${row.id}"> <spring:message
                                code="tripa.deny"/>
                        </a>
                    </display:column>
                    <display:column>
                        <a href="tripApplication/due.do?tripAId=${row.id}"> <spring:message
                                code="tripa.due"/>
                        </a>
                    </display:column>
            </display:table>
        </jstl:if>
    </security:authorize>
</div>

<hr>

<div id="trip-footer" class="footer">

    <div id="additonal-info-box">
        <spring:message code="trip.info" var="info2"/>
        <h2><jstl:out value="${info2}"/></h2>
        <h3><jstl:out value="${manager}"/></h3>

        <spring:message code="trip.creationDate" var="creationDate2"/>
        <h4><jstl:out value="${creationDate2}"/></h4>
        <jstl:out value="${publicationDate}"/>
    </div>

    <div id="requirments-box">
        <spring:message code="trip.requirements" var="requirements"/>
        <h2><jstl:out value="${requirements}"/></h2>

        <display:table pagesize="5" class="displaytag" keepStatus="true"
                       name="requirements" requestURI="${requestURI}" id="row">

            <spring:message code="requirement.name" var="name"/>
            <display:column property="name" title="${name}" sortable="true"/>

            <spring:message code="requirement.description" var="description"/>
            <display:column property="description" title="${description}" sortable="true"/>

        </display:table>

    </div>
    <hr>

    <div id="legalText-box">
        <spring:message code="trip.legalText" var="legalText"/>
        <h2><jstl:out value="${legalText}"/></h2>

        <c:forEach items="${legalTexts}" var="legaltext">
            <jstl:if test="${status}">
                <div id="${legaltext}-box">
                    <h2><jstl:out value="${legaltext.title}"/></h2>
                    <p>
                        <jstl:out value="${legaltext.body}"/>
                    </p>
                    <h3><jstl:out value="${legaltext.numberOfAplicableLaws}"/></h3>
                    <h4 style="color:grey;"><jstl:out value="${legaltext.registrationDate} "/></h4>

                </div>
            </jstl:if>
        </c:forEach>

    </div>
    <hr>

</div>