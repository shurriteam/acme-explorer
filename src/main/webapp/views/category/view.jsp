<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>


<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.


  --%>

<h2><jstl:out value="${name}"/></h2>

<security:authorize access="hasRole('ADMINISTRATOR')">

    <a class="button" href="/category/create.do?categoryId=${id}"><spring:message code="category.createson"/></a>
</security:authorize>

<spring:message code="category.relatedTrips" var="tripsr"/>
<h3><jstl:out value="${tripsr}"/></h3>

<display:table pagesize="5" class="displaytag" keepStatus="true"
               name="trips" requestURI="${requestURI}" id="row">


    <spring:message code="trip.ticker" var="ticker"/>
    <display:column property="ticker" title="${ticker}" sortable="true"/>

    <spring:message code="trip.title" var="title"/>
    <display:column property="title" title="${title}" sortable="true"/>

    <spring:message code="trip.description" var="description"/>
    <display:column property="description" title="${description}" sortable="true"/>

    <spring:message code="trip.startDate" var="startDate"/>
    <display:column property="startDate" title="${startDate}" sortable="true"/>

    <spring:message code="trip.endDate" var="endDate"/>
    <display:column property="endDate" title="${endDate}" sortable="true"/>

    <spring:message code="trip.publicationDate" var="publicationDate"/>
    <display:column property="publicationDate" title="${publicationDate}" sortable="true"/>

    <spring:message code="trip.isCancelled" var="cancelled"/>
    <display:column property="cancelled" title="${cancelled}" sortable="true"/>
    <spring:message code="trip.manager" var="manager"/>
    <display:column property="manager" title="${manager}" sortable="true"/>

    <spring:message code="trip.category" var="category"/>
    <display:column property="category" title="${category}" sortable="true"/>

    <security:authorize access="hasRole('ADMINISTRATOR')">

        <display:column>
            <a href="trip/view.do?tripId=${row.id}"> <spring:message
                    code="general.view"/>
            </a>
        </display:column>
    </security:authorize>


</display:table>

<spring:message code="category.sons" var="tripsr2"/>
<h3><jstl:out value="${tripsr2}"/></h3>

<display:table pagesize="20" class="displaytag" keepStatus="true"
               name="sons" requestURI="${requestURI}" id="row">


    <!-- Attributes -->
    <security:authorize access="isAnonymous()">
        <display:column>

            <a href="category/viewTrips.do?categoryId=${row.id}"> <spring:message
                    code="general.viewTrips"/>
            </a>
        </display:column>
    </security:authorize>


    <security:authorize access="hasRole('ADMINISTRATOR')">
        <display:column>
            <a href="category/edit.do?categoryId=${row.id}"> <spring:message
                    code="general.edit"/>
            </a>
        </display:column>
    </security:authorize>

    <spring:message code="category.name" var="title"/>
    <display:column property="name" title="${title}" sortable="true"/>

    <display:column>
        <a href="category/view.do?categoryId=${row.id}"> <spring:message
                code="general.view"/>
        </a>
    </display:column>

    <security:authorize access="hasRole('ADMINISTRATOR')">
        <display:column>
            <a href="category/delete.do?categoryId=${row.id}"> <spring:message
                    code="general.delete"/>
            </a>
        </display:column>


    </security:authorize>


</display:table>