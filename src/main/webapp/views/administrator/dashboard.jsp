<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<%--
  Created by IntelliJ IDEA.
  User: mruwzum
  Date: 19/12/16
  Time: 23:24
  To change this template use File | Settings | File Templates.
--%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<br>



<spring:message code="dashboard.q1" var="q1b"/>
<jstl:out value="${q1b}"/>:
<jstl:out value="${q1}"/>
<br/>
<br>
<spring:message code="dashboard.q2" var="q2b"/>
<jstl:out value="${q2b}"/>:
<jstl:out value="${q2}"/>
<br>
<br/>
<spring:message code="dashboard.q3" var="q3b"/>
<jstl:out value="${q3b}"/>:
<jstl:out value="${q3}"/>
<br>
<br/>
<spring:message code="dashboard.q4" var="q4b"/>
<jstl:out value="${q4b}"/>:
<jstl:out value="${q4}"/>
<br>
<br/>
<spring:message code="dashboard.q5" var="q5b"/>
<jstl:out value="${q5b}"/>:
<jstl:out value="${q5}"/>
<br>
<br/>
<spring:message code="dashboard.q6" var="q6b"/>
<jstl:out value="${q6b}"/>:
<jstl:out value="${q6}"/>
<br>
<br/>
<spring:message code="dashboard.q7" var="q7b"/>
<jstl:out value="${q7b}"/>:
<jstl:out value="${q7}"/>
<br>
<br/>
<spring:message code="dashboard.q8" var="q8b"/>
<jstl:out value="${q8b}"/>
<jstl:out value="${q8}"/>
<br>
<br/>
<spring:message code="dashboard.q9" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q9}"/>
<br>
<br/>
<spring:message code="dashboard.q10" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q10}"/>
<br>
<br/>
<spring:message code="dashboard.q11" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q11}"/>
<br>
<br/>
<spring:message code="dashboard.q12" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q12}"/>
<br>
<br/>
<spring:message code="dashboard.q13" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q13}"/>
<br>
<br/>
<spring:message code="dashboard.q14" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q14}"/>
<br>
<br/>
<spring:message code="dashboard.q15" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q15}"/>
<br>
<br/>
<spring:message code="dashboard.q16" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q16}"/>
<br>
<br/>
<spring:message code="dashboard.q18" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q18}"/>
<br>
<br/>
<spring:message code="dashboard.q19" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q19}"/>
<br>
<br/>
<spring:message code="dashboard.q20" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q20}"/>
<br>
<br/>
<spring:message code="dashboard.q21" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q21}"/>
<br>
<br/>
<spring:message code="dashboard.q22" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q22}"/>
<br>
<br/>
<spring:message code="dashboard.q23" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q23}"/>
<br>
<br/>
<spring:message code="dashboard.q24" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q24}"/>
<br>
<br/>
<spring:message code="dashboard.q25" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q25}"/>
<br>
<br/>
<spring:message code="dashboard.q26" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q26}"/>
<br>
<br/>
<spring:message code="dashboard.q27" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q27}"/>
<br>
<br/>



<spring:message code="dashboard.q28" var="q9b"/>
<jstl:out value="${q9b}"/>:

<display:table pagesize="5" class="displaytag" keepStatus="true"
               name="q28" requestURI="${requestURI}" id="row">


    <spring:message code="trip.ticker" var="ticker"/>
    <display:column property="ticker" title="${ticker}" sortable="true"/>

    <spring:message code="trip.title" var="title"/>
    <display:column property="title" title="${title}" sortable="true"/>

    <spring:message code="trip.description" var="description"/>
    <display:column property="description" title="${description}" sortable="true"/>

    <spring:message code="trip.startDate" var="startDate"/>
    <display:column property="startDate" title="${startDate}" sortable="true"/>

    <spring:message code="trip.endDate" var="endDate"/>
    <display:column property="endDate" title="${endDate}" sortable="true"/>

    <spring:message code="trip.publicationDate" var="publicationDate"/>
    <display:column property="publicationDate" title="${publicationDate}" sortable="true"/>

    <spring:message code="trip.manager" var="manager"/>
    <display:column property="manager" title="${manager}" sortable="true"/>

    <spring:message code="trip.category" var="category"/>
    <display:column property="category" title="${category}" sortable="true"/>


</display:table>
<br>
<br/>
<spring:message code="dashboard.q29" var="q9b"/>
<jstl:out value="${q9b}"/>:

<table>
    <c:forEach var="legal" items="${q29}">
        <tr><td><c:out value="${legal.key}"/></td> <td><c:out value="${legal.value}"/> </td></tr>
    </c:forEach>
</table

<br>
<br/>



<spring:message code="dashboard.q30" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q30}"/>
<br>
<br/>
<spring:message code="dashboard.q31" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q31}"/>
<br>
<br/>
<spring:message code="dashboard.q32" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q32}"/>
<br>
<br/>
<spring:message code="dashboard.q33" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q33}"/>
<br>
<br/>
<spring:message code="dashboard.q34" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q34}"/>
<br>
<br/>
<spring:message code="dashboard.q35" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q35}"/>
<br>
<br/>
<spring:message code="dashboard.q36" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q36}"/>
<br>
<br/>
<spring:message code="dashboard.q37" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q37}"/>
<br>
<br/>
<spring:message code="dashboard.q38" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q38}"/>
<br>
<br/>
<spring:message code="dashboard.q39" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q39}"/>
<br>
<br/>
<spring:message code="dashboard.q40" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q40}"/>
<br>
<br/>
<spring:message code="dashboard.q41" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q41}"/>
<br>
<br/>
<spring:message code="dashboard.q42" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q42}"/>
<br>
<br/>
<spring:message code="dashboard.q43" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q43}"/>
<br>
<br/>
<spring:message code="dashboard.q44" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q44}"/>
<br>
<br/>
<spring:message code="dashboard.q45" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q45}"/>
<br>
<br/>
<spring:message code="dashboard.q46" var="q9b"/>
<jstl:out value="${q9b}"/>:
<jstl:out value="${q46}"/>
<br>
<br/>


