
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>



<security:authorize access="hasRole('ADMINISTRATOR')">
    <div>
        <H5>
            <a href="tag/create.do"> <spring:message
                    code="general.create"/>
            </a>
        </H5>
    </div>
</security:authorize>
<display:table pagesize="5" class="displaytag" keepStatus="true"
               name="tags" requestURI="${requestURI}" id="row">


    <!-- Attributes -->
    <security:authorize access="isAnonymous()">
        <display:column>

            <a href="tag/view.do?tagId=${row.id}"> <spring:message
                    code="general.view"/>
            </a>
        </display:column>
    </security:authorize>


    <security:authorize access="hasRole('ADMINISTRATOR')">
        <display:column>
            <a href="tag/edit.do?tagId=${row.id}"> <spring:message
                    code="general.edit"/>
            </a>
        </display:column>
    </security:authorize>


    <security:authorize access="hasRole('ADMINISTRATOR')">
        <display:column>
            <a href="tag/delete.do?tagId=${row.id}"> <spring:message
                    code="general.delete"/>
            </a>
        </display:column>
    </security:authorize>

    <spring:message code="tag.name" var="name"/>
    <display:column property="name" title="${name}"/>



</display:table>
