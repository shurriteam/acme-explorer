<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>

<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<security:authorize access="hasRole('AUDITOR')">
    <div>
        <H5>
            <a class="radio-button" href="note/create.do"> <spring:message
                    code="general.create"/>
            </a>
        </H5>
    </div>
</security:authorize>

<!-- Listing grid -->
<display:table pagesize="5" class="displaytag" keepStatus="true"
               name="notes" requestURI="${requestURI}" id="row">
    <%--<security:authorize access="hasRole('AUDITOR')">--%>
    <%--<a class="radio-button" href="note/create.do"> <spring:message--%>
    <%--code="general.create"/>--%>
    <%--</a>--%>
    <%--security:authorize>--%>
    <!-- Attributes -->

    <security:authorize access="hasRole('MANAGER')">
        <display:column>
            <a href="note/reply.do?noteId=${row.id}"> <spring:message
                    code="general.reply"/>
            </a>
        </display:column>
    </security:authorize>
    <%--<security:authorize access="hasRole('AUDITOR')">--%>
    <%--<display:column>--%>
    <%--<a href="note/create.do"> <spring:message--%>
    <%--code="general.create"/>--%>
    <%--</a>--%>
    <%--</display:column>--%>
    <%--</security:authorize>--%>
    <spring:message code="note.creationMomment" var="creationMomment"/>
    <display:column property="creationMomment" title="${creationMomment}" sortable="true"/>
    <spring:message code="note.replyDate" var="replyDate"/>
    <display:column property="replyDate" title="${replyDate}" sortable="true"/>
    <spring:message code="note.remark" var="remark"/>
    <display:column property="remark" title="${remark}" sortable="true"/>
    <spring:message code="note.reply" var="reply"/>
    <display:column property="reply" title="${reply}" sortable="true"/>
    <spring:message code="note.owner" var="owner"/>
    <display:column property="owner.name" title="${owner}" sortable="true"/>
    <spring:message code="note.manager" var="manager"/>
    <display:column property="manager.name" title="${manager}" sortable="true"/>

</display:table>