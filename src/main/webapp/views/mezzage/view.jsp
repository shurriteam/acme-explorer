<%--
  Created by IntelliJ IDEA.
  User: kawtarchbouki
  Date: 8/12/17
  Time: 12:16
  To change this template use File | Settings | File Templates.
--%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>


<spring:message code="mezzage.senderEmail" var="senderEmail1"/>
<h3><jstl:out value="${senderEmail1}"/></h3>
<jstl:out value="${senderEmail}"/>


<spring:message code="mezzage.receiverEmail" var="receiverEmail1"/>
<h3><jstl:out value="${receiverEmail1}"/></h3>
<jstl:out value="${receiverEmail}"/>

<spring:message code="mezzage.moment" var="moment1"/>
<h3><jstl:out value="${moment1}"/></h3>
<jstl:out value="${sendmoment}"/>

<spring:message code="mezzage.sender" var="sender1"/>
<h3><jstl:out value="${sender1}"/></h3>
<jstl:out value="${sender}"/>


<spring:message code="mezzage.subject" var="subject1"/>
<h3><jstl:out value="${subject1}"/></h3>
<jstl:out value="${subject}"/>

<spring:message code="mezzage.body" var="body1"/>
<h3><jstl:out value="${body1}"/></h3>
<jstl:out value="${body}"/>


<spring:message code="mezzage.priority" var="priority1"/>
<h3><jstl:out value="${priority1}"/></h3>
<jstl:out value="${priority}"/>


<jstl:if test="${not isBroadc}">


    <spring:message code="general.movetoFolder" var="priority1"/>
    <h3><jstl:out value="${priority1}"/></h3>

    <form:form action="mezzage/movef.do" modelAttribute="mezzage">

    <form:hidden path="id"/>
    <form:hidden path="version"/>
    <form:hidden path="subject"/>
    <form:hidden path="body"/>
    <form:hidden path="senderEmail"/>
    <form:hidden path="receiverEmail"/>
    <form:hidden path="moment"/>
    <form:hidden path="sender"/>
    <form:hidden path="priority"/>
    <form:hidden path="recipient"/>



    <acme:select path="folder" code="mezzage.folder" items="${folders}" itemLabel="name"/>
    <br>

    <acme:submit name="save" code="general.movetoFolder"/>

        <a class="button" href="folder/list.do"><spring:message code="general.cancel"/></a>

    </form:form>

</jstl:if>
