<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>


<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<form:form action="curricula/saveEDU.do" modelAttribute="curricula">


    <form:hidden path="id"/>
    <form:hidden path="version"/>
    <form:hidden path="curricula"/>
    <form:hidden path="attachment"/>
    <form:hidden path="comments"/>



    <acme:textbox path="title" code="EDU.title"/>
    <acme:textbox path="startDate" code="EDU.startDate"/>
    <acme:textbox path="endDate" code="EDU.endDate"/>
    <acme:textbox path="expeditioninstitution" code="EDU.expeditioninstitution"/>


    <!---------------------------- BOTONES -------------------------->
    <input class="button2" type="submit" name="save"
           value="<spring:message code="general.save" />"/>


    <a class="button2" href="/welcome/index.do"><spring:message code="general.cancel"/></a>


</form:form>