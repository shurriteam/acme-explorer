<%--
  Created by IntelliJ IDEA.
  User: mruwzum
  Date: 22/11/16
  Time: 11:44
  To change this template use File | Settings | File Templates.
--%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>

<jstl:if test="${noc}">
    <spring:message code="curricula.notcurricula" var="select2"/>
    <h3><jstl:out value="${select2}"/></h3>
    <a class="button2" href="curricula/create.do?rangerId=${expId}"><spring:message code="curricula.cretec"/></a>
</jstl:if>
<jstl:if test="${not noc}">
    <a onclick="return confirm('<spring:message code="general.confirm.delete"/>')"
       href="curricula/delete.do?curriculaId=${cid}"> <spring:message code="general.delete"/>
    </a>

    <spring:message code="curricula.select" var="select1"/>
<h3><jstl:out value="${select1}"/></h3>

    <a class="button2" href="/curricula/editPR.do"><spring:message code="curricula.personal"/></a>
<a class="button2" href="/curricula/editEDU.do?curriculaId=${cid}"><spring:message code="curricula.education"/></a>
<a class="button2" href="/curricula/editPRO.do?curriculaId=${cid}"><spring:message code="curricula.professional"/></a>
<a class="button2" href="/curricula/editEND.do?curriculaId=${cid}"><spring:message code="curricula.endorser"/></a>
<a class="button2" href="/curricula/editMIS.do?curriculaId=${cid}"><spring:message code="curricula.misc"/></a>


    <h2><jstl:out value="${pr.name}"/></h2>
</br>
<h2><jstl:out value="${pr.photo}"/></h2>
</br>
<h2><jstl:out value="${pr.email}"/></h2>
</br>
<h2><jstl:out value="${pr.phone}"/></h2>
</br>
<h2><jstl:out value="${pr.linked}"/></h2>
</br>

    <display:table pagesize="5" class="displaytag" keepStatus="true"
                   name="PRO" requestURI="${requestURI}" id="row">

        <spring:message code="PRO.companyName" var="companyName"/>
        <display:column property="companyName" title="${companyName}" sortable="true"/>

        <spring:message code="PRO.startDate" var="startDate"/>
        <display:column property="startDate" title="${startDate}" sortable="true"/>

        <spring:message code="EDU.endDate" var="endDate"/>
        <display:column property="endDate" title="${endDate}" sortable="true"/>

        <spring:message code="PRO.rol" var="rol"/>
        <display:column property="rol" title="${rol}" sortable="true"/>


    </display:table>

    <display:table pagesize="5" class="displaytag" keepStatus="true"
                   name="EDU" requestURI="${requestURI}" id="row">

        <spring:message code="EDU.title" var="title"/>
        <display:column property="title" title="${title}" sortable="true"/>

        <spring:message code="EDU.startDate" var="startDate"/>
        <display:column property="startDate" title="${startDate}" sortable="true"/>

        <spring:message code="EDU.endDate" var="endDate"/>
        <display:column property="endDate" title="${endDate}" sortable="true"/>

        <spring:message code="EDU.expeditioninstitution" var="expeditioninstitution"/>
        <display:column property="expeditioninstitution" title="${expeditioninstitution}" sortable="true"/>


    </display:table>

    <display:table pagesize="5" class="displaytag" keepStatus="true"
                   name="END" requestURI="${requestURI}" id="row">

        <spring:message code="END.name" var="name"/>
        <display:column property="name" title="${name}" sortable="true"/>

        <spring:message code="END.email" var="email"/>
        <display:column property="email" title="${email}" sortable="true"/>

        <spring:message code="END.link" var="link"/>
        <display:column property="link" title="${link}" sortable="true"/>


    </display:table>

    <display:table pagesize="5" class="displaytag" keepStatus="true"
                   name="MIS" requestURI="${requestURI}" id="row">

        <spring:message code="EDU.title" var="title"/>
        <display:column property="title" title="${title}" sortable="true"/>


    </display:table>

</jstl:if>