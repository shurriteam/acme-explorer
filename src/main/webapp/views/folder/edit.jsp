<%--
  Created by IntelliJ IDEA.
  User: kawtarchbouki
  Date: 8/12/17
  Time: 17:17
  To change this template use File | Settings | File Templates.
--%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>


<%--
 private String name;
   private Collection<Mezzage> mezzages;
   private Actor owner;
   private Collection<Folder> sons;
  --%>

<form:form action="folder/edit.do" modelAttribute="folder">

    <form:hidden path="id"/>
    <form:hidden path="version"/>

    <acme:textbox path="name" code="folder.name"/>
    <br/>

    <!---------------------------- BOTONES -------------------------->

    <acme:submit name="save" code="general.save"/>

    <a class="button" href="/folder/list.do"><spring:message code="general.cancel"/></a>


</form:form>