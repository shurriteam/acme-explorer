<%--
  Created by IntelliJ IDEA.
  User: kawtarchbouki
  Date: 8/12/17
  Time: 17:18
  To change this template use File | Settings | File Templates.




    private String name;
   private Collection<Mezzage> mezzages;
   private Actor owner;
   private Collection<Folder> sons;




--%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>

<%--
  --%>

<security:authorize access="isAuthenticated()">
    <div>
        <H3>
            <a class="button" href="mezzage/create.do"> <spring:message
                    code="message.send"/>
            </a>
        </H3>
    </div>
</security:authorize>
<security:authorize access="isAuthenticated()">
    <div>
        <H5>
            <a href="folder/create.do"> <spring:message
                    code="general.create"/>
            </a>
        </H5>
    </div>
</security:authorize>

<security:authorize access="hasRole('ADMINISTRATOR')">
    <div>
        <H5>
            <a href="administrator/createBM.do"> <spring:message
                    code="master.page.broadcast"/>
            </a>
        </H5>
    </div>
</security:authorize>

<!-- Listing grid -->
<display:table pagesize="5" class="displaytag" keepStatus="true"
               name="folders" requestURI="${requestURI}" id="row">


    <!-- Attributes -->

    <security:authorize access="isAuthenticated()">
        <display:column>
            <a href="folder/edit.do?folderId=${row.id}"> <spring:message
                    code="general.edit"/>
            </a>
        </display:column>
        <display:column>
            <a href="folder/view.do?folderId=${row.id}"> <spring:message
                    code="general.view"/>
            </a>
        </display:column>
        <display:column>
            <a href="folder/delete.do?folderId=${row.id}"> <spring:message
                    code="general.delete"/>
            </a>
        </display:column>
    </security:authorize>

    <spring:message code="folder.name" var="title"/>
    <display:column property="name" title="${title}" sortable="true"/>

</display:table>

