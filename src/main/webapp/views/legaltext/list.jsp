<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<security:authorize access="hasRole('ADMINISTRATOR')">
    <div>
        <H5>
            <a href="legaltext/create.do"> <spring:message
                    code="general.create"/>
            </a>
        </H5>
    </div>
</security:authorize>

<!-- Listing grid -->


<display:table pagesize="20" class="displaytag" keepStatus="true"
               name="legaltexts" requestURI="${requestURI}" id="row">


    <!-- Attributes -->
    <security:authorize access="isAnonymous()">
        <display:column>

            <a href="legaltext/view.do?legaltextId=${row.id}"> <spring:message
                    code="general.view"/>
            </a>
        </display:column>
    </security:authorize>

    <security:authorize access="hasRole('ADMINISTRATOR')">
        <display:column>
            <jstl:if test="${row.status == 'DRAFT'}">


                <a href="legaltext/publish.do?legalTextId=${row.id}"> <spring:message
                        code="general.publish"/>
                </a>

            </jstl:if>
        </display:column>
    </security:authorize>


    <security:authorize access="hasRole('ADMINISTRATOR')">
        <display:column>
            <a href="legaltext/edit.do?legaltextId=${row.id}"> <spring:message
                    code="general.edit"/>
            </a>
        </display:column>
    </security:authorize>

    <spring:message code="legaltext.title" var="title1"/>
    <display:column property="title" title="${title1}" sortable="true"/>

    <spring:message code="legaltext.numberOfAplicableLaws" var="numberOfAplicableLaws1"/>
    <display:column property="numberOfAplicableLaws" title="${numberOfAplicableLaws1}" sortable="true"/>

    <spring:message code="legaltext.registrationDate" var="registrationDate1"/>
    <display:column property="registrationDate" title="${registrationDate1}" sortable="true"/>


    <security:authorize access="hasRole('ADMINISTRATOR')">
        <display:column>
            <a href="legaltext/delete.do?legalTextId=${row.id}"> <spring:message
                    code="general.delete"/>
            </a>
        </display:column>
        <display:column>
            <jstl:out value="${row.status}"/>
        </display:column>
    </security:authorize>
</display:table>