<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>

<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>


<!-- Listing grid -->
<display:table pagesize="5" class="displaytag" keepStatus="true"
               name="tripApplications" requestURI="${requestURI}" id="row">

    <!-- Attributes -->
    <security:authorize access="hasRole('MANAGER')">
        <display:column>
            <a href="tripApplication/editStatus.do?tripApplicationId=${row.id}"> <spring:message
                    code="general.editStatus"/>
            </a>

        </display:column>
    </security:authorize>

    <security:authorize access="hasRole('EXPLORER')">
        <display:column>
            <a href="trip/viewFromTripApp.do?tripApplicationId=${row.id}"> <spring:message
                    code="general.view"/>
            </a>

        </display:column>
    </security:authorize>




    <spring:message code="application.trip" var="trip"/>
    <display:column property="trip" title="${trip}" sortable="true"/>


    <spring:message code="application.creationDate" var="creationDate"/>
    <display:column property="creationDate" title="${creationDate}" sortable="true"/>

    <spring:message code="application.status" var="status"/>
    <display:column property="status" title="${status}" sortable="true"/>

    <%--<spring:message code="application.isCancelled" var="isCancelled"/>--%>
    <%--<display:column property="isCancelled" title="${isCancelled}" sortable="true"/>--%>

    <%--<spring:message code="application.cancellReason" var="cancellReason"/>--%>
    <%--<display:column property="cancellReason" title="${cancellReason}" sortable="true"/>--%>

    <spring:message code="application.owner" var="owner"/>
    <display:column property="owner" title="${owner}" sortable="true"/>



</display:table>