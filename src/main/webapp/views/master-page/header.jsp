<%--
 * header.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<div>
    <a href="welcome/index.do"> <img src="images/logo.png" alt="Acme Inc."/></a>
</div>

<div>
	<ul id="jMenu">
		<%--TODO: FAV ICON--%>
		<!-- Do not forget the "fNiv" class for the first level links !! -->


		<security:authorize access="hasRole('ADMINISTRATOR')&&!hasRole('BAN')">
            <li><a class="fNiv" href="administrator/listsuspmanagerandrangers.do"><spring:message
                    code="master.admin.listsuspmanagerandrangers"/></a></li>
			<li><a class="fNiv" href="ranger/create.do"><spring:message code="master.page.createRanger"/></a></li>
			<li><a class="fNiv" href="manageer/create.do"><spring:message code="master.page.createManager"/></a></li>
			<li><a class="fNiv" href="legaltext/list.do"><spring:message code="master.page.legalText"/></a></li>
			<li><a class="fNiv" href="tag/list.do"><spring:message code="master.page.tag"/></a></li>
			<li><a class="fNiv" href="administrator/dashboard.do"><spring:message code="master.page.dashboard"/></a></li>

			<li><a class="fNiv"><spring:message code="master.page.administrator"/></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="actor/editAdministrator.do"><spring:message code="master.page.editprofile"/> </a></li>
				</ul>
			</li>
			<li><a href="actor/list.do"><spring:message code="master.page.actors"/></a></li>

			<li><a class="fNiv"><spring:message code="master.page.administrator.utils"/></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="administrator/changeCache.do"><spring:message code="master.page.administrator.utils.cache"/> </a></li>
                    <li><a href="administrator/changeNumber.do"><spring:message code="master.page.administrator.utils.number"/> </a></li>
                    <li><a href="administrator/changeVAT.do"><spring:message code="master.page.administrator.utils.vat"/> </a></li>
                    <li><a href="administrator/changeBanner.do"><spring:message
                            code="master.page.administrator.utils.banner"/> </a></li>
                </ul>
			</li>
		</security:authorize>


			<security:authorize access="hasRole('SPONSOR')&&!hasRole('BAN')">
				<li><a class="fNiv"><spring:message code="master.page.sponsor"/></a>
					<ul>
						<li class="arrow"></li>
						<li><a href="actor/editSponsor.do"><spring:message code="master.page.editprofile"/> </a></li>
						</li>

					</ul>
				</li>
			</security:authorize>


            <security:authorize access="hasRole('AUDITOR')&&!hasRole('BAN')">
                <li><a class="fNiv" href="note/listAuditor.do"><spring:message code="master.page.WrittenNotes"/> </a>
                </li>
                </li>
                <li><a href="audit/list.do"><spring:message code="master.page.audits"/> </a></li>

                <li><a class="fNiv"><spring:message code="master.page.auditor"/></a>
                    <ul>
                        <li class="arrow"></li>
                        <li><a href="actor/editAuditor.do"><spring:message code="master.page.editprofile"/> </a></li>
                        </li>

                    </ul>
                </li>
            </security:authorize>

		<security:authorize access="hasRole('EXPLORER')&&!hasRole('BAN')">
			<li><a class="fNiv"><spring:message	code="master.page.explorer" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="actor/editExplorer.do"><spring:message code="master.page.editprofile"/> </a></li>
					<li><a href="tripApplication/listMyTripApplications.do"><spring:message
							code="master.page.explorer.listTripApp"/></a></li>
					<li><a href="creditCard/edit.do"><spring:message code="master.page.explorer.editCreditCard"/></a>
					</li>

				</ul>
			</li>
		</security:authorize>
            <security:authorize access="hasRole('RANGER')&&!hasRole('BAN')">
                <li><a class="fNiv"><spring:message	code="master.page.ranger" /></a>
                    <ul>
                        <li class="arrow"></li>
						<li><a href="actor/editRanger.do"><spring:message code="master.page.editprofile"/> </a></li>
                        <li><a href="curricula/edit.do"><spring:message code="master.page.editCurricula"/> </a></li>

                    </ul>
                </li>
            </security:authorize>
            <security:authorize access="hasRole('MANAGER')&&!hasRole('BAN')">

                <li><a class="fNiv"><spring:message	code="master.page.manager" /></a>
                    <ul>
                        <li class="arrow"></li>
						<li><a href="actor/editManager.do"><spring:message code="master.page.editprofile"/> </a></li>
                        <li><a href="trip/myTrips.do"><spring:message code="master.page.Mytrips" /></a></li>
                        <li><a href="survivalClass/listManager.do"><spring:message
                                code="master.page.survivalClass"/></a></li>
                        <li><a href="tripApplication/listMy.do"><spring:message
                                code="master.page.MytripsApplications"/></a></li>
                        <li><a href="note/list.do"><spring:message code="master.page.WrittenNotes"/></a></li>




					</ul>
                </li>
            </security:authorize>
		<security:authorize access="isAnonymous()">
			<li><a class="fNiv" href="security/login.do"><spring:message code="master.page.login" /></a></li>
			<li><a class="fNiv" href="explorer/create.do"><spring:message code="master.page.explorersigin" /></a></li>
			<li><a class="fNiv" href="ranger/create.do"><spring:message code="master.page.rangersignin" /></a></li>
			<li><a class="fNiv" href="category/list.do"><spring:message code="master.page.categories" /></a></li>
            <li><a class="fNiv" href="trip/listShowables.do"><spring:message code="master.page.trips" /></a></li>
            <%--DAVID - ENLACE A SEARCH--%>
			<li><a class="fNiv" href="searcher/createna.do"><spring:message code="master.page.searcher"/></a></li>




		</security:authorize>
		
		<security:authorize access="isAuthenticated()&&!hasRole('BAN')">
            <li><a class="fNiv" href="category/list.do"><spring:message code="master.page.categories" /></a></li>
            <li><a class="fNiv" href="trip/listShowables.do"><spring:message code="master.page.trips" /></a></li>
            <li><a class="fNiv" href="searcher/create.do"><spring:message code="master.page.searcher"/></a></li>
            <li><a href="folder/list.do"><spring:message
					code="master.page.mezzages"/></a></li>

		</security:authorize>

			<security:authorize access="isAuthenticated()">
				<li>
					<a class="fNiv">
						<spring:message code="master.page.profile" />
						(<security:authentication property="principal.username" />)
					</a>

					<ul>
						<li class="arrow"></li>
						<li><a href="j_spring_security_logout"><spring:message code="master.page.logout" /> </a></li>
					</ul>
				</li>
			</security:authorize>

	</ul>
</div>

<div>
	<a href="?language=en">en</a> | <a href="?language=es">es</a>
</div>

