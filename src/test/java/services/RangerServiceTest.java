package services;

import domain.Actor;
import domain.Administrator;
import domain.Curricula;
import domain.Ranger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.w3c.dom.ranges.Range;
import security.Authority;
import security.UserAccount;
import utilities.AbstractTest;

import javax.transaction.Transactional;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/datasource.xml",
        "classpath:spring/config/packages.xml"})
@Transactional
public class RangerServiceTest extends AbstractTest {
   @Autowired
   private RangerService rangerService;
   @Autowired
   private CurriculaService curriculaService;
   @Autowired
   private ActorService actorService;
   @Autowired
   private FolderService folderService;



   @Test
   public void registerAsRanger() throws Exception {
      Ranger administrator = rangerService.create();
      administrator.setName("perri");
      administrator.setEmail("perri@gmail.com");
      administrator.setAddress("calle de perri 23");
      administrator.setPhoneNumber("+34567345234");
      administrator.setSurname("el perro");
      UserAccount userAcco = new UserAccount();
      userAcco.setPassword("passwd");
      userAcco.setUsername("perriel");
      Set<Authority> auth = new HashSet<>();
      Authority authority = new Authority();
      authority.setAuthority("ADMINISTRATOR");
      auth.add(authority);
      userAcco.setAuthorities(auth);
      administrator.setUserAccount(userAcco);

      Actor res =  rangerService.registerAsRanger(administrator);
      Assert.assertNotNull("resultado nulo al registrar el ranger",res);
   }

   @Test
   public void getCurriculaByRangerId() throws Exception {
      authenticate("ranger1");
      Curricula curricula = rangerService.getCurriculaByRangerId(actorService.findByPrincipal().getId());
      Assert.assertNotNull(curricula);
   }

   @Test
   public void modifyCurriculaofRanger() throws Exception {
      authenticate("ranger1");
      Curricula curricula = rangerService.getCurriculaByRangerId(actorService.findByPrincipal().getId());
      Assert.assertNotNull(curricula);
      boolean res = rangerService.modifyCurriculaofRanger(curricula,rangerService.findByPrincipal());
      org.springframework.util.Assert.isTrue(res);
   }

   @Test
   public void deleteCurriculaofRanger() throws Exception {
      authenticate("ranger1");
      curriculaService.save(rangerService.findByPrincipal().getCurricula());
      boolean res = rangerService.deleteCurriculaofRanger(rangerService.findByPrincipal());

   }

}