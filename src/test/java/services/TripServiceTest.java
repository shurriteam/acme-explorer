package services;

import domain.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;
import utilities.AbstractTest;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/datasource.xml",
        "classpath:spring/config/packages.xml"})
@Transactional
public class TripServiceTest extends AbstractTest {

   @Autowired
   private TripService tripService;
   @Autowired
   private ActorService actorService;
   @Autowired
   private RangerService rangerService;
   @Autowired
   private CurriculaService curriculaService;
   @Autowired
   private AuditService auditService;
   @Autowired
   private NoteService noteService;
   @Autowired
   private CategoryService categoryService;
   @Autowired
   private FolderService folderService;
   @Autowired
   private ExplorerService explorerService;
   @Autowired
   private ManagerService managerService;




   @Test
   public void displayRangersCurriculumFromTrip() throws Exception {

      authenticate("ranger3");
      Ranger ranger = rangerService.findByPrincipal();
      Trip t = new ArrayList<>(ranger.getTripsToGuide()).get(0);
      Curricula res = tripService.displayRangersCurriculumFromTrip(t.getId());
      Assert.notNull(res);


   }

   @Test
   public void displayAuditsFromTrip() throws Exception {
      authenticate("ranger3");
      Ranger ranger = rangerService.findByPrincipal();
      Trip t = new ArrayList<>(ranger.getTripsToGuide()).get(0);
      Collection<Audit> audits = tripService.displayAuditsFromTrip(t.getId());
      Assert.notEmpty(audits);
   }

   @Test
   public void getNotesByTrip() throws Exception {
      authenticate("ranger3");
      Ranger ranger = rangerService.findByPrincipal();
      Trip t = new ArrayList<>(ranger.getTripsToGuide()).get(0);
      Collection<Note> notes = tripService.getNotesByTrip(t.getId());
      Assert.notEmpty(notes);

   }

   @Test
   public void tripsByCategory() throws Exception {

      Category category = new ArrayList<>(categoryService.findAll()).get(0);

     Collection<Trip> trips =  tripService.tripsByCategory(category);
      Assert.notEmpty(trips);

   }



   }


