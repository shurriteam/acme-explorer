package services;

import domain.CreditCard;
import domain.Mezzage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;
import utilities.AbstractTest;

import javax.transaction.Transactional;

import java.util.ArrayList;

import static org.junit.Assert.*;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/datasource.xml",
        "classpath:spring/config/packages.xml"})
@Transactional
public class CreditCardServiceTest extends AbstractTest {
   @Autowired
   private CreditCardService creditCardService;
   @Autowired
   private ActorService actorService;
   @Autowired
   private ExplorerService explorerService;
   @Autowired
   private SponsorService sponsorService;
   @Autowired
   private MezzageService mezzageService;
   @Autowired
   private ManagerService managerService;


   @Test
   public void checkCreditCard() throws Exception {
      CreditCard creditCard = new ArrayList<>(creditCardService.findAll()).get(0);
      boolean res  = creditCardService.checkCreditCard(creditCard);
      Assert.isTrue(res);
   }

}