package services;

import domain.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import security.Authority;
import security.UserAccount;
import utilities.AbstractTest;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/datasource.xml",
        "classpath:spring/config/packages.xml"})
@Transactional


public class AdministratorServiceTest extends AbstractTest {

  @Autowired
  private AdministratorService administratorService;
  @Autowired
  private ActorService actorService;
  @Autowired
  private FolderService folderService;
  @Autowired
  private ManagerService managerService;
  @Autowired
  private SponsorService sponsorService;
  @Autowired
  private RangerService rangerService;
  @Autowired
  private TagService tagService;
  @Autowired
  private LegalTextService legalTextService;
  @Autowired
  private TripService tripService;
  @Autowired
  private CategoryService categoryService;
  @Autowired
  private MezzageService mezzageService;



   @Test
   public void registerAsAdministrator() {
      Administrator administrator = administratorService.create();
      administrator.setName("perri");
      administrator.setEmail("perri@gmail.com");
      administrator.setAddress("calle de perri 23");
      administrator.setPhoneNumber("+34567345234");
      administrator.setSurname("el perro");
      UserAccount userAcco = new UserAccount();
      userAcco.setPassword("passwd");
      userAcco.setUsername("perriel");
      Set<Authority> auth = new HashSet<>();
      Authority authority = new Authority();
      authority.setAuthority("ADMINISTRATOR");
      auth.add(authority);
      userAcco.setAuthorities(auth);
      administrator.setUserAccount(userAcco);

     Actor res =  administratorService.registerAsAdministrator(administrator);
      Assert.assertNotNull("resultado nulo al registrar el administrador",res);
   }

   @Test
   public void suspiciousManagers() {
      //TODO esperar a arreglar querys
   }

   @Test
   public void suspciousRangers() {
      //TODO esperar a arreglar querys
   }

   @Test
   public void banActor() {
      authenticate("administrator1");
      Actor a = new ArrayList<>(actorService.findAll()).get(0);
      Assert.assertNotNull(a);
      administratorService.banActor(a);
      Authority autor = new Authority();
      autor.setAuthority("BAN");
      Assert.assertTrue(a.getUserAccount().getAuthorities().contains(autor));
      unauthenticate();
   }

   @Test()
   public void unbanActor() {
      authenticate("administrator1");
      Actor a = new ArrayList<>(actorService.findAll()).get(0);
      Assert.assertNotNull(a);
      administratorService.banActor(a);
      Authority autor = new Authority();
      autor.setAuthority("BAN");
      Assert.assertTrue(a.getUserAccount().getAuthorities().contains(autor));
      administratorService.unbanActor(a);
      unauthenticate();
   }

   @Test
   public void createLegalText() {
      authenticate("administrator1");
     LegalText legalText =  administratorService.createLegalText();
     Assert.assertNotNull(legalText);
   }

    @Test(expected = NullPointerException.class)
   public void editLegalText() {
      authenticate("administrator1");
      LegalText legalText =  administratorService.createLegalText();
      legalText.setStatus(null);
      legalTextService.save(legalText);
      boolean res = administratorService.editLegalText(legalText);
      Assert.assertTrue(!res);
   }

    @Test(expected = IllegalArgumentException.class)
   public void deletLegalText() {
      authenticate("administrator1");
      LegalText legalText =  administratorService.createLegalText();
      legalText.setStatus(LegalStatus.DRAFT);
      legalTextService.save(legalText);
      boolean res = administratorService.deletLegalText(legalText);
      Assert.assertTrue(!res);
   }

   @Test
   public void createTag() {
      authenticate("administrator1");
      Tag tag = tagService.create();
      tag.setName("perri");
      tag.setTrip(tripService.findAll());
      org.springframework.util.Assert.notEmpty(tag.getTrip());
   }

   @Test
   public void editTags() {
      authenticate("administrator1");
      Tag tag = tagService.create();
      tag.setName("perri");
      tag.setTrip(tripService.findAll());
      org.springframework.util.Assert.notEmpty(tag.getTrip());
      tagService.save(tag);
      boolean res =administratorService.editTags(tag);
      org.springframework.util.Assert.isTrue(res);
   }

   @Test
   public void createCategory() {
      authenticate("administrator1");
      Category category =administratorService.createCategory();
      Assert.assertNotNull(category);
   }

   @Test
   public void editCategory() {
      authenticate("administrator1");
      Category category =administratorService.createCategory();
      Assert.assertNotNull(category);
      categoryService.save(category);
      boolean res = administratorService.editCategory(category);
       Assert.assertTrue(res);
   }

   @Test
   public void deleteCategory() {
      authenticate("administrator1");
      Category category =administratorService.createCategory();
      Assert.assertNotNull(category);
     Category category1 =  categoryService.save(category);
      boolean res = administratorService.deleteCategory(category1);
       Assert.assertTrue(res);
   }

   @Test
   public void storeBroadCastMessage() {
      authenticate("administrator1");
      Mezzage m = mezzageService.create();
      m.setBody("perroi");
      m.setSender(actorService.findByPrincipal());
      m.setSenderEmail(actorService.findByPrincipal().getEmail());
     boolean res =  administratorService.storeBroadCastMessage(m);
       Assert.assertTrue(res);
   }

}