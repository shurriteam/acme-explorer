package services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import utilities.AbstractTest;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/datasource.xml",
        "classpath:spring/config/packages.xml"})
@Transactional


public class ActorServiceTest extends AbstractTest {



   @Autowired
   private MezzageService mezzageService;
   @Autowired
   private ActorService actorService;
   @Autowired
   private FolderService folderService;
   @Autowired
   private AdministratorService administratorService;

   @Autowired
   private SearchService searchService;
   @Autowired
   private TripService tripService;

   @Test
   public void updateBroacastMezzages() {
      boolean res = false;
      authenticate("manager1");

      res = actorService.updateBroacastMezzages();
      Assert.assertTrue(!res);

   }


   @Test
   public void testmap() {


      System.out.println(administratorService.numberOfTimesEachLegalTextHasBeenReferenced());
   }

}