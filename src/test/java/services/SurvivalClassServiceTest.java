package services;

import domain.Explorer;
import domain.Story;
import domain.SurvivalClass;
import domain.Trip;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import utilities.AbstractTest;

import javax.transaction.Transactional;

import java.util.ArrayList;

import static org.junit.Assert.*;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/datasource.xml",
        "classpath:spring/config/packages.xml"})
@Transactional
public class SurvivalClassServiceTest extends AbstractTest {

   @Autowired
   private SurvivalClassService survivalClassService;
   @Autowired
   private ManagerService managerService;
   @Autowired
   private ExplorerService explorerService;
   @Autowired
   private TripService tripService;
   @Autowired
   private StoryService storyService;

   @Test
   public void enrollSurvivalClass() throws Exception {
      authenticate("explorer1");
      SurvivalClass surviv = new ArrayList<>(survivalClassService.findAll()).get(0);
   boolean res = survivalClassService.enrollSurvivalClass(surviv.getId(),explorerService.findByPrincipal().getId());
      org.springframework.util.Assert.isTrue(!res);

   }

   @Test
   public void writeAnStoryInAnyTrip() throws Exception {
      authenticate("explorer1");
      Trip trip = new ArrayList<>(tripService.findAll()).get(1);
      Story story = storyService.create();
      story.setExplorer(explorerService.findByPrincipal());
      story.setText("dsdfs");
      story.setTitle("sdfsdf");
      storyService.save(story);
     boolean res =  survivalClassService.writeAnStoryInAnyTrip(story,trip.getId(),explorerService.findByPrincipal().getId());
      org.springframework.util.Assert.isTrue(!res);

   }

}