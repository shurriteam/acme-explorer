package services;

import domain.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import security.Authority;
import security.UserAccount;
import utilities.AbstractTest;

import javax.transaction.Transactional;

import java.util.*;

import static org.junit.Assert.*;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/datasource.xml",
        "classpath:spring/config/packages.xml"})
@Transactional
public class AuditorServiceTest extends AbstractTest {

   @Autowired
   private AuditorService auditorService;
    @Autowired
    private ActorService actorService;
    @Autowired
    private TripService tripService;
    @Autowired
    private NoteService noteService;
    @Autowired
    private AuditService auditService;
   @Test
   public void registerAsAuditor() throws Exception {
      Auditor  auditor = auditorService.create();

      auditor.setName("perri");
      auditor.setEmail("perri@gmail.com");
      auditor.setAddress("calle de perri 23");
      auditor.setPhoneNumber("+34567345234");
      auditor.setSurname("el perro");
      UserAccount userAcco = new UserAccount();
      userAcco.setPassword("passwd");
      userAcco.setUsername("perriel");
      Set<Authority> auth = new HashSet<>();
      Authority authority = new Authority();
      authority.setAuthority("AUDITOR");
      auth.add(authority);
      userAcco.setAuthorities(auth);
      auditor.setUserAccount(userAcco);

      Actor res =  auditorService.registerAsAuditor(auditor);
      Assert.assertNotNull("resultado nulo al registrar el auditor",res);
   }




   @Test
   public void getNotesByAuditor() throws Exception {

       authenticate("auditor1");
       Collection<Note> n =new ArrayList<>(auditorService.getNotesByAuditor(actorService.findByPrincipal().getId()));
       Assert.assertNotNull(n);




   }

   @Test
   public void writeNoteForTrip() throws Exception {

       Trip trip = new ArrayList<>(tripService.findAll()).get(0);

       Note note = noteService.create();
       Collection<Trip> trips = new ArrayList<>();
       trips.add(trip);
       note.setTrips(trips);
       boolean res = auditorService.writeNoteForTrip(note, trip);
       org.springframework.util.Assert.isTrue(res);

   }

   @Test
   public void getAuditionsByAuditor() throws Exception {
       authenticate("auditor2");
       Collection<Audit> a= new ArrayList<>(auditorService.getAuditionsByAuditor(actorService.findByPrincipal().getId()));
       Assert.assertNotNull(a);
   }

   @Test
   public void writeAuditForTrip() throws Exception {
       Trip trip = new ArrayList<>(tripService.findAll()).get(0);

       Audit audit = auditService.create();
       Collection<Trip> trips = new ArrayList<>();
       trips.add(trip);
       audit.setTrip(trip);
       boolean res = auditorService.writeAuditForTrip(audit,trip);
       org.springframework.util.Assert.isTrue(res);

   }

   @Test
   public void modifyAudit() throws Exception {

       authenticate("auditor1");
      Audit audit = new ArrayList<>(auditorService.findByPrincipal().getAudits()).get(0);
      audit.setAuditStatus(AuditStatus.DRAFT);
       boolean res= auditorService.modifyAudit(audit,auditorService.findByPrincipal().getId());
       org.springframework.util.Assert.isTrue(res);


   }

   @Test
   public void deleteAudit() throws Exception {
       authenticate("auditor1");
       List<Audit> audit=new ArrayList<>(auditorService.findByPrincipal().getAudits());
       Audit audit1 = audit.get(0);
       audit1.setAuditStatus(AuditStatus.DRAFT);
       auditService.save(audit1);
       Trip trip = audit1.getTrip();
       Assert.assertTrue(auditorService.deleteAudit(audit1,trip));



   }

}