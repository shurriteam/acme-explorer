package services;

import domain.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import security.Authority;
import security.UserAccount;
import utilities.AbstractTest;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/datasource.xml",
        "classpath:spring/config/packages.xml"})
@Transactional
public class ManagerServiceTest extends AbstractTest {

   @Autowired
   private CreditCardService creditCardService;
   @Autowired
   private ActorService actorService;
   @Autowired
   private ExplorerService explorerService;
   @Autowired
   private SponsorService sponsorService;
   @Autowired
   private MezzageService mezzageService;
   @Autowired
   private ManagerService managerService;
   @Autowired
   private TripService tripService;
   @Autowired
   private TripApplicationService tripApplicationService;
   @Autowired
   private NoteService noteService;


   @Test
   public void noteReply() throws Exception {
      authenticate("manager1");
      Note note = noteService.create();
      note.setManager(managerService.findByPrincipal());
      note.setRemark("");
      Note note1 =  noteService.save(note);
      managerService.noteReply(note1.getId(),"reply");
   }

   @Test
   public void registerAsMAnager() throws Exception {
      Manager administrator = managerService.create();
      administrator.setName("perri");
      administrator.setEmail("perri@gmail.com");
      administrator.setAddress("calle de perri 23");
      administrator.setPhoneNumber("+34567345234");
      administrator.setSurname("el perro");
      UserAccount userAcco = new UserAccount();
      userAcco.setPassword("passwd");
      userAcco.setUsername("perriel");
      Set<Authority> auth = new HashSet<>();
      Authority authority = new Authority();
      authority.setAuthority("MANAGER");
      auth.add(authority);
      userAcco.setAuthorities(auth);
      administrator.setUserAccount(userAcco);

      Actor res =  managerService.registerAsMAnager(administrator);
      Assert.assertNotNull("resultado nulo al registrar el manager",res);

   }

   @Test
   public void editTrip() throws Exception {
      authenticate("manager1");
      Trip trip = new ArrayList<>(tripService.findAll()).get(0);
      boolean res = managerService.editTrip(trip);
      org.springframework.util.Assert.isTrue(res);
   }

   @Test(expected = IllegalArgumentException.class)
   public void cancelTripOrDeleteTrip() throws Exception {
      authenticate("manager1");
      Trip trip = new ArrayList<>(tripService.findAll()).get(0);
      tripService.save(trip);
      boolean res = managerService.cancelTripOrDeleteTrip(trip);
      org.springframework.util.Assert.isTrue(res);
   }

   @Test
   public void editMyTrip() throws Exception {
         authenticate("manager1");
         TripApplication tripApp = new ArrayList<>(tripApplicationService.findAll()).get(0);
         boolean res = managerService.editMyTrip(tripApp);
         org.springframework.util.Assert.isTrue(res);
   }

   @Test
   public void createSurvivalClass() throws Exception {
      authenticate("manager1");
     SurvivalClass survivalClass =  managerService.createSurvivalClass();
      Assert.assertNotNull(survivalClass);
   }

   @Test
   public void editSurvivalClass() throws Exception {
      authenticate("manager1");

      Trip t = new ArrayList<>(managerService.findByPrincipal().getOrganizedTrips()).get(0);
      SurvivalClass survivalClass = new ArrayList<>(t.getSurvivalClasses()).get(0);
      boolean res = managerService.editSurvivalClass(survivalClass);
      org.springframework.util.Assert.isTrue(res);
   }

   @Test
   public void deletSurvivalClass() throws Exception {
      authenticate("manager1");
      Trip t = new ArrayList<>(managerService.findByPrincipal().getOrganizedTrips()).get(0);
      SurvivalClass survivalClass = new ArrayList<>(t.getSurvivalClasses()).get(0);
      boolean res = managerService.deletSurvivalClass(survivalClass);
      org.springframework.util.Assert.isTrue(res);
   }

   @Test
   public void mispuņeterostrips()   {
      authenticate("manager2");

      Collection<Trip> mytrips = managerService.findByPrincipal().getOrganizedTrips();
      System.out.println(mytrips);
      org.springframework.util.Assert.notEmpty(mytrips);
   }
}