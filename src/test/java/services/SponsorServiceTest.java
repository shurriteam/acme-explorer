package services;

import domain.Actor;
import domain.Administrator;
import domain.Sponsor;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import security.Authority;
import security.UserAccount;
import utilities.AbstractTest;

import javax.transaction.Transactional;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/datasource.xml",
        "classpath:spring/config/packages.xml"})
@Transactional
public class SponsorServiceTest extends AbstractTest {

   @Autowired
   private CreditCardService creditCardService;
   @Autowired
   private ActorService actorService;
   @Autowired
   private ExplorerService explorerService;
   @Autowired
   private SponsorService sponsorService;
   @Autowired
   private MezzageService mezzageService;
   @Autowired
   private ManagerService managerService;


   @Test
   public void registerAsSponsor() throws Exception {
      Sponsor administrator = sponsorService.create();
      administrator.setName("perri");
      administrator.setEmail("perri@gmail.com");
      administrator.setAddress("calle de perri 23");
      administrator.setPhoneNumber("+34567345234");
      administrator.setSurname("el perro");
      UserAccount userAcco = new UserAccount();
      userAcco.setPassword("passwd");
      userAcco.setUsername("perriel");
      Set<Authority> auth = new HashSet<>();
      Authority authority = new Authority();
      authority.setAuthority("SPONSOR");
      auth.add(authority);
      userAcco.setAuthorities(auth);
      administrator.setUserAccount(userAcco);

      Actor res =  sponsorService.registerAsSponsor(administrator);
      Assert.assertNotNull("resultado nulo al registrar el sponsor",res);
   }

}