package services;

import domain.Folder;
import domain.Mezzage;
import domain.Priority;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;
import utilities.AbstractTest;

import javax.transaction.Transactional;

import java.util.Collection;

import static org.junit.Assert.*;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/datasource.xml",
        "classpath:spring/config/packages.xml"})
@Transactional
public class MezzageServiceTest extends AbstractTest {
   @Autowired
   private MezzageService mezzageService;
   @Autowired
   private FolderService folderService;
   @Autowired
   private ActorService actorService;
   @Autowired
   private AdministratorService administratorService;



   @Test
   public void getFolders() throws Exception {
      authenticate("explorer1");
     Collection<Folder> folders = mezzageService.getFolders();
      Assert.notEmpty(folders);
   }

   @Test
   public void modifyFolder() throws Exception {
      authenticate("explorer1");
      Folder f = folderService.create();
      f.setOwner(actorService.findByPrincipal());
      f.setName("perri");
      mezzageService.modifyFolder(f);
   }

   @Test
   public void deleteFolder() throws Exception {
      authenticate("explorer1");
      Folder f = folderService.create();
      f.setOwner(actorService.findByPrincipal());
      f.setName("pa borrar");
      mezzageService.deleteFolder(f);
   }

   @Test
   public void recieveMessage() throws Exception {
      authenticate("explorer1");
      Mezzage message = mezzageService.create();
      message.setReceiverEmail(actorService.findByPrincipal().getEmail());
      message.setRecipient(actorService.findByPrincipal());
      message.setBody("rasfsd");
      message.setSubject("test");
      message.setPriority(Priority.NEUTRAL);
      mezzageService.save(message);
      mezzageService.recieveMessage(message,actorService.findByPrincipal());
   }

   @Test
   public void textMessage() throws Exception {
      authenticate("explorer1");
      Mezzage message = mezzageService.create();
      message.setReceiverEmail(actorService.findByPrincipal().getEmail());
      message.setRecipient(actorService.findByPrincipal());
      message.setBody("rasfsd");
      message.setSubject("test");
      message.setPriority(Priority.NEUTRAL);
      mezzageService.save(message);
      mezzageService.textMessage(message);
   }

   @Test
   public void allMessages() throws Exception {
      authenticate("explorer1");
      Collection<Mezzage> mezzages = mezzageService.allMessages(actorService.findByPrincipal());
      Assert.notEmpty(mezzages);
   }

   @Test
   public void folderByName() throws Exception {
      authenticate("explorer1");
      Folder f = mezzageService.folderByName(actorService.findByPrincipal(),"inbox");
      org.junit.Assert.assertNotNull(f);
   }

}