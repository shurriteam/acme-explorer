package services;

import domain.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import security.Authority;
import security.UserAccount;
import utilities.AbstractTest;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/datasource.xml",
        "classpath:spring/config/packages.xml"})
@Transactional
public class ExplorerServiceTest extends AbstractTest {

   @Autowired
   private CreditCardService creditCardService;
   @Autowired
   private ActorService actorService;
   @Autowired
   private ExplorerService explorerService;
   @Autowired
   private SponsorService sponsorService;
   @Autowired
   private MezzageService mezzageService;
   @Autowired
   private ManagerService managerService;
   @Autowired
   private TripService tripService;
   @Autowired
   private SearchService searchService;
@Autowired
private TripApplicationService tripApplicationService;


   @Test
   public void registerAsExplorer() {
      Explorer administrator = explorerService.create();
      administrator.setName("perri");
      administrator.setEmail("perri@gmail.com");
      administrator.setAddress("calle de perri 23");
      administrator.setPhoneNumber("+34567345234");
      administrator.setSurname("el perro");
      UserAccount userAcco = new UserAccount();
      userAcco.setPassword("passwd");
      userAcco.setUsername("perriel");
      Set<Authority> auth = new HashSet<>();
      Authority authority = new Authority();
      authority.setAuthority("EXPLORER");
      auth.add(authority);
      userAcco.setAuthorities(auth);
      administrator.setUserAccount(userAcco);

      Actor res =  explorerService.registerAsExplorer(administrator);
      Assert.assertNotNull("resultado nulo al registrar el explorer",res);
   }

   @Test(expected = IllegalArgumentException.class)
   public void tripsEndedInWithIveEnrolled() {
      authenticate("explorer2");
      Collection<Trip> trips = explorerService.tripsEndedInWithIveEnrolled(explorerService.findByPrincipal().getId());
      org.springframework.util.Assert.notEmpty(trips);
   }


   @Test
   public void aplyTrip() {
      authenticate("explorer1");
      Trip trip = new ArrayList<>(tripService.findAll()).get(0);
      TripApplication tripApplication = tripApplicationService.create();
      tripApplication.setOwner(explorerService.findByPrincipal());
      trip.setTitle("perri");
      boolean res = explorerService.AplyTrip(trip, tripApplication);
      org.springframework.util.Assert.isTrue(res);
   }

   @Test
   public void myTripApplicationsFromStatus() {
      authenticate("explorer1");
      Status status = Status.ACCEPTED;
      Collection<TripApplication> tripApplications = explorerService.myTripApplicationsFromStatus(status);
      org.springframework.util.Assert.notEmpty(tripApplications);
   }

   @Test
   public void cancelTripApplication() {
      authenticate("explorer1");
      Status status = Status.ACCEPTED;
      TripApplication tripApplication = new ArrayList<>(explorerService.myTripApplicationsFromStatus(status)).get(0);
     boolean res =  explorerService.cancelTripApplication(tripApplication);
      org.springframework.util.Assert.isTrue(res);
   }

   @Test
   public void acceptTripApplication() {
      authenticate("explorer1");
      TripApplication tripApplication = tripApplicationService.create();
      tripApplication.setOwner(explorerService.findByPrincipal());
      CreditCard creditCard = new ArrayList<>( creditCardService.findAll()).get(0);
      explorerService.findByPrincipal().setCreditCard(creditCard);
      tripApplication.setStatus(Status.DUE);
      explorerService.findByPrincipal().getTripApplications().add(tripApplication);
      boolean res = explorerService.acceptTripApplication(tripApplication);
      org.springframework.util.Assert.isTrue(!res);
   }

   @Test
   public void editSearch() {
      authenticate("explorer1");
      Search search = searchService.create();
      search.setKeyWord("trip");
      explorerService.findByPrincipal().getSearches().add(search);
      search.setSearcher(explorerService.findByPrincipal());
      explorerService.editSearch(search);
   }

//   @Test
//   public void deleteSearch() throws Exception {
//      authenticate("explorer1");
//      Search search = searchService.create();
//      search.setKeyWord("trip");
//      explorerService.findByPrincipal().getSearches().add(search);
//      search.setSearcher(explorerService.findByPrincipal());
//      Collection<Trip> trips =  explorerService.makeSearchbyKeyword(search);
//      explorerService.deleteSearch(search);
//   }
//
//   @Test
//   public void makeSearchbyKeyword() throws Exception {
//      authenticate("explorer1");
//      Search search = searchService.create();
//      search.setKeyWord("trip");
//      explorerService.findByPrincipal().getSearches().add(search);
//      search.setSearcher(explorerService.findByPrincipal());
//     Collection<Trip> trips =  explorerService.makeSearchbyKeyword(search);
//   }

}